import * as React from "react";
import * as ReactDOM from "react-dom";

import { Header } from "./components/header/Header";
import { Footer } from "./components/footer/Footer";
import { ContentContainer } from "./components/homepage/container/ContentContainer"

//needed to adjust BODY margins
require("./global/globalStyles.less");

ReactDOM.render(
    <Header sectionTitle="Spending summaries"/>,
    document.getElementById("headerContainerTarget")
);

ReactDOM.render(
    <ContentContainer title="Your summaries" spendingSummaries={null} />,
    document.getElementById("contentContainerTarget")
);

ReactDOM.render(
    <Footer />,
    document.getElementById("footerContainerTarget")
);

