import * as React from "react";
import { ChartType } from "../../common/ChartType";
import * as RadixToggleGroup from '@radix-ui/react-toggle-group';
import { DragHandleHorizontalIcon, BarChartIcon, PieChartIcon } from '@radix-ui/react-icons';

const localStyles = require("./ChartTypeToggleGroup.module.less");

export interface ChartTypeToggleGroupProps { title: string; onValueChange: (chartType: ChartType) => void }

export class ChartTypeToggleGroup extends React.Component<ChartTypeToggleGroupProps, {}> {

    constructor(props: ChartTypeToggleGroupProps) {
        super(props);
    }

    validateAndUpdateValue = (chartType: ChartType) : void => {
      if (chartType.toString() !== "") {
        this.props.onValueChange(chartType);
      }
    }

    public render() {
        return <div className={localStyles.toggleWrapper}>
                  <RadixToggleGroup.Root
                    className={localStyles.toggleGroup}
                    type="single"
                    defaultValue={ChartType.LINE_GRAPH}
                    aria-label={this.props.title}
                    onValueChange={this.validateAndUpdateValue}
                  >
                    <RadixToggleGroup.Item className={localStyles.toggleGroupItem}
                                          value={ChartType.LINE_GRAPH}
                                          aria-label={ChartType.LINE_GRAPH}>
                      <DragHandleHorizontalIcon/>
                    </RadixToggleGroup.Item>
                    <RadixToggleGroup.Item className={localStyles.toggleGroupItem}
                                            value={ChartType.BAR_CHART}
                                            aria-label={ChartType.BAR_CHART}>
                      <BarChartIcon/>
                    </RadixToggleGroup.Item>
                    <RadixToggleGroup.Item className={localStyles.toggleGroupItem}
                                            value={ChartType.DOUGHNUT_CHART}
                                            aria-label={ChartType.DOUGHNUT_CHART}>
                      <PieChartIcon/>
                    </RadixToggleGroup.Item>
                  </RadixToggleGroup.Root>
        </div>;
    }
}
