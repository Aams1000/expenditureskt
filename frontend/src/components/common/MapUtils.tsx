export function getOrDefault(map: Map<any, any>, key: any, defaultValue: any) : any {
    return map.has(key) ? map.get(key) : defaultValue;
}
