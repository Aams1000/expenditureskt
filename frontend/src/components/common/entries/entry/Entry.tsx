import * as React from "react";
import { Columns } from "react-bulma-components";
import { Expenditure } from "../../entries/Expenditure"

const localStyles = require('./Entry.module.less');

export interface EntryProps { data: Expenditure; isChecked: boolean; titles: Array<string>; parentUpdateCheckbox: (id: number) => void;}

export class Entry extends React.Component<EntryProps, {}> {

    updateCheckbox = () : void => {
        this.props.parentUpdateCheckbox(this.props.data.id);
    }

    public render() {
        return <div className={localStyles.entry}>
                    <input name="isSelected" type="checkbox" checked={this.props.isChecked} onChange={this.updateCheckbox} />
                    <span>{this.props.data.id}</span>
                    <span>{this.props.data.transactionDate}</span>
                    <span>{this.props.data.merchant}</span>
                    <span>{this.props.data.amount}</span>
                    <span>{this.props.data.category}</span>
                </div>;
    }
}















