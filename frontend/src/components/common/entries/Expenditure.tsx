export interface Expenditure {
    id: number;
    transactionDate: string;
    merchant: string;
    amount: number;
    category: string;
    insertionDate: string;
}

