import * as React from "react";
import { Columns } from "react-bulma-components";
import { Expenditure } from "../../entries/Expenditure"
import { Entry } from "../entry/Entry"
import { EntryAndButtons } from "../entryandbuttons/EntryAndButtons"
import { EntriesFunctions } from "../EntriesFunctions";
import { Autocomplete } from "../../../common/autocomplete/Autocomplete";
import { DisplayToggle } from "../../../common/displaytoggle/DisplayToggle";

const localStyles = require('./EntriesContainer.module.less');

export interface EntriesContainerProps { titles: Array<string>;
                                                data: Array<[Expenditure, boolean]>;
                                                categories: Array<string>;
                                                isDisplayed: boolean;
                                                parentFunctions: EntriesFunctions }

export class EntriesContainer extends React.Component<EntriesContainerProps, {}> {

    generateContent = () : Array<JSX.Element> => {
        let content: Array<JSX.Element> = new Array();
        if (this.props.data !== null) {
            if (this.props.data.length === 0) {
                content.push(<div className={localStyles.parsedEntriesContainer}>
                                  <span className={localStyles.descriptionText}>{this.props.titles[0]}</span>
                                </div>);
            }
            else {
                this.props.data.forEach((pair: [Expenditure, boolean]) => content.push(<EntryAndButtons data={pair[0]}
                                                                                                        isChecked={pair[1]}
                                                                                                        categories={this.props.categories}
                                                                                                        titles={this.props.titles}
                                                                                                        parentFunctions={this.props.parentFunctions} />));
                const newContent: Array<JSX.Element> = new Array<JSX.Element>();
                newContent.push(<div className={localStyles.parsedEntriesContainer}>
                                  <DisplayToggle isContentDisplayed={this.props.isDisplayed}
                                                 showContentMessage="Show expenditures"
                                                 hideContentMessage="Hide expenditures"
                                                 onToggle={this.props.parentFunctions.toggleDisplay}/>
                                    {this.props.isDisplayed && 
                                      <table className={localStyles.table}>
                                         <caption>
                                           <a>{this.props.titles[1]}</a>
                                           <div className={localStyles.buttonAndAutoCompleteContainer}>
                                             <div className={localStyles.innerCaptionContainer}>
                                               <Autocomplete options={this.props.categories}
                                                                   extraTopMargin={true}
                                                                   extraLeftMargin={true}
                                                                     parentUpdateSelection={this.props.parentFunctions.updateBatchEntrySelection}
                                                                     parentConfirmChoiceAndProceed={this.props.parentFunctions.onUpdateSelectedEntries} />
                                                     <button className={localStyles.updateSelectedButton} onClick={this.props.parentFunctions.onUpdateSelectedEntries}>{this.props.titles[2]}</button>
                                               </div>
                                             </div>
                                           </caption>
                                         <thead>
                                           <tr>
                                             <th scope="col" className={localStyles.batchSelectCell}>
                                             </th>
                                             <th scope="col">{this.props.titles[3]}</th>
                                             <th scope="col">{this.props.titles[4]}</th>
                                             <th scope="col">{this.props.titles[5]}</th>
                                             <th scope="col">{this.props.titles[6]}</th>
                                             <th scope="col">{this.props.titles[7]}</th>
                                           </tr>
                                         </thead>
                                         <tbody>
                                           {content}
                                         </tbody>
                                     </table>
                                   }
                                   </div>
                );
                content = newContent;
            }
        }
        return content;
    }

    public render() {
        return this.generateContent();
    }
}
