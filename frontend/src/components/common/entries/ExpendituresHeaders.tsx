export const NEW_EXPENDITURES: Array<string> =  new Array("Inserted no new entries with known categories",
                                                                                                "The following entries were inserted into the database",
                                                                                                "Update selected entries",
                                                                                                "Date",
                                                                                                "Merchant",
                                                                                                "Amount",
                                                                                                "Category",
                                                                                                "Options");
                                                                                                
export const ALL_EXPENDITURES: Array<string> =  new Array("Inserted no new entries with known categories",
                                                                                                "All expenditures",
                                                                                                "Update selected entries",
                                                                                                "Date",
                                                                                                "Merchant",
                                                                                                "Amount",
                                                                                                "Category",
                                                                                                "Options");