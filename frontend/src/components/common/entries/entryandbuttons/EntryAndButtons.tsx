import * as React from "react";
import { Columns } from "react-bulma-components";
import { Entry } from "../entry/Entry";
import { Expenditure } from "../../entries/Expenditure";
import { Autocomplete } from "../../../common/autocomplete/Autocomplete";
import { EntriesFunctions } from "../EntriesFunctions";
import { Cross2Icon } from '@radix-ui/react-icons';
// import * as Autosuggest from 'react-autosuggest';

const localStyles = require('./EntryAndButtons.module.less');
const testOptions: Array<string> = new Array("one", "two", "three");


export interface EntryAndButtonsProps { data: Expenditure;
                                        isChecked: boolean;
                                        categories: Array<string>;
                                        titles: Array<string>;
                                        parentFunctions: EntriesFunctions; }

export class EntryAndButtons extends React.Component<EntryAndButtonsProps, {}> {

    updateCheckbox = () : void => {
        this.props.parentFunctions.onEntryCheckbox(this.props.data.id);
    }

    updateCategory = () : void => {
        this.props.parentFunctions.parentUpdateCategory(this.props.data.id);
    }

    updateMerchantCategory = () : void => {
        this.props.parentFunctions.onMerchantUpdate(this.props.data.id, this.props.data.merchant);
    }

    testOnSelect = (value: string) : void => {
        this.props.parentFunctions.onCategoryUpdate(this.props.data.id, value);
    }

    confirmChoiceAndProceed = () : void => {
        this.props.parentFunctions.parentUpdateCategory(this.props.data.id);
    }

    deleteExpenditure = () : void => {
        this.props.parentFunctions.deleteEntry(this.props.data.id);
    }

    renderItem = (item: string) => {
         return <div>
                  {item}
                </div>
    }

    getSuggestion = (value: string) : string => {
        return value;
    }

// Teach Autosuggest how to calculate suggestions for any given input value.
getSuggestions = (value: string) : Array<string> => {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;
 
  return inputLength === 0 ? [] : testOptions.filter((lang: string) =>
    lang.toLowerCase().slice(0, inputLength) === inputValue
  );
};
 
// When suggestion is clicked, Autosuggest needs to populate the input
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.
// const getSuggestionValue = suggestion => suggestion.name;
 
// Use your imagination to render suggestions.
renderSuggestion = (suggestion: string) => {
  return <div>
    {suggestion}
  </div>
}
 
  onSuggestionsFetchRequested = () : void => {

  }
 
  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () : void => {
  }
 
    // Autosuggest will pass through all these props to the input.
    getInputProps = () => {
        return {
              placeholder: "",
              value: "",
              onChange: this.testOnSelect
            };
    }
    

    public render() {
        return <tr>
                   <td scope="row" data-label="">
                    <input name="isSelected" type="checkbox" checked={this.props.isChecked} onChange={this.updateCheckbox} />
                  </td>
                    <td scope="row" data-label={this.props.titles[3]}>{this.props.data.transactionDate}</td>
                    <td scope="row" data-label={this.props.titles[4]}>{this.props.data.merchant}</td>
                    <td scope="row" data-label={this.props.titles[5]}>{this.props.data.amount}</td>
                    <td scope="row" data-label={this.props.titles[6]}>{this.props.data.category}</td>
                   <td scope="row" className={localStyles.buttonCell} data-label={this.props.titles[7]}>
                       <Autocomplete options={this.props.categories} 
                                   extraTopMargin={true}
                                   extraLeftMargin={false}
                                   parentUpdateSelection={this.testOnSelect}
                                   parentConfirmChoiceAndProceed={this.confirmChoiceAndProceed} />
                        <button className={localStyles.setCategoryButton} onClick={this.updateCategory}>Set category</button>
                        <button className={localStyles.setMerchantCategoryButton} onClick={this.updateMerchantCategory}>Set merchant category</button>
                        <button className={localStyles.deleteExpenditureButton} onClick={this.deleteExpenditure}>X</button>
                        {/*<Cross2Icon onClick={this.deleteExpenditure}/>*/}
                        {/*<button className={localStyles.deleteExpenditureButton} onClick={this.deleteExpenditure}><Cross2Icon/></button>*/}
                   </td>
                </tr>;
    }
}















