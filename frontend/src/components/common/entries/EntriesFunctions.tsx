export class EntriesFunctions {
    onEntryCheckbox: (id: number) => void;
    onUpdateSelectedEntries: () => void;
    onCategoryUpdate: (id: number, category: string) => void;
    parentUpdateCategory: (id: number) => void;
    onMerchantUpdate: (entryId: number, merchant: string) => void;
    updateBatchEntrySelection: (category: string) => void;
    toggleDisplay: () => void;
    deleteEntry: (entryId: number) => void;

    constructor(onEntryCheckbox: (id: number) => void,
                onUpdateSelectedEntries: () => void,
                onCategoryUpdate: (id: number, category: string) => void,
                parentUpdateCategory: (id: number) => void,
                onMerchantUpdate: (entryId: number, merchant: string) => void,
                updateBatchEntrySelection: (category: string) => void,
                toggleDisplay: () => void,
                deleteEntry: (entryId: number) => void) {
        this.onEntryCheckbox = onEntryCheckbox;
        this.onUpdateSelectedEntries = onUpdateSelectedEntries;
        this.onCategoryUpdate = onCategoryUpdate;
        this.parentUpdateCategory = parentUpdateCategory;
        this.onMerchantUpdate = onMerchantUpdate;
        this.updateBatchEntrySelection = updateBatchEntrySelection;
        this.toggleDisplay = toggleDisplay;
        this.deleteEntry = deleteEntry;
    }
}

