export function singleGetRequest(url: string,
                                 onSuccess: (data: object) => void,
                                 errorMessage: string) : void {
    fetch(url, {
        method: 'GET',
        referrer: 'no-referrer',
    })
    .then((response: Response) => {
        response.json().then(data => {
            if (response.ok) {
                onSuccess(data)
            }
            else {
                console.log(errorMessage);
            }
        });
    })
    .catch((error: Error) => console.log("Failed to execute get request: " + error.toString()));
}

export function singlePostRequest(url: string,
                                  data: FormData,
                                  onSuccess: (data: object) => void,
                                  errorMessage: string) : void {
    fetch(url, {
        method: 'POST',
        body: data,
        referrer: 'no-referrer',
    })
    .then((response: Response) => {
        response.json().then(data => {
            // console.log(data);
            if (response.ok) {
                onSuccess(data);
            }
            else {
                console.log(errorMessage);
            }
        });
    })
    .catch((error: Error) => console.log("Failed to execute post request: " + error.toString()));
}

export function postThenGetRequest(postUrl: string,
                                   getUrl: string,
                                   data: FormData,
                                   onSuccess: (data: object) => void,
                                   postErrorMessage: string,
                                   getErrorMessage: string) : void {
    fetch(postUrl, {
            method: 'POST',
            body: data,
            referrer: 'no-referrer',
        })
        .then((response: Response) => {
            response.json().then(data => {
                if (response.ok) {
                    fetch(getUrl, {
                        method: 'GET',
                        referrer: 'no-referrer',
                    })
                    .then((response: Response) => {
                        response.json().then(data => {
                            if (response.ok) {
                                onSuccess(data);
                            }
                            else {
                                console.log(getErrorMessage);
                            }
                        });
                    });
                }
                else {
                    console.log(postErrorMessage);
                }
            });
        })
        .catch((error: Error) => console.log("Failed to execute post and get request: " + error.toString()));
}

