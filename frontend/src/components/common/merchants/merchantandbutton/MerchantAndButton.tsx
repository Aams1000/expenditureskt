import * as React from "react";
import { Columns } from "react-bulma-components";
import { Autocomplete } from "../../../common/autocomplete/Autocomplete";
import { MerchantsFunctions } from "../MerchantsFunctions";

const localStyles = require('./MerchantAndButton.module.less');

export interface MerchantAndButtonProps { merchant: string;
                                            isChecked: boolean;
                                            currentCategory: string;
                                            categories: Array<string>;
                                            titles: Array<string>;
                                            parentFunctions: MerchantsFunctions; }

export class MerchantAndButton extends React.Component<MerchantAndButtonProps, {}> {

    updateCheckbox = () : void => {
        this.props.parentFunctions.parentUpdateCheckbox(this.props.merchant);
    }

    testOnSelect = (value: string) : void => {
        this.props.parentFunctions.onCategoryUpdate(this.props.merchant, value);
    }

    confirmChoiceAndProceed = () : void => {
        this.props.parentFunctions.onMerchantUpdate(this.props.merchant);
    }

    ignoreMerchant = () : void => {
        this.props.parentFunctions.ignoreMerchant(this.props.merchant);
    }

    public render() {

      const newMerchantAutocomplete: JSX.Element = <Autocomplete options={this.props.categories} 
                                   extraTopMargin={false}
                                   extraLeftMargin={false}
                                   parentUpdateSelection={this.testOnSelect}
                                   parentConfirmChoiceAndProceed={this.confirmChoiceAndProceed} />;
      const allMerchantsAutocomplete: JSX.Element = <Autocomplete options={this.props.categories} 
                                   extraTopMargin={false}
                                   extraLeftMargin={false}
                                   parentUpdateSelection={this.testOnSelect}
                                   parentConfirmChoiceAndProceed={this.confirmChoiceAndProceed} />;
      const setMerchantCategoryButton: JSX.Element = <button className={localStyles.setMerchantCategoryButton} onClick={this.confirmChoiceAndProceed}>Set merchant category</button>;
      const ignoreMerchantButton: JSX.Element = <button className={localStyles.ignoreMercantButton} onClick={this.ignoreMerchant}>Ignore merchant</button>;

      // const noCurrentCategoryAutocomplete: JSX.Element = <td data-label={this.props.titles[3]}>{autocomplete}</td>;
      // const noCurrentCategoryButtons: JSX.Element = <td scope="row" data-label={this.props.titles[4]}>
      //                                                  {setMerchantCategoryButton}
      //                                                  {ignoreMerchantButton}
      //                                              </td>;

      // const currentCategoryAutocompleteAndButtons: JSX.Element = <td scope="row" data-label={this.props.titles[4]}>
      //                                                                  {autocomplete}
      //                                                                  {setMerchantCategoryButton}
      //                                                                  {ignoreMerchantButton}
      //                                                              </td>;

        return <tr>
                   <td scope="row" data-label={""}>
                       <input name="isSelected" type="checkbox" checked={this.props.isChecked} onChange={this.updateCheckbox} />
                   </td>
                   <td scope="row" data-label={this.props.titles[2]}>{this.props.merchant}</td>
                   {this.props.currentCategory !== null ? 
                      <>
                       <td scope="row" data-label={this.props.titles[3]}>{this.props.currentCategory}</td>
                       <td scope="row" className={localStyles.buttonCell} data-label={this.props.titles[4]}>
                                                                       {allMerchantsAutocomplete}
                                                                       {setMerchantCategoryButton}
                                                                       {ignoreMerchantButton}
                                                                   </td>
                      </>
                      :
                       <>
                        <td data-label={this.props.titles[3]}>{newMerchantAutocomplete}</td>
                        <td scope="row" data-label={this.props.titles[4]}>
                                                       {setMerchantCategoryButton}
                                                       {ignoreMerchantButton}
                                                   </td>
                       </>
                   }
                   
                </tr>;
    }
}















