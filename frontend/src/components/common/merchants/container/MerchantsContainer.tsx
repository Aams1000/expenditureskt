import * as React from "react";
import { Columns } from "react-bulma-components";
import { MerchantAndButton } from "../merchantandbutton/MerchantAndButton"
import { MerchantsFunctions } from "../MerchantsFunctions";
import { Autocomplete } from "../../../common/autocomplete/Autocomplete";
import { DisplayToggle } from "../../../common/displaytoggle/DisplayToggle";

const localStyles = require('./MerchantsContainer.module.less');

export interface MerchantsContainerProps { titles: Array<string>;
                                                    merchantsByCategory: Map<string, string>;
                                                    shouldShowCategory: boolean;
                                                    merchantAndBoxStatus: Array<[string, boolean]>;
                                                    categories: Array<string>;
                                                    isDisplayed: boolean;
                                                    parentFunctions: MerchantsFunctions; }

export class MerchantsContainer extends React.Component<MerchantsContainerProps, {}> {

    generateContent = () : Array<JSX.Element> => {
        let content: Array<JSX.Element> = new Array();
        if (this.props.merchantAndBoxStatus !== null) {
            if (this.props.merchantAndBoxStatus.length === 0) {
                content.push(<div className={localStyles.MerchantsContainer}>
                                  <span className={localStyles.descriptionText}>{this.props.titles[0]}</span>
                                </div>);
            }
            else {
                this.props.merchantAndBoxStatus.forEach((pair: [string, boolean]) => content.push(<MerchantAndButton merchant={pair[0]}
                                                                                                        isChecked={pair[1]}
                                                                                                        currentCategory={this.props.shouldShowCategory ? this.props.merchantsByCategory.get(pair[0]) : null}
                                                                                                        categories={this.props.categories}
                                                                                                        titles={this.props.titles}
                                                                                                        parentFunctions={this.props.parentFunctions}/>));
                const newContent: Array<JSX.Element> = new Array<JSX.Element>();
                newContent.push(<div className={localStyles.MerchantsContainer}>
                                  <DisplayToggle isContentDisplayed={this.props.isDisplayed}
                                                 showContentMessage="Show merchants"
                                                 hideContentMessage="Hide merchants"
                                                 onToggle={this.props.parentFunctions.toggleDisplay}/>
                                    {this.props.isDisplayed && 
                                      <table className={localStyles.table}>
                                         <caption>
                                           <a>{this.props.titles[1]}</a>
                                           <div className={localStyles.buttonAndAutoCompleteContainer}>
                                             <div className={localStyles.innerCaptionContainer}>
                                               <Autocomplete options={this.props.categories}
                                                                   extraTopMargin={true}
                                                                   extraLeftMargin={true}
                                                                     parentUpdateSelection={this.props.parentFunctions.onBatchCategoryUpdate}
                                                                     parentConfirmChoiceAndProceed={this.props.parentFunctions.onBatchUpdate} />
                                                     <button className={localStyles.updateSelectedButton} onClick={this.props.parentFunctions.onBatchUpdate}>{this.props.titles[5]}</button>
                                               </div>
                                             </div>
                                         </caption>
                                         <thead>
                                           <tr>
                                             <th scope="col" className={localStyles.batchSelectCell}>
                                             </th>
                                             <th scope="col">{this.props.titles[2]}</th>
                                             <th scope="col">{this.props.titles[3]}</th>
                                             <th scope="col">{this.props.titles[4]}</th>
                                           </tr>
                                         </thead>
                                         <tbody>
                                           {content}
                                         </tbody>
                                     </table>
                                   }
                                   </div>);

                content = newContent;
            }
        }
        return content;
    }

    public render() {
        return this.generateContent();
    }
}
