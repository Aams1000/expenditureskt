export const NEW_MERCHANTS: Array<string> = new Array("All merchants from files have assigned categories",
                                                                                                "The files contained the following uncategorized merchants",
                                                                                                "Merchant",
                                                                                                "Category",
                                                                                                "Options",
                                                                                                "Update selected merchants");

export const ALL_MERCHANTS: Array<string> = new Array("No merchants have been added",
                                                                                                "All merchants",
                                                                                                "Merchant",
                                                                                                "Category",
                                                                                                "Options",
                                                                                                "Update selected merchants");

export const IGNORED_MERCHANTS: Array<string> = new Array("No merchants have been ignored",
                                                                                                "Ignored merchants",
                                                                                                "Merchant",
                                                                                                "Category",
                                                                                                "Options",
                                                                                                "Update selected merchants");
