import * as React from "react";
import { Columns } from "react-bulma-components";

import Select from 'react-select';

export interface MultiSelectProps { options: Array<string>;
                                    onChange: () => void;}

export class MultiSelect extends React.Component<MultiSelectProps, {}> {

        public render() {
            return <Select
                    isMulti
                    // options={this.props.options}
                    options={["a", "b", "c"]}
                    className="basic-multi-select"
                    classNamePrefix="select"
                  />
        }                                                                                
}

