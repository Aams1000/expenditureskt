const COLOR_MAP: Map<string, string> = new Map<string, string>([["themePurple", "#a192bf"],
                                                                ["themeLightMoss", "#99bbbb"],
                                                                ["themeCharcoal", "#3e4c5e"],
                                                                ["themeIndigo", "#2c3d55"],
                                                                ["themeCopper", "#a8763e"],
                                                                ["themeOlive", "#6F732F"],
                                                                ["themePearl", "#CFD2B2"],
                                                                ["themeChamoisee", "#AD8350"],
                                                                ["themeCeladon", "#80CFA9"],
                                                                ["themeDesertSand", "#ECC8AF"],
                                                                ["themeVistaBlue", "#829CBC"],
                                                                ["themeDarkPurple", "#7a66a1"],
                                                                ["themePlatinum", "#E5E5E5"],
                                                                ["themeFairyTale", "#E6BCCD"],
                                                                ["themeSage", "#B1AE91"],
                                                                ["themeLilac", "#D295BF"],
                                                                ["themeCambridgeBlue", "#7A9E7E"],
                                                                ["themeEcru", "#AFA060"],
                                                                ["themeOlivine", "#95BF8F"],
                                                                ["themeAmaranthPurple", "#A63A50"],
                                                                ["themeTiffanyBlue", "#95D9C3"],
                                                                ["nonPhotoBlue", "#A7E2E3"]]);

export function getColorNames(length: number) : Array<string> {
    let colorArray: Array<string> = [];
    const colorNames: Array<string> = Array.from(COLOR_MAP.values());
    for (let i = 0; i < length; i++) {
        colorArray.push(colorNames[i % colorNames.length]);
    }
    return colorArray;
}

