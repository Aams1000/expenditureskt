import * as React from 'react';

const localStyles = require('./Autocomplete.module.less');

// Adapted from https://codesandbox.io/s/ojr02w7x55?from-embed=&file=/src/Autocomplete.js
export interface AutocompleteProps { options: Array<string>;
                                      extraTopMargin: boolean;
                                      extraLeftMargin: boolean;
                                      parentUpdateSelection: (value: string) => void;
                                      parentConfirmChoiceAndProceed: () => void; }

export class Autocomplete extends React.Component<AutocompleteProps, {activeOption: number;
                                                                      filteredOptions: Array<string>;
                                                                      showOptions: boolean;
                                                                      userInput: string; }> {

  constructor(props: AutocompleteProps){
        super(props);
        this.state = {activeOption: 0,
                      filteredOptions: [],
                      showOptions: false,
                      userInput: '' 
                    };
  }

  callparentUpdateSelectionIfValid = (value: string) : void => {
    if (this.props.options.includes(value)) {
      this.props.parentUpdateSelection(value);
    }
    else {
      console.log("Invalid input");
    }
  }
  

  onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { options } = this.props;
    const userInput = e.currentTarget.value;

    const filteredOptions: Array<string> = options.filter(
      (optionName) =>
        optionName.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );

    this.setState({
      activeOption: 0,
      filteredOptions,
      showOptions: true,
      userInput: e.currentTarget.value
    });
  };

  onClick = (e: React.MouseEvent<HTMLElement>) => {
    const selectedOption: string = e.currentTarget.innerText;
    this.props.parentUpdateSelection(selectedOption);
    this.setState({
      activeOption: 0,
      filteredOptions: [],
      showOptions: false,
      userInput: selectedOption
    });
  };

  onKeyDown = (e: React.KeyboardEvent<HTMLElement>) => {
    const { activeOption, filteredOptions } = this.state;

    // enter
    if (e.keyCode === 13) {
      if (this.props.options.includes(this.state.userInput)) {
        this.props.parentUpdateSelection(this.state.userInput);
        this.props.parentConfirmChoiceAndProceed();
      }
    }
    // tab
    else if (e.keyCode === 9) {
      const selectedOption: string = filteredOptions[activeOption];
      if (selectedOption) {
        this.props.parentUpdateSelection(selectedOption);
      }
      this.setState({
        activeOption: 0,
        showOptions: false,
        userInput: selectedOption ? selectedOption : this.state.userInput
      });
    }
    // up arrow
    else if (e.keyCode === 38) {
      if (activeOption === 0) {
        return;
      }
      this.setState({ activeOption: activeOption - 1 });
    }
    // down arrow
     else if (e.keyCode === 40) {
      if (activeOption === filteredOptions.length - 1) {
        // console.log(activeOption);
        return;
      }
      this.setState({ activeOption: activeOption + 1 });
    }
  };

  render() {
    const {
      onChange,
      onClick,
      onKeyDown,

      state: { activeOption, filteredOptions, showOptions, userInput }
    } = this;
    let optionList;
    const firstOptionsClassExtension: string = this.props.extraTopMargin ? " " + localStyles.extraTopMargin : "";
    const secondOptionClassExtension: string = this.props.extraLeftMargin ? " " + localStyles.extraLeftMargin : "";
    const optionsClassName: string = localStyles.options + firstOptionsClassExtension + secondOptionClassExtension;
    if (showOptions && userInput) {
      if (filteredOptions.length) {
        optionList = (
          <ul className={optionsClassName}>
            {filteredOptions.map((optionName, index) => {
              let className;
              if (index === activeOption) {
                className = localStyles.optionActive;
              }
              return (
                <li className={className} key={optionName} onClick={onClick}>
                  {optionName}
                </li>
              );
            })}
          </ul>
        );
      } else {
        optionList = new Array();
      }
    }
    return (
      <React.Fragment>
        <div className={localStyles.search}>
          <input
            type="text"
            className={localStyles.searchBox}
            onChange={onChange}
            onKeyDown={onKeyDown}
            value={userInput}
          />
          {/*<input type="submit" value="" className="search-btn" />*/}
        </div>
        {optionList}
      </React.Fragment>
    );
  }
}

