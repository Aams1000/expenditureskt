// Deliberately non-const due to https://stackoverflow.com/questions/17380845/how-do-i-convert-a-string-to-enum-in-typescript/56076148#56076148
export enum ChartType {
    LINE_GRAPH = "Line graph",
    BAR_CHART = "Bar chart",
    DOUGHNUT_CHART = "Doughnut chart"
}
