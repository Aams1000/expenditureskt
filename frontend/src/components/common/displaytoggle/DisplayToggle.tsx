import * as React from "react";

const localStyles = require('./DisplayToggle.module.less');

export interface DisplayToggleProps { isContentDisplayed: boolean;
                                                showContentMessage: string;
                                                hideContentMessage: string;
                                                onToggle: () => void;
 }

export class DisplayToggle extends React.Component<DisplayToggleProps, {}> {

    public render() {
        const message: string = this.props.isContentDisplayed ? this.props.hideContentMessage : this.props.showContentMessage;
        const chevron: string = this.props.isContentDisplayed ? "\u2303" : "\u2304";
        return <div className={localStyles.toggleDisplayContainer} onClick={e => this.props.onToggle()}>
                    {chevron}
                    <span className={localStyles.displayToggleText}>{message}</span>
                </div>;
    }
}
