import { Expenditure } from "../entries/Expenditure";

export function generateExpendituresAndStatus(expenditures: Array<Expenditure>, checkedBoxes: Set<number>) : Array<[Expenditure, boolean]> {
    const expendituresAndStatus: Array<[Expenditure, boolean]> = new Array();
    expenditures.forEach((expenditure: Expenditure) => expendituresAndStatus.push([expenditure, checkedBoxes.has(expenditure.id)]));
    return expendituresAndStatus;
}

export function generateEntitiesAndStatus(entities: Array<string>, checkedBoxes: Set<string>) : Array<[string, boolean]> {
    const entitiesAndStatus: Array<[string, boolean]> = new Array();
    entities.forEach((entity: string) => entitiesAndStatus.push([entity, checkedBoxes.has(entity)]));
    return entitiesAndStatus;
}


