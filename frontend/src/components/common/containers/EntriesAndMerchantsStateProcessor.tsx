import { Expenditure } from "../../common/entries/Expenditure";
import * as MapUtils from "../../common/MapUtils";
import { EntriesAndMerchantsState } from "../../common/containers/EntriesAndMerchantsState";

export function updateStateWithNewMerchantCategories(merchants: Array<string>, newCategory: string, content: EntriesAndMerchantsState) : EntriesAndMerchantsState {
    const merchantSet: Set<string> = new Set(merchants);
    const newExpenditures: Array<Expenditure> = new Array();
    const newMerchantsByCategory: Map<string, string> = new Map();
    for (const expenditure of content.expenditures) {
      if (merchantSet.has(expenditure.merchant)) {
        expenditure.category = newCategory;
      }
      newExpenditures.push(expenditure);
    }
    for (const merchant of content.merchantsByCategory.keys()) {
      const category: string = merchantSet.has(merchant) ? newCategory : content.merchantsByCategory.get(merchant);
      newMerchantsByCategory.set(merchant, category);
    }
    const newContent: EntriesAndMerchantsState = EntriesAndMerchantsState.from(content);
    newContent.expenditures = newExpenditures;
    newContent.merchantsByCategory = newMerchantsByCategory;
    return newContent;
}

export function updateCategoriesForEntries(entryIds: Array<number>, newCategory: string, content: EntriesAndMerchantsState) : EntriesAndMerchantsState {
    const entryIdSet: Set<number> = new Set(entryIds);
    const newExpenditures: Array<Expenditure> = new Array();
    for (const expenditure of content.expenditures) {
      if (entryIdSet.has(expenditure.id)) {
        expenditure.category = newCategory;
      }
      newExpenditures.push(expenditure);
    }
    const newContent: EntriesAndMerchantsState = EntriesAndMerchantsState.from(content);
    newContent.expenditures = newExpenditures;
    return newContent;
}

export function generateContentWithoutMerchants(merchantsArray: Array<string>, content: EntriesAndMerchantsState) : EntriesAndMerchantsState  {
    const merchantsSet: Set<string> = new Set(merchantsArray);
    const newMerchants: Array<string> = new Array();
    const newMerchantsByCategory: Map<string, string> = new Map();

    for (const merchant of content.merchantsByCategory.keys()) {
      if (!merchantsSet.has(merchant)) {
          newMerchants.push(merchant);
          newMerchantsByCategory.set(merchant, content.merchantsByCategory.get(merchant));
      }
    }

    const newExpenditures: Array<Expenditure> = content.expenditures.filter(entry => !merchantsSet.has(entry.merchant));
    const newCheckedEntries: Set<number> = new Set(newExpenditures.filter(entry => content.checkedEntries.has(entry.id))
                                                          .map(entry => entry.id));

    const newCheckedMerchants: Set<string> = content.checkedMerchants;
    const newCategorySelectionsByMerchant: Map<string, string> = content.categorySelectionsByMerchant;
    const newEntryIdsByMerchant: Map<string, Array<number>> = content.entryIdsByMerchant;
    for (const merchant of merchantsSet) {
        newCategorySelectionsByMerchant.delete(merchant);
        newCheckedMerchants.delete(merchant);
        newEntryIdsByMerchant.delete(merchant);
    }
    
    const newCategorySelectionsByEntry: Map<number, string> = new Map();
    newExpenditures.forEach(entry => {
        const previousValue: string = content.categorySelectionsByEntry.get(entry.id);
        if (previousValue !== null) {
            newCategorySelectionsByEntry.set(entry.id, previousValue);
        }
    });

    const newContent: EntriesAndMerchantsState = EntriesAndMerchantsState.from(content);
    newContent.expenditures = newExpenditures;
    newContent.checkedEntries = newCheckedEntries;
    newContent.merchantsByCategory = newMerchantsByCategory;
    newContent.checkedMerchants = newCheckedMerchants;
    newContent.entryIdsByMerchant = newEntryIdsByMerchant;
    newContent.categorySelectionsByEntry = newCategorySelectionsByEntry;
    newContent.categorySelectionsByMerchant = newCategorySelectionsByMerchant;
    return newContent;
}

export function generateContentWithoutEntries(entryIds: Array<number>, content: EntriesAndMerchantsState) : EntriesAndMerchantsState  {
    const entryIdSet: Set<number> = new Set(entryIds);
    const newExpenditures: Array<Expenditure> = content.expenditures.filter(entry => !entryIdSet.has(entry.id));
    const newCheckedEntries: Set<number> = new Set(newExpenditures.filter(entry => content.checkedEntries.has(entry.id))
                                                          .map(entry => entry.id));

    const newEntryIdsByMerchant: Map<string, Array<number>> = new Map();
    for (const merchantAndIdArray of content.entryIdsByMerchant) {
        newEntryIdsByMerchant.set(merchantAndIdArray[0], merchantAndIdArray[1].filter(id => !entryIdSet.has(id)));
    }

    const newCategorySelectionsByEntry: Map<number, string> = content.categorySelectionsByEntry;
    entryIdSet.forEach(id => newCategorySelectionsByEntry.delete(id));

    const newContent: EntriesAndMerchantsState = EntriesAndMerchantsState.from(content);
    newContent.expenditures = newExpenditures;
    newContent.checkedEntries = newCheckedEntries;
    newContent.entryIdsByMerchant = newEntryIdsByMerchant;
    newContent.categorySelectionsByEntry = newCategorySelectionsByEntry;
    return newContent;
}

export function generateState(allExpenditures: Array<Expenditure>, merchantsByCategory: Map<string, string>, shouldDisplay: boolean) : EntriesAndMerchantsState {
    const checkedMerchants: Set<string> = new Set();
    const checkedEntries: Set<number> = new Set();
    const entryIdsByMerchant: Map<string, Array<number>> = initializeRowTracking(allExpenditures, Array.from(merchantsByCategory.keys()));
    const categorySelectionsByEntry: Map<number, string> = new Map();
    const categorySelectionsByMerchant: Map<string, string> = new Map();
    const batchMerchantCategory: string = null;
    const batchEntryCategory: string = null;
    const entriesAndMerchantsState: EntriesAndMerchantsState = new EntriesAndMerchantsState(checkedMerchants,
                                                                                               checkedEntries,
                                                                                               merchantsByCategory,
                                                                                               allExpenditures,
                                                                                               entryIdsByMerchant,
                                                                                               categorySelectionsByEntry,
                                                                                               categorySelectionsByMerchant,
                                                                                               batchMerchantCategory,
                                                                                               batchEntryCategory,
                                                                                               shouldDisplay,
                                                                                               shouldDisplay);
    return entriesAndMerchantsState;
}

export function generateInitialState(isDataPresent: boolean,
                                        allExpenditures: Array<Expenditure>,
                                        allMerchants: Array<string>,
                                        shouldDisplay: boolean) : EntriesAndMerchantsState {
    const merchantsByCategory: Map<string, string> = allMerchants !== null ? unknownMerchantsToCategoryMap(allMerchants) : null;
    return generateInitialStateFromMerchantsMap(isDataPresent, allExpenditures, merchantsByCategory, shouldDisplay);
}

//TODO - pull default behavior into a generateDefault() method
export function generateInitialStateFromMerchantsMap(isDataPresent: boolean,
                                                        allExpenditures: Array<Expenditure>,
                                                        merchantsByCategory: Map<string, string>,
                                                        shouldDisplay: boolean) : EntriesAndMerchantsState {
      const checkedMerchants: Set<string> = isDataPresent ? new Set() : null;
    const checkedEntries: Set<number> = isDataPresent ? new Set() : null;
    const entryIdsByMerchant: Map<string, Array<number>> = legacyInitializeRowTracking(allExpenditures, merchantsByCategory);
    const categorySelectionsByEntry: Map<number, string> = new Map();
    const categorySelectionsByMerchant: Map<string, string> = new Map();
    const batchMerchantCategory: string = null;
    const batchEntryCategory: string = null;
    const entriesAndMerchantsState: EntriesAndMerchantsState = new EntriesAndMerchantsState(checkedMerchants,
                                                                                               checkedEntries,
                                                                                               merchantsByCategory,
                                                                                               allExpenditures,
                                                                                               entryIdsByMerchant,
                                                                                               categorySelectionsByEntry,
                                                                                               categorySelectionsByMerchant,
                                                                                               batchMerchantCategory,
                                                                                               batchEntryCategory,
                                                                                               shouldDisplay,
                                                                                               shouldDisplay);
    return entriesAndMerchantsState;
}

export function toggleExpendituresDisplay(state: EntriesAndMerchantsState) : EntriesAndMerchantsState {
    state.shouldShowEntries = !state.shouldShowEntries;
    return state;
}

export function toggleMerchantsDisplay(state: EntriesAndMerchantsState) : EntriesAndMerchantsState {
    state.shouldShowMerchants = !state.shouldShowMerchants;
    return state;
}

//TODO - pull default behavior into a generateDefault() method
function legacyInitializeRowTracking(allExpenditures: Array<Expenditure>, merchantsByCategory: Map<string, string>) : Map<string, Array<number>> {
    let result: Map<string, Array<number>> = null;
    if (allExpenditures !== null && merchantsByCategory !== null) {
        result = initializeRowTracking(allExpenditures, Array.from(merchantsByCategory.keys()));
    }
    return result;
}

function initializeRowTracking(allExpenditures: Array<Expenditure>, allMerchants: Array<string>) : Map<string, Array<number>> {
    const entryIdsByMerchant: Map<string, Array<number>> = new Map();
    allExpenditures.forEach((expenditure: Expenditure) => {
        const merchant: string = expenditure.merchant;
        const entryIds: Array<number> = MapUtils.getOrDefault(entryIdsByMerchant, merchant, new Array());
        entryIds.push(expenditure.id);
        entryIdsByMerchant.set(merchant, entryIds);
    });
    return entryIdsByMerchant;
}

/**
 * @deprecated appears to be used only in the generation of initial state, which should likely not exist
 * as the page should just use the componentDidMount pattern to wait for the server response instead 
 */
function unknownMerchantsToCategoryMap(merchants: Array<string>) : Map<string, string> {
    const categoryMap: Map<string, string> = new Map();
    merchants.forEach(merchant => categoryMap.set(merchant, "Unknown"));
    return categoryMap;
}


