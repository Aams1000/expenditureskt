import { Expenditure } from "../entries/Expenditure";

export class EntriesAndMerchantsState {

     checkedMerchants: Set<string>;
     checkedEntries: Set<number>;
     merchantsByCategory: Map<string, string>;
     expenditures: Array<Expenditure>;
     entryIdsByMerchant: Map<string, Array<number>>;
     categorySelectionsByEntry: Map<number, string>;
     categorySelectionsByMerchant: Map<string, string>;
     batchMerchantCategory: string;
     batchEntryCategory: string;
     shouldShowEntries: boolean;
     shouldShowMerchants: boolean;

     constructor(checkedMerchants: Set<string>,
                checkedEntries: Set<number>,
                merchantsByCategory: Map<string, string>,
                expenditures: Array<Expenditure>,
                entryIdsByMerchant: Map<string, Array<number>>,
                categorySelectionsByEntry: Map<number, string>,
                categorySelectionsByMerchant: Map<string, string>,
                batchMerchantCategory: string,
                batchEntryCategory: string,
                shouldShowEntries: boolean,
                shouldShowMerchants: boolean) {
        this.checkedMerchants = checkedMerchants;
        this.checkedEntries = checkedEntries;
        this.merchantsByCategory = merchantsByCategory;
        this.expenditures = expenditures;
        this.entryIdsByMerchant = entryIdsByMerchant;
        this.categorySelectionsByEntry = categorySelectionsByEntry;
        this.categorySelectionsByMerchant = categorySelectionsByMerchant;
        this.batchMerchantCategory = batchMerchantCategory;
        this.batchEntryCategory = batchEntryCategory;
        this.shouldShowEntries = shouldShowEntries;
        this.shouldShowMerchants = shouldShowMerchants;
     }

     static from(other: EntriesAndMerchantsState) : EntriesAndMerchantsState {
         return new EntriesAndMerchantsState(other.checkedMerchants,
                                                other.checkedEntries,
                                                other.merchantsByCategory,
                                                other.expenditures,
                                                other.entryIdsByMerchant,
                                                other.categorySelectionsByEntry,
                                                other.categorySelectionsByMerchant,
                                                other.batchMerchantCategory,
                                                other.batchEntryCategory,
                                                other.shouldShowEntries,
                                                other.shouldShowMerchants); 
     }

     //TODO - update these default values to be consistent
     static generateDefault() : EntriesAndMerchantsState {
         return new EntriesAndMerchantsState(null,
                                             null,
                                             null,
                                             null,
                                             null,
                                             new Map(),
                                             new Map(),
                                             null,
                                             null,
                                             false,
                                             false);
     }
}



