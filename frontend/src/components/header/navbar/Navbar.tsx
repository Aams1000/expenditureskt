import * as React from "react";
import { Columns } from "react-bulma-components";
import { NavbarItem } from "./navbarItem/NavbarItem"
import * as NavbarData from "./NavbarData"
const localStyles = require('./Navbar.module.less');

export interface NavbarProps { }

export class Navbar extends React.Component<NavbarProps, {}> {
    render() {
        return <div id={localStyles.navbar} className={localStyles.header}>
                  <NavbarItem  title={NavbarData.getTitle(NavbarData.Section.HOME)} isRightmost={false} onClickFunction={NavbarData.getOnClick(NavbarData.Section.HOME)}/>
                  <NavbarItem  title={NavbarData.getTitle(NavbarData.Section.ADD_FILES)} isRightmost={false} onClickFunction={NavbarData.getOnClick(NavbarData.Section.ADD_FILES)}/>
                  <NavbarItem  title={NavbarData.getTitle(NavbarData.Section.MANAGE)} isRightmost={false} onClickFunction={NavbarData.getOnClick(NavbarData.Section.MANAGE)}/>
                  <NavbarItem  title={NavbarData.getTitle(NavbarData.Section.EXPLORE)} isRightmost={true} onClickFunction={NavbarData.getOnClick(NavbarData.Section.EXPLORE)}/>
            </div>;
    }
}
