import * as React from "react";
import * as ReactDOM from "react-dom";
import { Header } from "../../../components/header/Header"
import { ContentContainer } from "../../../components/homepage/container/ContentContainer"
import { AddFilesContentContainer } from "../../../components/addfiles/container/AddFilesContentContainer"
import { ManageContentContainer } from "../../../components/manage/container/ManageContentContainer"
import { ExploreContainer } from "../../../components/explore/container/ExploreContainer"


export const enum Section {
    HOME,
    ADD_FILES,
    MANAGE,
    EXPLORE
};

const labelMap: Map<Section, string> = new Map([[Section.HOME, "Home"],
                                                [Section.ADD_FILES, "Add files"],
                                                [Section.MANAGE, "Manage"],
                                                [Section.EXPLORE, "Explore"]]);

function onClickHome() : void {
    ReactDOM.render(
        <Header sectionTitle="Spending summaries"/>,
        document.getElementById("headerContainerTarget")
    );
    ReactDOM.render(
        <ContentContainer title="Your summaries" spendingSummaries={null} />,
        document.getElementById("contentContainerTarget")
    );
}

function onClickAddFiles() : void {
    ReactDOM.render(
        <Header sectionTitle="Upload files"/>,
        document.getElementById("headerContainerTarget")
    );
    ReactDOM.render(
        <AddFilesContentContainer />,
        document.getElementById("contentContainerTarget")
    );
}

function onClickManageContent() : void {
    ReactDOM.render(
        <Header sectionTitle="Manage data"/>,
        document.getElementById("headerContainerTarget")
    );
    ReactDOM.render(
        <ManageContentContainer />,
        document.getElementById("contentContainerTarget")
    );
}

function onClickExplore() : void {
    ReactDOM.render(
        <Header sectionTitle="Explore"/>,
        document.getElementById("headerContainerTarget")
    );
    ReactDOM.render(
        <ExploreContainer />,
        document.getElementById("contentContainerTarget")
    );
}

function testFunction() : void {
    console.log("Executing on click");
}

const onClickMap: Map<Section, Function> = new Map([[Section.HOME, onClickHome],
                                                    [Section.ADD_FILES, onClickAddFiles],
                                                    [Section.MANAGE, onClickManageContent],
                                                    [Section.EXPLORE, onClickExplore]])

export function getTitle(section: Section) : string {
    return labelMap.get(section);
}

export function getOnClick(section: Section) : Function {
    return onClickMap.get(section);
}


















