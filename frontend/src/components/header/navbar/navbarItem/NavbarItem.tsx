import * as React from "react";
import { Columns } from "react-bulma-components";

const localStyles = require('./NavbarItem.module.less');

export interface NavbarItemProps { title: string; onClickFunction: Function, isRightmost: boolean}

export class NavbarItem extends React.Component<NavbarItemProps, {}> {
    render() {
        const additionalClass: string = this.props.isRightmost ? " " + localStyles.end : "";
        const className: string = localStyles.navbarItem + additionalClass;

        return <span className={className} onClick={(e) => this.props.onClickFunction()}>{this.props.title}</span>;
    }
}
