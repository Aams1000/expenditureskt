import * as React from "react";
import { Columns } from "react-bulma-components";
import { Navbar } from "./navbar/Navbar"

const localStyles = require('./Header.module.less');

export interface HeaderProps { sectionTitle: string; }

export class Header extends React.Component<HeaderProps, { sectionTitle: string; }> {
    render() {
        return <div id="headerContainer" className={localStyles.headerContainer}>
            <Columns className={localStyles.headerGrid}>
              <Columns.Column size={3}>
                  <div id="header" className={localStyles.header}>
                      <span className={localStyles.welcomeText}>{this.props.sectionTitle}</span>
                    </div>
              </Columns.Column>
                <Columns.Column size={4} className={localStyles.navbarColumnContainer}>
                    <Navbar />
              </Columns.Column>
            </Columns>
        </div>;
    }
}
