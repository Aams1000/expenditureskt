export class AddCategoryFunctions {

    addCategory: () => void;
    updateCategoryString: (pattern: string) => void;
    constructor(addCategory: () => void,
                updateCategoryString: (pattern: string) => void) {
                this.addCategory = addCategory;
                this.updateCategoryString = updateCategoryString;
    }
}

