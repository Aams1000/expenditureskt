export class CategoriesState {

    checkedCategories: Set<string>;
    categoriesToReplacements: Map<string, string>;
    batchReplacementCategory: string;
    addedCategoryString: string;
    shouldDisplay: boolean;

    constructor(checkedCategories: Set<string>,
                categoriesToReplacements: Map<string, string>,
                batchReplacementCategory: string,
                addedCategoryString: string,
                shouldDisplay: boolean) {
        this.checkedCategories = checkedCategories;
        this.categoriesToReplacements = categoriesToReplacements;
        this.batchReplacementCategory = batchReplacementCategory;
        this.addedCategoryString = addedCategoryString;
        this.shouldDisplay = shouldDisplay;
    }

    toggleDisplay() : CategoriesState {
         this.shouldDisplay = !this.shouldDisplay;
         return this;
    }

    static generateDefault(): CategoriesState {
        return new CategoriesState(new Set(),
                                    new Map(),
                                    null,
                                    null,
                                    false);
        }
}
