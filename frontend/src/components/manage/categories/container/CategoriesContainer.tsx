import * as React from "react";
import { Columns } from "react-bulma-components";
import { CategoryAndButtons } from "../categoryandbuttons/CategoryAndButtons";
import { AddCategory } from "../addcategory/AddCategory";
import { AddCategoryFunctions } from "../AddCategoryFunctions";
import { ManageCategoriesFunctions } from "../ManageCategoriesFunctions";
import { Autocomplete } from "../../../common/autocomplete/Autocomplete";
import { DisplayToggle } from "../../../common/displaytoggle/DisplayToggle";
import * as ManageCategoriesHeaders from "../ManageCategoriesHeaders";

const localStyles = require('./CategoriesContainer.module.less');

export interface CategoriesContainerProps { categories: Array<string>;
                                            categoriesAndBoxStatus: Array<[string, boolean]>;
                                            categoriesToReplacements: Map<string, string>;
                                            isDisplayed: boolean;
                                            addedCategoryString: string;
                                            addCategoryFunctions: AddCategoryFunctions;
                                            manageCategoriesFunctions: ManageCategoriesFunctions; }

export class CategoriesContainer extends React.Component<CategoriesContainerProps, {}> {

    generateContent = () : Array<JSX.Element> => {
        let content: Array<JSX.Element> = new Array();
        const addCategory: JSX.Element = <AddCategory category={this.props.addedCategoryString}
                                                  parentFunctions={this.props.addCategoryFunctions}
                                                  />;
          this.props.categoriesAndBoxStatus.forEach((pair: [string, boolean]) => content.push(<CategoryAndButtons category={pair[0]}
                                                                                                        isChecked={pair[1]}
                                                                                                        replacementCategory={this.props.categoriesToReplacements.get(pair[0])}
                                                                                                        categories={this.props.categories}
                                                                                                        parentFunctions={this.props.manageCategoriesFunctions}/>));
        const newContent: Array<JSX.Element> = new Array<JSX.Element>();
        newContent.push(<div className={localStyles.categoriesContainer}>
                          <DisplayToggle isContentDisplayed={this.props.isDisplayed}
                                                 showContentMessage="Show categories"
                                                 hideContentMessage="Hide categories"
                                                 onToggle={this.props.manageCategoriesFunctions.toggleDisplay}/>
                            {this.props.isDisplayed && 
                              <>
                              {addCategory}
                              <table className={localStyles.table}>
                                 <caption>
                                   <a>{ManageCategoriesHeaders.TABLE_HEADER}</a>
                                   <div className={localStyles.buttonAndAutoCompleteContainer}>
                                     <div className={localStyles.innerCaptionContainer}>
                                       <Autocomplete options={this.props.categories}
                                                           extraTopMargin={true}
                                                           extraLeftMargin={true}
                                                             parentUpdateSelection={this.props.manageCategoriesFunctions.updateBatchReplacementCategory}
                                                             parentConfirmChoiceAndProceed={this.props.manageCategoriesFunctions.batchUpdateAndReplaceCategories} />
                                             <button className={localStyles.updateSelectedButton} onClick={this.props.manageCategoriesFunctions.batchUpdateAndReplaceCategories}>{ManageCategoriesHeaders.DELETE_AND_REPLACE_SELECTED}</button>
                                             <button className={localStyles.deleteRuleButton} onClick={this.props.manageCategoriesFunctions.batchDeleteCategories}>{ManageCategoriesHeaders.DELETE_SELECTED}</button>
                                       </div>
                                     </div>
                                 </caption>
                                 <thead>
                                   <tr>
                                     <th scope="col" className={localStyles.batchSelectCell}>
                                     </th>
                                     <th scope="col">{ManageCategoriesHeaders.CATEGORY}</th>
                                     <th scope="col">{ManageCategoriesHeaders.REPLACEMENT_CATEGORY}</th>
                                     <th scope="col">{ManageCategoriesHeaders.OPTIONS}</th>
                                   </tr>
                                 </thead>
                                 <tbody>
                                   {content}
                                 </tbody>
                             </table>
                             </>
                           }
                         </div>);

        content = newContent;
        return content;
    }

    public render() {
        return this.generateContent();
    }
}
