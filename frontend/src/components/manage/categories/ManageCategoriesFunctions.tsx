export class ManageCategoriesFunctions {

    deleteCategory: (category: string) => void;
    deleteAndReplaceCategory: (category: string) => void;
    parentUpdateCheckbox: (category: string) => void;
    updateReplacementCategory: (category: string, replacementCategory: string) => void;
    updateBatchReplacementCategory: (replacementCategory: string) => void;
    batchDeleteCategories: () => void;
    batchUpdateAndReplaceCategories: () => void;
    toggleDisplay: () => void;

    constructor(deleteCategory: (category: string) => void,
                deleteAndReplaceCategory: (category: string) => void,
                parentUpdateCheckbox: (category: string) => void,
                updateReplacementCategory: (category: string, replacementCategory: string) => void,
                updateBatchReplacementCategory: (replacementCategory: string) => void,
                batchDeleteCategories: () => void,
                batchUpdateAndReplaceCategories: () => void,
                toggleDisplay: () => void) {
        this.deleteCategory = deleteCategory;
        this.deleteAndReplaceCategory = deleteAndReplaceCategory;
        this.parentUpdateCheckbox = parentUpdateCheckbox;
        this.updateReplacementCategory = updateReplacementCategory
        this.updateBatchReplacementCategory = updateBatchReplacementCategory;
        this.batchDeleteCategories = batchDeleteCategories;
        this.batchUpdateAndReplaceCategories = batchUpdateAndReplaceCategories;
        this.toggleDisplay = toggleDisplay;
    }
}

