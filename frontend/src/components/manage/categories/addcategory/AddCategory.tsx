import * as React from "react";
import { Columns } from "react-bulma-components";
import { Autocomplete } from "../../../common/autocomplete/Autocomplete";
import { AddCategoryFunctions } from "../AddCategoryFunctions"
import * as AddCategoryHeaders from "../AddCategoryHeaders";

const localStyles = require('./AddCategory.module.less');

export interface AddCategoryProps { category: string;
                                    parentFunctions: AddCategoryFunctions; }

export class AddCategory extends React.Component<AddCategoryProps, {}> {

    updateCategoryString = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.props.parentFunctions.updateCategoryString(event.target.value);
    }

    public render() {
      const displayCategory: string = this.props.category !== null ? this.props.category : "";
      return <div className={localStyles.addCategoryContainer}>
                <table className={localStyles.table}>
                   <caption>
                     <a>{AddCategoryHeaders.ADD_CATEGORY_TABLE_HEADER}</a>
                   </caption>
                   <thead>
                     <tr>
                       <th scope="col">{AddCategoryHeaders.CATEGORY}</th>
                       <th scope="col">{AddCategoryHeaders.ADD_CATEGORY}</th>
                     </tr>
                   </thead>
                   <tbody>
                   <tr>
                    <td scope="row" data-label={AddCategoryHeaders.CATEGORY}>
                      <input
                        type="text"
                        className={localStyles.categoryInputBox}
                        onChange={this.updateCategoryString}
                        value={displayCategory}
                      />
                    </td>
                   <td scope="row" className={localStyles.buttonCell} data-label={AddCategoryHeaders.ADD_CATEGORY}>
                        <button className={localStyles.addCategoryButton} onClick={this.props.parentFunctions.addCategory}>{AddCategoryHeaders.ADD_CATEGORY}</button>
                   </td>
                </tr>
                </tbody>
               </table>
               </div>

    }
}

