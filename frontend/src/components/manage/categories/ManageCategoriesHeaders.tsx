export const TABLE_HEADER: string = "Categories";
export const CATEGORY: string = "Category";
export const REPLACEMENT_CATEGORY: string = "Replacement category";
export const OPTIONS: string = "Options";
export const DELETE_AND_REPLACE_CATEGORY: string = "Replace category";
export const DELETE_CATEGORY: string = "Delete category";
export const DELETE_SELECTED: string = "Delete selected categories"
export const DELETE_AND_REPLACE_SELECTED: string = "Replace selected categories";
