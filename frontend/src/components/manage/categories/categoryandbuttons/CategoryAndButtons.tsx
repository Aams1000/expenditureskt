import * as React from "react";
import { Columns } from "react-bulma-components";
import { Autocomplete } from "../../../common/autocomplete/Autocomplete";
import { ManageCategoriesFunctions } from "../ManageCategoriesFunctions";
import * as ManageCategoriesHeaders from "../ManageCategoriesHeaders";

const localStyles = require('./CategoryAndButtons.module.less');
const UNKNOWN_CATEGORY: string = "Unknown";

export interface CategoryAndButtonsProps { category: string;
                                        replacementCategory: string;
                                        isChecked: boolean;
                                        categories: Array<string>;
                                        parentFunctions: ManageCategoriesFunctions; }

export class CategoryAndButtons extends React.Component<CategoryAndButtonsProps, {}> {

    deleteCategory = () : void => {
      this.props.parentFunctions.deleteCategory(this.props.category);
    }

    deleteAndReplaceCategory = () : void => {
        this.props.parentFunctions.deleteAndReplaceCategory(this.props.category);
    }

    updateCheckbox = () : void => {
        this.props.parentFunctions.parentUpdateCheckbox(this.props.category);
    }

    updateReplacementCategory = (category: string) : void => {
      this.props.parentFunctions.updateReplacementCategory(this.props.category, category);
    }

    public render() {
        const isUnknownCategory: boolean = this.props.category === UNKNOWN_CATEGORY;
        const replacementCategoryAutocomplete: JSX.Element = isUnknownCategory
                                                              ? null
                                                              : <Autocomplete options={this.props.categories} 
                                                                                         extraTopMargin={true}
                                                                                         extraLeftMargin={false}
                                                                                         parentUpdateSelection={this.updateReplacementCategory}
                                                                                         parentConfirmChoiceAndProceed={this.deleteAndReplaceCategory} />
        const deleteButtonStyle: string = isUnknownCategory ? localStyles.disabledButton : localStyles.deleteCategoryButton;
        const deleteAndReplaceButtonStyle: string = isUnknownCategory ? localStyles.disabledButton : localStyles.deleteAndReplaceCategoryButton;
        const deleteButtonFunction: () => void = isUnknownCategory ? () => {} : this.deleteCategory;
        const deleteAndReplaceButtonFunction: () => void = isUnknownCategory ? () => {} : this.deleteAndReplaceCategory;

        return <tr>
                   <td scope="row" data-label="">
                    <input name="isSelected" type="checkbox" checked={this.props.isChecked} onChange={this.updateCheckbox} />
                  </td>
                    <td scope="row" data-label={ManageCategoriesHeaders.CATEGORY}>{this.props.category}</td>
                    <td scope="row" data-label={ManageCategoriesHeaders.REPLACEMENT_CATEGORY}>
                      {replacementCategoryAutocomplete}
                    </td>
                    <td scope="row" className={localStyles.buttonCell} data-label={ManageCategoriesHeaders.OPTIONS}>
                        <button className={deleteAndReplaceButtonStyle} onClick={deleteAndReplaceButtonFunction}>{ManageCategoriesHeaders.DELETE_AND_REPLACE_CATEGORY}</button>
                        <button className={deleteButtonStyle} onClick={deleteButtonFunction}>{ManageCategoriesHeaders.DELETE_CATEGORY}</button>
                   </td>
                </tr>;
    }
}

