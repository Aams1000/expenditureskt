export class AddRuleFunctions {

    addRule: () => void;
    updateRuleCategory: (category: string) => void;
    updateRuleString: (pattern: string) => void;
    constructor(addRule: () => void,
                updateRuleCategory: (category: string) => void,
                updateRuleString: (pattern: string) => void) {
                this.addRule = addRule;
                this.updateRuleCategory = updateRuleCategory;
                this.updateRuleString = updateRuleString;
    }
}

