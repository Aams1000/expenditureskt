export class AddRuleContent {

    rule: string;
    category: string;

    constructor(rule: string,
                category: string) {
        this.rule = rule;
        this.category = category;
    }

    static generateDefault(): AddRuleContent {
        return new AddRuleContent(null, null);
    }
}
