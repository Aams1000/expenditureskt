export class MerchantRulesState {

     merchantRules: Map<string, string>;
     checkedRules: Set<string>;
     categorySelectionsByRule: Map<string, string>;
     batchRuleCategory: string;
     shouldDisplay: boolean;

     constructor(merchantRules: Map<string, string>,
                 checkedRules: Set<string>,
                 categorySelectionsByRule: Map<string, string>,
                 batchRuleCategory: string,
                 shouldDisplay: boolean) {
        this.merchantRules = merchantRules;
        this.checkedRules = checkedRules;
        this.categorySelectionsByRule = categorySelectionsByRule;
        this.batchRuleCategory = batchRuleCategory;
        this.shouldDisplay = shouldDisplay;
     }

     toggleDisplay() : MerchantRulesState {
         this.shouldDisplay = !this.shouldDisplay;
         return this;
     }

     static from(other: MerchantRulesState) : MerchantRulesState {
         return new MerchantRulesState(other.merchantRules,
                                          other.checkedRules,
                                          other.categorySelectionsByRule,
                                          other.batchRuleCategory,
                                          other.shouldDisplay); 
     }

     static generateState(merchantRules: Map<string, string>) : MerchantRulesState {
         const defaultState: MerchantRulesState = this.generateDefault();
         defaultState.merchantRules = merchantRules;
         return defaultState;
     }

     static generateDefault() : MerchantRulesState {
         return new MerchantRulesState(new Map<string, string>(),
                                          new Set<string>(),
                                          new Map<string, string>(),
                                          null,
                                          false);
     }

     static removeRules(state: MerchantRulesState, rulesToRemove: Set<string>) : MerchantRulesState {
         const newRules: Map<string, string> = state.merchantRules;
         const newCheckedRules: Set<string> = state.checkedRules;
         const newCategorySelectionsByRule: Map<string, string> = state.categorySelectionsByRule;
         rulesToRemove.forEach(rule => {
           newRules.delete(rule);
           newCheckedRules.delete(rule);
           newCategorySelectionsByRule.delete(rule);
         });
         return new MerchantRulesState(newRules, newCheckedRules, newCategorySelectionsByRule, state.batchRuleCategory, state.shouldDisplay);
     }
}



