import * as React from "react";
import { Columns } from "react-bulma-components";
import { Autocomplete } from "../../../common/autocomplete/Autocomplete";
import { MerchantRulesFunctions } from "../MerchantRulesFunctions";
import * as MerchantRulesHeaders from "../MerchantRulesHeaders";

const localStyles = require('./RuleAndButtons.module.less');
const testOptions: Array<string> = new Array("one", "two", "three");


export interface RuleAndButtonsProps { rule: string;
                                        currentCategory: string;
                                        isChecked: boolean;
                                        categories: Array<string>;
                                        parentFunctions: MerchantRulesFunctions; }

export class RuleAndButtons extends React.Component<RuleAndButtonsProps, {}> {

    deleteRule = () : void => {
      this.props.parentFunctions.onRuleDelete(this.props.rule);
    }

    updateCheckbox = () : void => {
        this.props.parentFunctions.parentUpdateCheckbox(this.props.rule);
    }

    updateRule = () : void => {
        this.props.parentFunctions.onRuleUpdate(this.props.rule);
    }

    updateCategory = (value: string) : void => {
        this.props.parentFunctions.onCategoryUpdate(this.props.rule, value);
    }

    public render() {
        return <tr>
                   <td scope="row" data-label="">
                    <input name="isSelected" type="checkbox" checked={this.props.isChecked} onChange={this.updateCheckbox} />
                  </td>
                                                         {/*<th scope="col">{MerchantRulesHeaders.RULE_SUBSTRING}</th>
                                       <th scope="col">{MerchantRulesHeaders.CATEGORY}</th>
                                       <th scope="col">{MerchantRulesHeaders.OPTIONS}</th>*/}
                    <td scope="row" data-label={MerchantRulesHeaders.RULE_SUBSTRING}>{this.props.rule}</td>
                    <td scope="row" data-label={MerchantRulesHeaders.CATEGORY}>{this.props.currentCategory}</td>
                    {/*<td scope="row" data-label={MerchantRulesHeaders.OPTIONS}>{}</td>*/}
                   <td scope="row" className={localStyles.buttonCell} data-label={MerchantRulesHeaders.OPTIONS}>
                       <Autocomplete options={this.props.categories} 
                                   extraTopMargin={true}
                                   extraLeftMargin={false}
                                   parentUpdateSelection={this.updateCategory}
                                   parentConfirmChoiceAndProceed={this.updateRule} />
                        <button className={localStyles.setCategoryButton} onClick={this.updateRule}>{MerchantRulesHeaders.UPDATE_RULE}</button>
                        <button className={localStyles.deleteRuleButton} onClick={this.deleteRule}>{MerchantRulesHeaders.DELETE_RULE}</button>
                   </td>
                </tr>;
    }
}



