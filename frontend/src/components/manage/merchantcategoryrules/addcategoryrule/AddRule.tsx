import * as React from "react";
import { Columns } from "react-bulma-components";
import { Autocomplete } from "../../../common/autocomplete/Autocomplete";
import { AddRuleFunctions } from "../AddRuleFunctions";
import * as AddRuleHeaders from "../AddRuleHeaders";

const localStyles = require('./AddRule.module.less');

export interface AddRuleProps { rule: string;
                                category: string;
                                categories: Array<string>;
                                parentFunctions: AddRuleFunctions; }

export class AddRule extends React.Component<AddRuleProps, {}> {

    updateRuleString = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.props.parentFunctions.updateRuleString(event.target.value);
    }

    public render() {
      const displayRule: string = this.props.rule !== null ? this.props.rule : "";
      const displayCategory: string = this.props.category !== null ? this.props.category : "";
        return <div className={localStyles.addRuleContainer}>
                  <table className={localStyles.table}>
                     <caption>
                       <a>{AddRuleHeaders.ADD_RULE_TABLE_HEADER}</a>
                     </caption>
                     <thead>
                       <tr>
                         <th scope="col">{AddRuleHeaders.RULE_SUBSTRING}</th>
                         <th scope="col">{AddRuleHeaders.CATEGORY}</th>
                         <th scope="col">{AddRuleHeaders.ADD_RULE}</th>
                       </tr>
                     </thead>
                     <tbody>
                     <tr>
                      <td scope="row" data-label={AddRuleHeaders.RULE_SUBSTRING}>
                        <input
                          type="text"
                          className={localStyles.ruleInputBox}
                          onChange={this.updateRuleString}
                          value={displayRule}
                        />
                      </td>
                      <td scope="row" data-label={AddRuleHeaders.CATEGORY}>
                          <Autocomplete options={this.props.categories} 
                                       extraTopMargin={true}
                                       extraLeftMargin={false}
                                       parentUpdateSelection={this.props.parentFunctions.updateRuleCategory}
                                       parentConfirmChoiceAndProceed={() => {}} />
                      </td>
                     <td scope="row" className={localStyles.buttonCell} data-label={AddRuleHeaders.ADD_RULE}>
                          <button className={localStyles.addRuleButton} onClick={this.props.parentFunctions.addRule}>{AddRuleHeaders.ADD_RULE}</button>
                     </td>
                  </tr>
                  </tbody>
                 </table>
                 </div>
                  ;
    }
}

