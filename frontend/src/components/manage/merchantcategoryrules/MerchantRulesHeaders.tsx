export const TABLE_HEADER: string = "Merchant rules";
export const NO_RULES: string = "There are no merchant rules";
export const DELETE_RULE: string = "Delete rule";
export const UPDATE_RULE: string = "Update category";
export const UPDATE_BATCH_CATEGORY: string = "Update selected rules";
export const DELETE_BATCH_RULES: string = "Delete selected rules";
export const RULE_SUBSTRING: string = "Contains";
export const CATEGORY: string = "Category";
export const OPTIONS: string = "Options";

