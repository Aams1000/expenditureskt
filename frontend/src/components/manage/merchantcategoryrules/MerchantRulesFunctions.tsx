export class MerchantRulesFunctions {

    parentUpdateCheckbox: (name: string) => void;
    onCategoryUpdate: (rule: string, category: string) => void;
    onRuleUpdate: (rule: string) => void;
    onBatchUpdate: () => void;
    onBatchCategoryUpdate: (rule: string) => void;
    onRuleDelete: (rule: string) => void;
    onBatchDelete: () => void;
    toggleDisplay: () => void;

    constructor(parentUpdateCheckbox: (name: string) => void,
                onCategoryUpdate: (rule: string, category: string) => void,
                onRuleUpdate: (rule: string) => void,
                onBatchUpdate: () => void,
                onBatchCategoryUpdate: (rule: string) => void,
                onRuleDelete: (rule: string) => void,
                onBatchDelete: () => void,
                toggleDisplay: () => void) {

                this.parentUpdateCheckbox = parentUpdateCheckbox;
                this.onCategoryUpdate = onCategoryUpdate;
                this.onRuleUpdate = onRuleUpdate;
                this.onBatchUpdate = onBatchUpdate;
                this.onBatchCategoryUpdate = onBatchCategoryUpdate;
                this.onRuleDelete = onRuleDelete;
                this.onBatchDelete = onBatchDelete;
                this.toggleDisplay = toggleDisplay;
    }
}

