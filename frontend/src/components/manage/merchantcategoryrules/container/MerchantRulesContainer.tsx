import * as React from "react";
import { Columns } from "react-bulma-components";
import { RuleAndButtons } from "../ruleandbuttons/RuleAndButtons";
import { AddRule } from "../addcategoryrule/AddRule";
import { AddRuleContent } from "../AddRuleContent";
import { AddRuleFunctions } from "../AddRuleFunctions";
import { MerchantRulesFunctions } from "../MerchantRulesFunctions";
import { Autocomplete } from "../../../common/autocomplete/Autocomplete";
import { DisplayToggle } from "../../../common/displaytoggle/DisplayToggle";
import * as MerchantRulesHeaders from "../MerchantRulesHeaders";

const localStyles = require('./MerchantRulesContainer.module.less');

export interface MerchantRulesContainerProps { rulesByCategory: Map<string, string>;
                                                rulesAndBoxStatus: Array<[string, boolean]>;
                                                isDisplayed: boolean;
                                                categories: Array<string>;
                                                addRuleContent: AddRuleContent;
                                                addRuleFunctions: AddRuleFunctions;
                                                merchantRulesFunctions: MerchantRulesFunctions; }

export class MerchantRulesContainer extends React.Component<MerchantRulesContainerProps, {}> {

    generateContent = () : Array<JSX.Element> => {
        let content: Array<JSX.Element> = new Array();
        const addRule: JSX.Element = <AddRule rule={this.props.addRuleContent.rule}
                                              category={this.props.addRuleContent.category}
                                              categories={this.props.categories}
                                              parentFunctions={this.props.addRuleFunctions}
                                              />;
        if (this.props.rulesAndBoxStatus.length === 0) {
            content.push(<div className={localStyles.merchantRulesContainer}>
                              {addRule}
                              <span className={localStyles.descriptionText}>{MerchantRulesHeaders.NO_RULES}</span>
                            </div>);
        }
        else {
            this.props.rulesAndBoxStatus.forEach((pair: [string, boolean]) => content.push(<RuleAndButtons rule={pair[0]}
                                                                                                          isChecked={pair[1]}
                                                                                                          currentCategory={this.props.rulesByCategory.get(pair[0])}
                                                                                                          categories={this.props.categories}
                                                                                                          parentFunctions={this.props.merchantRulesFunctions}/>));
            const newContent: Array<JSX.Element> = new Array<JSX.Element>();
            newContent.push(<div className={localStyles.merchantRulesContainer}>
                              <DisplayToggle isContentDisplayed={this.props.isDisplayed}
                                                 showContentMessage="Show merchant rules"
                                                 hideContentMessage="Hide merchant rules"
                                                 onToggle={this.props.merchantRulesFunctions.toggleDisplay}/>
                                {this.props.isDisplayed && 
                                  <>
                                  {addRule}
                                  <table className={localStyles.table}>
                                     <caption>
                                       <a>{MerchantRulesHeaders.TABLE_HEADER}</a>
                                       <div className={localStyles.buttonAndAutoCompleteContainer}>
                                         <div className={localStyles.innerCaptionContainer}>
                                           <Autocomplete options={this.props.categories}
                                                               extraTopMargin={true}
                                                               extraLeftMargin={true}
                                                                 parentUpdateSelection={this.props.merchantRulesFunctions.onBatchCategoryUpdate}
                                                                 parentConfirmChoiceAndProceed={this.props.merchantRulesFunctions.onBatchUpdate} />
                                                 <button className={localStyles.updateSelectedButton} onClick={this.props.merchantRulesFunctions.onBatchUpdate}>{MerchantRulesHeaders.UPDATE_BATCH_CATEGORY}</button>
                                                 <button className={localStyles.deleteRuleButton} onClick={this.props.merchantRulesFunctions.onBatchDelete}>{MerchantRulesHeaders.DELETE_BATCH_RULES}</button>
                                           </div>
                                         </div>
                                     </caption>
                                     <thead>
                                       <tr>
                                         <th scope="col" className={localStyles.batchSelectCell}>
                                         </th>
                                         <th scope="col">{MerchantRulesHeaders.RULE_SUBSTRING}</th>
                                         <th scope="col">{MerchantRulesHeaders.CATEGORY}</th>
                                         <th scope="col">{MerchantRulesHeaders.OPTIONS}</th>
                                       </tr>
                                     </thead>
                                     <tbody>
                                       {content}
                                     </tbody>
                                 </table>
                                 </>
                               }
                             </div>);

            content = newContent;
        }
        return content;
    }

    public render() {
        return this.generateContent();
    }
}
