import * as React from "react";
import { Columns } from "react-bulma-components";
import { EntriesContainer } from "../../common/entries/container/EntriesContainer";
import { Expenditure } from "../../common/entries/Expenditure";
import { MerchantsContainer } from "../../common/merchants/container/MerchantsContainer";
import * as MapUtils from "../../common/MapUtils";
import { EntriesFunctions } from "../../common/entries/EntriesFunctions";
import { MerchantsFunctions } from "../../common/merchants/MerchantsFunctions";
import { ManageContainerContent } from "../container/ManageContainerContent";
import * as MerchantsHeaders from "../../common/merchants/MerchantsHeaders";
import * as ExpendituresHeaders from "../../common/entries/ExpendituresHeaders";
import * as CheckboxSetup from "../../common/containers/CheckboxSetup";
import { IgnoredMerchantsContainer } from "../ignoredMerchants/container/IgnoredMerchantsContainer";
import { IgnoredMerchantsFunctions } from "../ignoredMerchants/IgnoredMerchantsFunctions";
import { MerchantRulesFunctions } from "../merchantcategoryrules/MerchantRulesFunctions";
import { MerchantRulesContainer } from "../merchantcategoryrules/container/MerchantRulesContainer";
import { AddRuleFunctions } from "../merchantcategoryrules/AddRuleFunctions";
import { CategoriesContainer } from "../categories/container/CategoriesContainer";
import { ManageCategoriesFunctions } from "../categories/ManageCategoriesFunctions";
import { AddCategoryFunctions } from "../categories/AddCategoryFunctions";
import { IgnoreRulesContainer } from "../ignoredMerchants/rules/container/IgnoreRulesContainer";
import { AddIgnoreRuleFunctions } from "../ignoredMerchants/rules/AddIgnoreRuleFunctions";
import { IgnoredMerchantRulesFunctions } from "../ignoredMerchants/rules/IgnoredMerchantRulesFunctions";

const localStyles = require('./ManageContentWrapper.module.less');

export interface ManageContentWrapperProps { title: string;
                                            categories: Array<string>;
                                            data: ManageContainerContent;
                                            merchantsFunctions: MerchantsFunctions;
                                            entriesFunctions: EntriesFunctions;
                                            ignoredMerchantsFunctions: IgnoredMerchantsFunctions;
                                            merchantRulesFunctions: MerchantRulesFunctions;
                                            addRuleFunctions: AddRuleFunctions,
                                            manageCategoriesFunctions: ManageCategoriesFunctions;
                                            addCategoryFunctions: AddCategoryFunctions;
                                            addIgnoreRuleFunctions: AddIgnoreRuleFunctions;
                                            ignoredMerchantRulesFunctions: IgnoredMerchantRulesFunctions }

export class ManageContentWrapper extends React.Component<ManageContentWrapperProps, {}> {

    public render() {
//         console.log("Rendering ManageContentWrapper");
        return <div className={localStyles.resultsContainer}>
                        <div className={localStyles.categoriesContainer}>
                            <Columns className={localStyles.fullWidthColumns}>
                              <Columns.Column size={12}>
                                  <CategoriesContainer
                                      categories={this.props.categories}
                                            categoriesAndBoxStatus={CheckboxSetup.generateEntitiesAndStatus(Array.from(this.props.categories), this.props.data.categoriesState.checkedCategories)}
                                            categoriesToReplacements={this.props.data.categoriesState.categoriesToReplacements}
                                            isDisplayed={this.props.data.categoriesState.shouldDisplay}
                                            addedCategoryString={this.props.data.categoriesState.addedCategoryString}
                                            addCategoryFunctions={this.props.addCategoryFunctions}
                                            manageCategoriesFunctions={this.props.manageCategoriesFunctions}
                                  />
                              </Columns.Column>
                            </Columns>
                        </div>
                        <div className={localStyles.merchantRulesContainer}>
                            <Columns className={localStyles.fullWidthColumns}>
                              <Columns.Column size={12}>
                                  <MerchantRulesContainer
                                      rulesByCategory={this.props.data.merchantRulesState.merchantRules}
                                      rulesAndBoxStatus={CheckboxSetup.generateEntitiesAndStatus(Array.from(this.props.data.merchantRulesState.merchantRules.keys()), this.props.data.merchantRulesState.checkedRules)}
                                      isDisplayed={this.props.data.merchantRulesState.shouldDisplay}
                                      categories={this.props.categories}
                                      addRuleContent={this.props.data.addRuleContent}
                                      addRuleFunctions={this.props.addRuleFunctions}
                                      merchantRulesFunctions={this.props.merchantRulesFunctions}
                                  />
                              </Columns.Column>
                            </Columns>
                        </div>

                        <div className={localStyles.ignoredMerchantRulesContainer}>
                            <Columns className={localStyles.fullWidthColumns}>
                              <Columns.Column size={12}>
                                  <IgnoreRulesContainer
                                      rulesAndBoxStatus={CheckboxSetup.generateEntitiesAndStatus(this.props.data.ignoreRulesState.ignoreRules, this.props.data.ignoreRulesState.checkedRules)}
                                      isDisplayed={this.props.data.ignoreRulesState.shouldDisplay}
                                      addIgnoreRuleContent={this.props.data.addIgnoreRuleContent}
                                      addIgnoreRuleFunctions={this.props.addIgnoreRuleFunctions}
                                      ignoredMerchantRulesFunctions={this.props.ignoredMerchantRulesFunctions}
                                  />
                              </Columns.Column>
                            </Columns>
                        </div>

                        <div className={localStyles.ignoredMerchantsContainer}>
                            <Columns className={localStyles.fullWidthColumns}>
                              <Columns.Column size={12}>
                                  <IgnoredMerchantsContainer
                                      titles={MerchantsHeaders.IGNORED_MERCHANTS}
                                      merchantAndBoxStatus={CheckboxSetup.generateEntitiesAndStatus(this.props.data.ignoredMerchantsState.ignoredMerchants, this.props.data.ignoredMerchantsState.checkedMerchants)}
                                      isDisplayed={this.props.data.ignoredMerchantsState.shouldDisplay}
                                      categories={this.props.categories}
                                      parentFunctions={this.props.ignoredMerchantsFunctions}
                                  />
                              </Columns.Column>
                            </Columns>
                        </div>
                        <div className={localStyles.newMerchantsContainer}>
                            <Columns className={localStyles.fullWidthColumns}>
                              <Columns.Column size={12}>
                                  <MerchantsContainer
                                      titles={MerchantsHeaders.ALL_MERCHANTS}
                                      merchantsByCategory={this.props.data.entriesAndMerchantsData.merchantsByCategory}
                                      shouldShowCategory={true}
                                      merchantAndBoxStatus={CheckboxSetup.generateEntitiesAndStatus(Array.from(this.props.data.entriesAndMerchantsData.merchantsByCategory.keys()), this.props.data.entriesAndMerchantsData.checkedMerchants)}
                                      categories={this.props.categories}
                                      isDisplayed={this.props.data.entriesAndMerchantsData.shouldShowMerchants}
                                      parentFunctions={this.props.merchantsFunctions}
                                  />
                              </Columns.Column>
                            </Columns>
                        </div>
                        <div className={localStyles.entriesContainer}>
                            <Columns className={localStyles.fullWidthColumns}>
                              <Columns.Column size={12}>
                                  <EntriesContainer
                                      titles={ExpendituresHeaders.ALL_EXPENDITURES}
                                      data={CheckboxSetup.generateExpendituresAndStatus(this.props.data.entriesAndMerchantsData.expenditures, this.props.data.entriesAndMerchantsData.checkedEntries)}
                                      categories={this.props.categories}
                                      isDisplayed={this.props.data.entriesAndMerchantsData.shouldShowEntries}
                                      parentFunctions={this.props.entriesFunctions}
                                  />
                              </Columns.Column>
                            </Columns>
                        </div>
                    </div>;;
    }
}
