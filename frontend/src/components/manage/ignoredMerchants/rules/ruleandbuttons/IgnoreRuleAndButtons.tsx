import * as React from "react";
import { Columns } from "react-bulma-components";
import { Autocomplete } from "../../../../common/autocomplete/Autocomplete";
import { IgnoredMerchantRulesFunctions } from "../IgnoredMerchantRulesFunctions";
import * as IgnoredMerchantRulesHeaders from "../IgnoredMerchantRulesHeaders";

const localStyles = require('./IgnoreRuleAndButtons.module.less');
const testOptions: Array<string> = new Array("one", "two", "three");


export interface IgnoreRuleAndButtonsProps { rule: string;
                                        isChecked: boolean;
                                        parentFunctions: IgnoredMerchantRulesFunctions; }

export class IgnoreRuleAndButtons extends React.Component<IgnoreRuleAndButtonsProps, {}> {

    deleteRule = () : void => {
      this.props.parentFunctions.onRuleDelete(this.props.rule);
    }

    updateCheckbox = () : void => {
        this.props.parentFunctions.parentUpdateCheckbox(this.props.rule);
    }

    // updateRule = () : void => {
    //     this.props.parentFunctions.onRuleUpdate(this.props.rule);
    // }

    // updateCategory = (value: string) : void => {
    //     this.props.parentFunctions.onCategoryUpdate(this.props.rule, value);
    // }

    public render() {
        return <tr>
                   <td scope="row" data-label="">
                    <input name="isSelected" type="checkbox" checked={this.props.isChecked} onChange={this.updateCheckbox} />
                  </td>
                                                         {/*<th scope="col">{IgnoredMerchantRulesHeaders.RULE_SUBSTRING}</th>
                                       <th scope="col">{IgnoredMerchantRulesHeaders.CATEGORY}</th>
                                       <th scope="col">{IgnoredMerchantRulesHeaders.OPTIONS}</th>*/}
                    <td scope="row" data-label={IgnoredMerchantRulesHeaders.RULE_SUBSTRING}>{this.props.rule}</td>
                    {/*<td scope="row" data-label={IgnoredMerchantRulesHeaders.CATEGORY}>{this.props.currentCategory}</td>*/}
                    {/*<td scope="row" data-label={IgnoredMerchantRulesHeaders.OPTIONS}>{}</td>*/}
                   <td scope="row" className={localStyles.buttonCell} data-label={IgnoredMerchantRulesHeaders.OPTIONS}>
                      {/* <Autocomplete options={this.props.categories} 
                                   extraTopMargin={true}
                                   extraLeftMargin={false}
                                   parentUpdateSelection={this.updateCategory}
                                   parentConfirmChoiceAndProceed={this.updateRule} />*/}
                        {/*<button className={localStyles.setCategoryButton} onClick={this.updateRule}>{IgnoredMerchantRulesHeaders.UPDATE_RULE}</button>*/}
                        <button className={localStyles.deleteRuleButton} onClick={this.deleteRule}>{IgnoredMerchantRulesHeaders.DELETE_RULE}</button>
                   </td>
                </tr>;
    }
}



