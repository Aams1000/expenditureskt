import * as React from "react";
import { Columns } from "react-bulma-components";
import { Autocomplete } from "../../../../common/autocomplete/Autocomplete";
import { AddIgnoreRuleFunctions } from "../AddIgnoreRuleFunctions";
import * as AddIgnoreRuleHeaders from "../AddIgnoreRuleHeaders";

const localStyles = require('./AddIgnoreRule.module.less');

export interface AddIgnoreRuleProps { rule: string;
                                parentFunctions: AddIgnoreRuleFunctions; }

export class AddIgnoreRule extends React.Component<AddIgnoreRuleProps, {}> {

    updateRuleString = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.props.parentFunctions.updateRuleString(event.target.value);
    }

    public render() {
      const displayRule: string = this.props.rule !== null ? this.props.rule : "";
        return <div className={localStyles.addIgnoreRuleContainer}>
                  <table className={localStyles.table}>
                     <caption>
                       <a>{AddIgnoreRuleHeaders.ADD_RULE_TABLE_HEADER}</a>
                     </caption>
                     <thead>
                       <tr>
                         <th scope="col">{AddIgnoreRuleHeaders.RULE_SUBSTRING}</th>
                         <th scope="col">{AddIgnoreRuleHeaders.ADD_RULE}</th>
                       </tr>
                     </thead>
                     <tbody>
                     <tr>
                      <td scope="row" data-label={AddIgnoreRuleHeaders.RULE_SUBSTRING}>
                        <input
                          type="text"
                          className={localStyles.ruleInputBox}
                          onChange={this.updateRuleString}
                          value={displayRule}
                        />
                      </td>
                     <td scope="row" className={localStyles.buttonCell} data-label={AddIgnoreRuleHeaders.ADD_RULE}>
                          <button className={localStyles.addIgnoreRuleButton} onClick={this.props.parentFunctions.addRule}>{AddIgnoreRuleHeaders.ADD_RULE}</button>
                     </td>
                  </tr>
                  </tbody>
                 </table>
                 </div>
                  ;
    }
}

