export class AddIgnoreRuleFunctions {

    addRule: () => void;
    updateRuleString: (pattern: string) => void;
    constructor(addRule: () => void,
                updateRuleString: (pattern: string) => void) {
                this.addRule = addRule;
                this.updateRuleString = updateRuleString;
    }
}

