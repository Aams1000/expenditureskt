export class IgnoredMerchantRulesFunctions {

    parentUpdateCheckbox: (name: string) => void;
    onRuleDelete: (rule: string) => void;
    onBatchDelete: () => void;
    toggleDisplay: () => void;

    constructor(parentUpdateCheckbox: (name: string) => void,
                onRuleDelete: (rule: string) => void,
                onBatchDelete: () => void,
                toggleDisplay: () => void) {

                this.parentUpdateCheckbox = parentUpdateCheckbox;
                this.onRuleDelete = onRuleDelete;
                this.onBatchDelete = onBatchDelete;
                this.toggleDisplay = toggleDisplay;
    }
}

