import * as React from "react";
import { Columns } from "react-bulma-components";
import { IgnoreRuleAndButtons } from "../ruleandbuttons/IgnoreRuleAndButtons";
import { AddIgnoreRule } from "../addignorerule/AddIgnoreRule";
import { AddIgnoreRuleContent } from "../AddIgnoreRuleContent";
import { AddIgnoreRuleFunctions } from "../AddIgnoreRuleFunctions";
import { IgnoredMerchantRulesFunctions } from "../IgnoredMerchantRulesFunctions";
import { Autocomplete } from "../../../../common/autocomplete/Autocomplete";
import { DisplayToggle } from "../../../../common/displaytoggle/DisplayToggle";
import * as IgnoredMerchantRulesHeaders from "../IgnoredMerchantRulesHeaders";

const localStyles = require('./IgnoreRulesContainer.module.less');

export interface IgnoreRulesContainerProps { rulesAndBoxStatus: Array<[string, boolean]>;
                                                isDisplayed: boolean;
                                                addIgnoreRuleContent: AddIgnoreRuleContent;
                                                addIgnoreRuleFunctions: AddIgnoreRuleFunctions;
                                                ignoredMerchantRulesFunctions: IgnoredMerchantRulesFunctions; }

export class IgnoreRulesContainer extends React.Component<IgnoreRulesContainerProps, {}> {

    generateContent = () : Array<JSX.Element> => {
        let content: Array<JSX.Element> = new Array();
        const addRule: JSX.Element = <AddIgnoreRule rule={this.props.addIgnoreRuleContent.rule}
                                                    parentFunctions={this.props.addIgnoreRuleFunctions}
                                                    />;
        if (this.props.rulesAndBoxStatus.length === 0) {
            content.push(<div className={localStyles.ignoreRulesContainer}>
                              {addRule}
                              <span className={localStyles.descriptionText}>{IgnoredMerchantRulesHeaders.NO_RULES}</span>
                            </div>);
        }
        else {
            this.props.rulesAndBoxStatus.forEach((pair: [string, boolean]) => content.push(<IgnoreRuleAndButtons rule={pair[0]}
                                                                                                          isChecked={pair[1]}
                                                                                                          parentFunctions={this.props.ignoredMerchantRulesFunctions}/>));
            const newContent: Array<JSX.Element> = new Array<JSX.Element>();
            newContent.push(<div className={localStyles.ignoreRulesContainer}>
                              <DisplayToggle isContentDisplayed={this.props.isDisplayed}
                                                 showContentMessage="Show ignore rules"
                                                 hideContentMessage="Hide ignore rules"
                                                 onToggle={this.props.ignoredMerchantRulesFunctions.toggleDisplay}/>
                                {this.props.isDisplayed && 
                                  <>
                                  {addRule}
                                  <table className={localStyles.table}>
                                     <caption>
                                       <a>{IgnoredMerchantRulesHeaders.TABLE_HEADER}</a>
                                       <div className={localStyles.buttonAndAutoCompleteContainer}>
                                         <div className={localStyles.innerCaptionContainer}>
                                             <button className={localStyles.deleteRuleButton} onClick={this.props.ignoredMerchantRulesFunctions.onBatchDelete}>{IgnoredMerchantRulesHeaders.DELETE_BATCH_RULES}</button>
                                         </div>
                                       </div>
                                     </caption>
                                     <thead>
                                       <tr>
                                         <th scope="col" className={localStyles.batchSelectCell}>
                                         </th>
                                         <th scope="col">{IgnoredMerchantRulesHeaders.RULE_SUBSTRING}</th>
                                         <th scope="col">{IgnoredMerchantRulesHeaders.OPTIONS}</th>
                                       </tr>
                                     </thead>
                                     <tbody>
                                       {content}
                                     </tbody>
                                 </table>
                                 </>
                               }
                             </div>);

            content = newContent;
        }
        return content;
    }

    public render() {
        return this.generateContent();
    }
}
