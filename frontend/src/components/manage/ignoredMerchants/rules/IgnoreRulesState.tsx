export class IgnoreRulesState {

     ignoreRules: Array<string>;
     checkedRules: Set<string>;
     shouldDisplay: boolean;

     constructor(ignoreRules: Array<string>,
                 checkedRules: Set<string>,
                 shouldDisplay: boolean) {
        this.ignoreRules = ignoreRules;
        this.checkedRules = checkedRules;
        this.shouldDisplay = shouldDisplay;
     }

     toggleDisplay() : IgnoreRulesState {
         this.shouldDisplay = !this.shouldDisplay;
         return this;
     }

     static from(other: IgnoreRulesState) : IgnoreRulesState {
         return new IgnoreRulesState(other.ignoreRules,
                                          other.checkedRules,
                                          other.shouldDisplay); 
     }

     static generateState(ignoreRules: Array<string>) : IgnoreRulesState {
         const defaultState: IgnoreRulesState = this.generateDefault();
         defaultState.ignoreRules = ignoreRules;
         return defaultState;
     }

     static generateDefault() : IgnoreRulesState {
         return new IgnoreRulesState(new Array<string>(),
                                          new Set<string>(),
                                          false);
     }

     static removeRules(state: IgnoreRulesState, rulesToRemove: Set<string>) : IgnoreRulesState {
         const newRules: Array<string> = new Array<string>();
         for (const rule of state.ignoreRules) {
           if (!rulesToRemove.has(rule)) {
             newRules.push(rule);
           }
         }
         const newCheckedRules: Set<string> = state.checkedRules;
         rulesToRemove.forEach(rule => {
           newCheckedRules.delete(rule);
         });
         return new IgnoreRulesState(newRules, newCheckedRules, state.shouldDisplay);
     }
}



