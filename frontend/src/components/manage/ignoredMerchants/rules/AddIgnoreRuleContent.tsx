export class AddIgnoreRuleContent {

    rule: string;
    category: string;

    constructor(rule: string,
                category: string) {
        this.rule = rule;
        this.category = category;
    }

    static generateDefault(): AddIgnoreRuleContent {
        return new AddIgnoreRuleContent(null, null);
    }
}
