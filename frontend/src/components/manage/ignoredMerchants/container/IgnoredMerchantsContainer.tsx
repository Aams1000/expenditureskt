import * as React from "react";
import { Columns } from "react-bulma-components";
import { MerchantAndButton } from "../../../common/merchants/merchantandbutton/MerchantAndButton"
import { IgnoredMerchantsFunctions } from "../IgnoredMerchantsFunctions";
import { IgnoredMerchantAndButton } from "../ignoredmerchantandbutton/IgnoredMerchantAndButton";
import { Autocomplete } from "../../../common/autocomplete/Autocomplete";
import { DisplayToggle } from "../../../common/displaytoggle/DisplayToggle";

const localStyles = require('./IgnoredMerchantsContainer.module.less');

export interface IgnoredMerchantsContainerProps { titles: Array<string>;
                                                    merchantAndBoxStatus: Array<[string, boolean]>;
                                                    isDisplayed: boolean;
                                                    categories: Array<string>;
                                                    parentFunctions: IgnoredMerchantsFunctions; }

export class IgnoredMerchantsContainer extends React.Component<IgnoredMerchantsContainerProps, {}> {

    generateContent = () : Array<JSX.Element> => {
        let content: Array<JSX.Element> = new Array();
        if (this.props.merchantAndBoxStatus !== null) {
            if (this.props.merchantAndBoxStatus.length === 0) {
                content.push(<div className={localStyles.ignoredMerchantsContainer}>
                                  <span className={localStyles.descriptionText}>{this.props.titles[0]}</span>
                                </div>);
            }
            else {
              this.props.merchantAndBoxStatus.forEach((pair: [string, boolean]) => content.push(<IgnoredMerchantAndButton merchant={pair[0]}
                                                                                                        isChecked={pair[1]}
                                                                                                        categories={this.props.categories}
                                                                                                        titles={this.props.titles}
                                                                                                        parentFunctions={this.props.parentFunctions}/>));
                const newContent: Array<JSX.Element> = new Array<JSX.Element>();
                newContent.push(<div className={localStyles.ignoredMerchantsContainer}>
                                  <DisplayToggle isContentDisplayed={this.props.isDisplayed}
                                                 showContentMessage="Show ignored merchants"
                                                 hideContentMessage="Hide ignored merchants"
                                                 onToggle={this.props.parentFunctions.toggleDisplay}/>
                                    {this.props.isDisplayed && 
                                      <>
                                      <table className={localStyles.table}>
                                         <caption>
                                           <a>{this.props.titles[1]}</a>
                                           <div className={localStyles.buttonAndAutoCompleteContainer}>
                                             <div className={localStyles.innerCaptionContainer}>
                                               <Autocomplete options={this.props.categories}
                                                                   extraTopMargin={true}
                                                                   extraLeftMargin={true}
                                                                     parentUpdateSelection={this.props.parentFunctions.onBatchCategoryUpdate}
                                                                     parentConfirmChoiceAndProceed={this.props.parentFunctions.onBatchUpdate} />
                                                     <button className={localStyles.updateSelectedButton} onClick={this.props.parentFunctions.onBatchUpdate}>{this.props.titles[5]}</button>
                                               </div>
                                             </div>
                                         </caption>
                                         <thead>
                                           <tr>
                                             <th scope="col" className={localStyles.batchSelectCell}>
                                             </th>
                                             <th scope="col">{this.props.titles[2]}</th>
                                             <th scope="col">{this.props.titles[3]}</th>
                                             <th scope="col">{this.props.titles[4]}</th>
                                           </tr>
                                         </thead>
                                         <tbody>
                                           {content}
                                         </tbody>
                                     </table>
                                     </>
                                   }
                                   </div>);

                content = newContent;
            }
        }
        return content;
    }

    public render() {
        return this.generateContent();
    }
}
