export class IgnoredMerchantsState {

     ignoredMerchants: Array<string>;
     checkedMerchants: Set<string>;
     categorySelectionsByMerchant: Map<string, string>;
     batchMerchantCategory: string;
     shouldDisplay: boolean;

     constructor(ignoredMerchants: Array<string>,
                 checkedMerchants: Set<string>,
                 categorySelectionsByMerchant: Map<string, string>,
                 batchMerchantCategory: string,
                 shouldDisplay: boolean) {
        this.ignoredMerchants = ignoredMerchants;
        this.checkedMerchants = checkedMerchants;
        this.categorySelectionsByMerchant = categorySelectionsByMerchant;
        this.batchMerchantCategory = batchMerchantCategory;
        this.shouldDisplay = shouldDisplay;
     }

     toggleDisplay() : IgnoredMerchantsState {
         this.shouldDisplay = !this.shouldDisplay;
         return this;
     }

     static from(other: IgnoredMerchantsState) : IgnoredMerchantsState {
         return new IgnoredMerchantsState(other.ignoredMerchants,
                                          other.checkedMerchants,
                                          other.categorySelectionsByMerchant,
                                          other.batchMerchantCategory,
                                          other.shouldDisplay); 
     }

     static generateState(ignoredMerchants: Array<string>) : IgnoredMerchantsState {
         const defaultState: IgnoredMerchantsState = this.generateDefault();
         defaultState.ignoredMerchants = ignoredMerchants;
         return defaultState;
     }

     static generateDefault() : IgnoredMerchantsState {
         return new IgnoredMerchantsState(new Array<string>(),
                                          new Set<string>(),
                                          new Map<string, string>(),
                                          null,
                                          false);
     }

     static removeMerchants(state: IgnoredMerchantsState, merchantsToRemove: Set<string>) : IgnoredMerchantsState {
         const newMerchants: Array<string> = state.ignoredMerchants.filter(merchant => !merchantsToRemove.has(merchant));
         const newCheckedMerchants: Set<string> = state.checkedMerchants;
         const newCategorySelectionsByMerchant: Map<string, string> = state.categorySelectionsByMerchant;
         merchantsToRemove.forEach(merchant => {
           newCheckedMerchants.delete(merchant);
           newCategorySelectionsByMerchant.delete(merchant);
         });
         return new IgnoredMerchantsState(newMerchants, newCheckedMerchants, newCategorySelectionsByMerchant, state.batchMerchantCategory, state.shouldDisplay);
     }
}



