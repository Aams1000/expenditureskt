export class IgnoredMerchantsFunctions {
    
    parentUpdateCheckbox: (name: string) => void;
    onCategoryUpdate: (merchant: string, category: string) => void;
    onMerchantUpdate: (merchant: string) => void;
    onBatchUpdate: () => void;
    onBatchCategoryUpdate: (merchant: string) => void;
    toggleDisplay: () => void;

    constructor(parentUpdateCheckbox: (name: string) => void,
                onBatchUpdate: () => void,
                onCategoryUpdate: (merchant: string, category: string) => void,
                onMerchantUpdate: (merchant: string) => void,
                onBatchCategoryUpdate: (merchant: string) => void,
                toggleDisplay: () => void) {

        this.parentUpdateCheckbox = parentUpdateCheckbox;
        this.onCategoryUpdate = onCategoryUpdate;
        this.onMerchantUpdate = onMerchantUpdate;
        this.onBatchUpdate = onBatchUpdate;
        this.onBatchCategoryUpdate = onBatchCategoryUpdate;
        this.toggleDisplay = toggleDisplay;
    }
}

