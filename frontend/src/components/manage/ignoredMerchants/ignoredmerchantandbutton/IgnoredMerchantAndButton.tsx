import * as React from "react";
import { Columns } from "react-bulma-components";
import { Autocomplete } from "../../../common/autocomplete/Autocomplete";
import { IgnoredMerchantsFunctions } from "../IgnoredMerchantsFunctions";

const localStyles = require('./IgnoredMerchantAndButton.module.less');

export interface IgnoredMerchantAndButtonProps { merchant: string;
                                            isChecked: boolean;
                                            categories: Array<string>;
                                            titles: Array<string>;
                                            parentFunctions: IgnoredMerchantsFunctions; }

export class IgnoredMerchantAndButton extends React.Component<IgnoredMerchantAndButtonProps, {}> {

    updateCheckbox = () : void => {
        this.props.parentFunctions.parentUpdateCheckbox(this.props.merchant);
    }

    testOnSelect = (value: string) : void => {
        this.props.parentFunctions.onCategoryUpdate(this.props.merchant, value);
    }

    confirmChoiceAndProceed = () : void => {
        this.props.parentFunctions.onMerchantUpdate(this.props.merchant);
    }

    public render() {

      const newMerchantAutocomplete: JSX.Element = <Autocomplete options={this.props.categories} 
                                   extraTopMargin={false}
                                   extraLeftMargin={false}
                                   parentUpdateSelection={this.testOnSelect}
                                   parentConfirmChoiceAndProceed={this.confirmChoiceAndProceed} />;
      const allMerchantsAutocomplete: JSX.Element = <Autocomplete options={this.props.categories} 
                                   extraTopMargin={false}
                                   extraLeftMargin={false}
                                   parentUpdateSelection={this.testOnSelect}
                                   parentConfirmChoiceAndProceed={this.confirmChoiceAndProceed} />;
      const setMerchantCategoryButton: JSX.Element = <button className={localStyles.setMerchantCategoryButton} onClick={this.confirmChoiceAndProceed}>Set merchant category</button>;

      // const noCurrentCategoryAutocomplete: JSX.Element = <td data-label={this.props.titles[3]}>{autocomplete}</td>;
      // const noCurrentCategoryButtons: JSX.Element = <td scope="row" data-label={this.props.titles[4]}>
      //                                                  {setMerchantCategoryButton}
      //                                                  {ignoreMerchantButton}
      //                                              </td>;

      // const currentCategoryAutocompleteAndButtons: JSX.Element = <td scope="row" data-label={this.props.titles[4]}>
      //                                                                  {autocomplete}
      //                                                                  {setMerchantCategoryButton}
      //                                                                  {ignoreMerchantButton}
      //                                                              </td>;

        return <tr>
                   <td scope="row" data-label={""}>
                       <input name="isSelected" type="checkbox" checked={this.props.isChecked} onChange={this.updateCheckbox} />
                   </td>
                   <td scope="row" data-label={this.props.titles[2]}>{this.props.merchant}</td>
        {/*           {this.props.currentCategory !== null ? 
                      <>
                       <td scope="row" data-label={this.props.titles[3]}>{this.props.currentCategory}</td>
                       <td scope="row" className={localStyles.buttonCell} data-label={this.props.titles[4]}>
                                                                       {allMerchantsAutocomplete}
                                                                       {setMerchantCategoryButton}
                                                                       {ignoreMerchantButton}
                                                                   </td>
                      </>*/}
                      {/*:*/}
                       {/*<>*/}
                        <td data-label={this.props.titles[3]}>{newMerchantAutocomplete}</td>
                        <td scope="row" data-label={this.props.titles[4]}>
                                                       {setMerchantCategoryButton}
                                                   </td>
                       {/*</>*/}
                    {/*}*/}
                   
                </tr>;
    }
}















