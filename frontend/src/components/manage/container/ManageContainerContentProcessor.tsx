import { EntriesAndMerchantsState } from "../../common/containers/EntriesAndMerchantsState";
import * as EntriesAndMerchantsStateProcessor from "../../common/containers/EntriesAndMerchantsStateProcessor";
import { ManageContainerContent } from "./ManageContainerContent";
import { ManageServletResponse } from "./ManageServletResponse";
import { IgnoredMerchantsState } from "../ignoredMerchants/IgnoredMerchantsState";
import { MerchantRulesState } from "../merchantcategoryrules/MerchantRulesState";
import { AddRuleContent } from "../merchantcategoryrules/AddRuleContent";
import { CategoriesState } from "../categories/CategoriesState";
import { IgnoreRulesState } from "../ignoredMerchants/rules/IgnoreRulesState";
import { AddIgnoreRuleContent } from "../ignoredMerchants/rules/AddIgnoreRuleContent";


export function generateDefault() : [ManageContainerContent, Array<string>] {
    return [ManageContainerContent.generateDefault(), new Array()];
}

export function generateState(response: ManageServletResponse) : [ManageContainerContent, Array<string>] {
    const ignoredMerchantsState: IgnoredMerchantsState = IgnoredMerchantsState.generateState(response.ignoredMerchants);
    const merchantRules: MerchantRulesState = MerchantRulesState.generateState(new Map<string, string>(Object.entries(response.merchantRules)));
    const shouldDisplay: boolean = false;
    const ignoreRulesState: IgnoreRulesState = IgnoreRulesState.generateState(response.ignoredMerchantRules);
    const resultsData: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.generateState(response.allExpenditures, new Map<string, string>(Object.entries(response.merchantsByCategory)), false);
    // the AddRuleContent does not use info returned from the server. Categories should belong to the categories state object,
    // but since everything else has previous operated with categories separte from the content object, leaving it
    // like that for now (it also better mimics the home page's ContentContainer)
    const content: ManageContainerContent = new ManageContainerContent(merchantRules,
                                                                        ignoredMerchantsState,
                                                                        resultsData,
                                                                        AddRuleContent.generateDefault(),
                                                                        CategoriesState.generateDefault(),
                                                                        ignoreRulesState,
                                                                        AddIgnoreRuleContent.generateDefault());
    return [content, response.categories];
}

