import { Expenditure } from "../../common/entries/Expenditure"

export interface ManageServletResponse {
    allExpenditures: Array<Expenditure>;
    ignoredMerchants: Array<string>;
    merchantRules: Map<string, string>;
    merchantsByCategory: Map<string, string>;
    categories: Array<string>;
    ignoredMerchantRules: Array<string>;
}

