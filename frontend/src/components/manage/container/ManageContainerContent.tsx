import { EntriesAndMerchantsState } from "../../common/containers/EntriesAndMerchantsState";
import { Expenditure } from "../../common/entries/Expenditure"; 
import { IgnoredMerchantsState } from "../ignoredMerchants/IgnoredMerchantsState";
import { MerchantRulesState } from "../merchantcategoryrules/MerchantRulesState";
import { AddRuleContent } from "../merchantcategoryrules/AddRuleContent";
import { CategoriesState } from "../categories/CategoriesState";
import { IgnoreRulesState } from "../ignoredMerchants/rules/IgnoreRulesState";
import { AddIgnoreRuleContent } from "../ignoredMerchants/rules/AddIgnoreRuleContent";

export class ManageContainerContent {
    
    ignoredMerchantsState: IgnoredMerchantsState;
    merchantRulesState: MerchantRulesState;
    entriesAndMerchantsData: EntriesAndMerchantsState;
    addRuleContent: AddRuleContent;
    categoriesState: CategoriesState;
    ignoreRulesState: IgnoreRulesState;
    addIgnoreRuleContent: AddIgnoreRuleContent;

    constructor(merchantRulesState: MerchantRulesState,
                ignoredMerchantsState: IgnoredMerchantsState,
                resultsData: EntriesAndMerchantsState,
                addRuleContent: AddRuleContent,
                categoriesState: CategoriesState,
                ignoreRulesState: IgnoreRulesState,
                addIgnoreRuleContent: AddIgnoreRuleContent) {
        this.ignoredMerchantsState = ignoredMerchantsState;
        this.merchantRulesState = merchantRulesState;
        this.entriesAndMerchantsData = resultsData;
        this.addRuleContent = addRuleContent;
        this.categoriesState = categoriesState;
        this.ignoreRulesState = ignoreRulesState;
        this.addIgnoreRuleContent = addIgnoreRuleContent;
    }

    static generateDefault() : ManageContainerContent {
        return new ManageContainerContent(MerchantRulesState.generateDefault(),
                                          IgnoredMerchantsState.generateDefault(),
                                          EntriesAndMerchantsState.generateDefault(),
                                          AddRuleContent.generateDefault(),
                                          CategoriesState.generateDefault(),
                                          IgnoreRulesState.generateDefault(),
                                          AddIgnoreRuleContent.generateDefault());
    }
}

