import * as React from "react";
import { Columns } from "react-bulma-components";
import { FileResultsContainer } from "../../addfiles/fileresultscontainer/FileResultsContainer"
import { ManageContainerContent } from "./ManageContainerContent";
import { ManageServletResponse } from "./ManageServletResponse";
import * as MapUtils from "../../common/MapUtils";
import * as ManageContainerContentProcessor from "./ManageContainerContentProcessor";
import { Expenditure } from "../../common/entries/Expenditure"
import { FailedFile } from "../../addfiles/failedfiles/FailedFile"
import { EntriesFunctions } from "../../common/entries/EntriesFunctions";
import { MerchantsFunctions } from "../../common/merchants/MerchantsFunctions";
import { EntriesAndMerchantsState } from "../../common/containers/EntriesAndMerchantsState";
import * as EntriesAndMerchantsStateProcessor from "../../common/containers/EntriesAndMerchantsStateProcessor";
import { ManageContentWrapper } from "../wrapper/ManageContentWrapper";
import { IgnoredMerchantsFunctions } from "../ignoredMerchants/IgnoredMerchantsFunctions";
import { IgnoredMerchantsState } from "../ignoredMerchants/IgnoredMerchantsState";
import { MerchantRulesState } from "../merchantcategoryrules/MerchantRulesState";
import { MerchantRulesFunctions } from "../merchantcategoryrules/MerchantRulesFunctions";
import { AddRuleFunctions } from "../merchantcategoryrules/AddRuleFunctions";
import { AddRuleContent } from "../merchantcategoryrules/AddRuleContent";
import { ManageCategoriesFunctions } from "../categories/ManageCategoriesFunctions";
import { AddCategoryFunctions } from "../categories/AddCategoryFunctions";
import { CategoriesState } from "../categories/CategoriesState";
import * as ServerCalls from "../../common/ServerCalls";
import { AddIgnoreRuleFunctions } from "../ignoredMerchants/rules/AddIgnoreRuleFunctions";
import { IgnoredMerchantRulesFunctions } from "../ignoredMerchants/rules/IgnoredMerchantRulesFunctions";
import { IgnoreRulesState } from "../ignoredMerchants/rules/IgnoreRulesState";

const localStyles = require('./ManageContentContainer.module.less');

const enum JsonKey {
    FILES = "files[]",
    MERCHANTS = "merchants",
    CATEGORY = "category",
    NEW_CATEGORY = "newCategory",
    EXPENDITURE_ID = "expenditureIds",
    PATTERNS = "patterns",
    RULE = "pattern",
    CATEGORIES = "categories",
    REPLACEMENT_CATEGORY = "replacementCategory"
};

const enum SectionTitle {
    RESULTS = "Upload results"
};

export interface ManageContentContainerProps { }

export class ManageContentContainer extends React.Component<ManageContentContainerProps, { content: ManageContainerContent;
                                                                                           allCategories: Array<string>; }> {

    constructor(props: ManageContentContainerProps){
        super(props);
        this.state = null;
    }

    componentDidMount() {
        ServerCalls.singleGetRequest("http://localhost:8080/manage/",
                                      (data: object) : void => {
                                            const result: ManageServletResponse = data as ManageServletResponse;
                                            const newContentAndCategories: [ManageContainerContent, Array<string>] = ManageContainerContentProcessor.generateState(result);
                                            this.setState( { content: newContentAndCategories[0],
                                                             allCategories: newContentAndCategories[1] });
                                      },
                                      "Failed to mount ManageContentContainer");
    }

    updateBatchMerchantSelection = (category: string) : void => {
        const newContent: ManageContainerContent = this.state.content;
        newContent.entriesAndMerchantsData.batchMerchantCategory = category;
        this.setState({ content: newContent });
    }

    updateBatchIgnoredMerchantSelection = (category: string) : void => {
        const newContent: ManageContainerContent = this.state.content;
        newContent.ignoredMerchantsState.batchMerchantCategory = category;
        this.setState({ content: newContent });
    }

    updateBatchMerchantRuleSelection = (category: string) : void => {
        const newContent: ManageContainerContent = this.state.content;
        newContent.merchantRulesState.batchRuleCategory = category;
        this.setState({ content: newContent });
    }

    updateBatchReplacementCategory = (replacementCategory: string) : void => {
        const newContent: ManageContainerContent = this.state.content;
        newContent.categoriesState.batchReplacementCategory = replacementCategory;
        this.setState({ content: newContent });
    }

    updateBatchEntrySelection = (category: string) : void => {
        const newContent: ManageContainerContent = this.state.content;
        newContent.entriesAndMerchantsData.batchEntryCategory = category;
        this.setState({ content: newContent });
    }

    updateCategorySelectionForEntry = (id: number, category: string) : void => {
        const newContent: ManageContainerContent = this.state.content;
        newContent.entriesAndMerchantsData.categorySelectionsByEntry.set(id, category);
        this.setState({ content: newContent });
    }

    updateCategorySelectionForMerchant = (merchant: string, category: string) : void => {
        const newContent: ManageContainerContent = this.state.content;
        newContent.entriesAndMerchantsData.categorySelectionsByMerchant.set(merchant, category);
        this.setState({ content: newContent });
    }

    updateCategorySelectionForIgnoredMerchant = (merchant: string, category: string) : void => {
        const newContent: ManageContainerContent = this.state.content;
        newContent.ignoredMerchantsState.categorySelectionsByMerchant.set(merchant, category);
        this.setState({ content: newContent });
    }

    updateCategorySelectionForMerchantRule = (rule: string, category: string) : void => {
        const newContent: ManageContainerContent = this.state.content;
        newContent.merchantRulesState.categorySelectionsByRule.set(rule, category);
        this.setState({ content: newContent });
    }

    updateReplacementForCategory = (category: string, replacementCategory: string) : void => {
        const newContent: ManageContainerContent = this.state.content;
        newContent.categoriesState.categoriesToReplacements.set(category, replacementCategory);
        this.setState({ content: newContent });
    }

    updateAddRuleCategory = (category: string) : void => {
        const newContent: ManageContainerContent = this.state.content;
        newContent.addRuleContent.category = category;
        this.setState({ content: newContent });
    }

    updateAddRuleString = (ruleString: string) : void => {
        const newContent: ManageContainerContent = this.state.content;
        newContent.addRuleContent.rule = ruleString;
        this.setState({ content: newContent });
    }

    updateIgnoreRuleString = (ruleString: string) : void => {
        const newContent: ManageContainerContent = this.state.content;
        newContent.addIgnoreRuleContent.rule = ruleString;
        this.setState({ content: newContent });
    }

    updateCategoryString = (category: string) : void => {
        const newContent: ManageContainerContent = this.state.content;
        newContent.categoriesState.addedCategoryString = category;
        this.setState({ content: newContent });
    }

    batchUpdateMerchants = () : void => {
        const category: string = this.state.content.entriesAndMerchantsData.batchMerchantCategory;
        if (category && this.state.content.entriesAndMerchantsData.checkedMerchants && this.state.content.entriesAndMerchantsData.checkedMerchants.size > 0) {
            console.log(category);
            console.log(this.state.content.entriesAndMerchantsData.checkedMerchants);
            const merchants: Array<string> = new Array();
            this.state.content.entriesAndMerchantsData.checkedMerchants.forEach(merchant => merchants.push(merchant));
            this.executeMerchantCategoryUpdate(merchants, category);
        }
    }

    batchUpdateIgnoredMerchants = () : void => {
        const category: string = this.state.content.ignoredMerchantsState.batchMerchantCategory;
        if (category && this.state.content.ignoredMerchantsState.checkedMerchants.size > 0) {
            console.log(category);
            console.log(this.state.content.ignoredMerchantsState.checkedMerchants);
            const merchants: Array<string> = new Array();
            this.state.content.ignoredMerchantsState.checkedMerchants.forEach(merchant => merchants.push(merchant));
            this.executeIgnoredMerchantCategoryUpdate(merchants, category);
        }
    }

    batchDeleteMerchantRules = () : void => {
        const checkedRules: Set<string> = this.state.content.merchantRulesState.checkedRules;
        if (checkedRules.size > 0) {
            const checkedRulesArray: Array<string> = new Array();
            checkedRules.forEach(rule => checkedRulesArray.push(rule));
            this.executeMerchantRuleDelete(checkedRulesArray);
        }
    }

    batchDeleteIgnoreMerchantRules = () : void => {
        const checkedRules: Set<string> = this.state.content.ignoreRulesState.checkedRules;
        if (checkedRules.size > 0) {
            const checkedRulesArray: Array<string> = new Array();
            checkedRules.forEach(rule => checkedRulesArray.push(rule));
            this.executeIgnoreRuleDelete(checkedRulesArray);
        }
    }

    batchUpdateMerchantRules = () : void => {
        const category: string = this.state.content.merchantRulesState.batchRuleCategory;
        if (category && this.state.content.merchantRulesState.checkedRules.size > 0) {
            console.log(category);
            console.log(this.state.content.merchantRulesState.checkedRules);
            const rules: Array<string> = new Array();
            this.state.content.merchantRulesState.checkedRules.forEach(rule => rules.push(rule));
            this.executeMerchantRuleCategoryUpdate(rules, category);
        }
    }

    batchUpdateEntries = () : void => {
        const category: string = this.state.content.entriesAndMerchantsData.batchEntryCategory;
        if (category && this.state.content.entriesAndMerchantsData.checkedEntries && this.state.content.entriesAndMerchantsData.checkedEntries.size > 0) {
            const entryIds: Array<number> = new Array();
            this.state.content.entriesAndMerchantsData.checkedEntries.forEach(id => entryIds.push(id));
            this.executeEntryCategoryUpdate(entryIds, category);
        }
    }

    updateCategoryForEntry = (id: number) : void => {
        const category: string = this.state.content.entriesAndMerchantsData.categorySelectionsByEntry.get(id);
        if (category) {
            const entryIds: Array<number> = new Array();
            entryIds.push(id);
            this.executeEntryCategoryUpdate(entryIds, category);
        }
    }

    deleteEntry = (id: number) : void => {
        if (id) {
            const entryIds: Array<number> = new Array();
            entryIds.push(id);
            this.executeEntryDeletion(entryIds);
        }
        else {
            console.log("Invalid (null or undefined) category ID " + id + ". Not executing delete call")
        }
    }

    updateCategoryForMerchant = (merchant: string) : void => {
        const category: string = this.state.content.entriesAndMerchantsData.categorySelectionsByMerchant.get(merchant);
        if (category) {
            this.executeMerchantCategoryUpdate(new Array(merchant), category);
        }
    }

    updateCategoryForIgnoredMerchant = (merchant: string) : void => {
        const category: string = this.state.content.ignoredMerchantsState.categorySelectionsByMerchant.get(merchant);
        if (category) {
            this.executeIgnoredMerchantCategoryUpdate(new Array(merchant), category);
        }
    }

    updateCategoryForMerchantRule = (rule: string) : void => {
        const category: string = this.state.content.merchantRulesState.categorySelectionsByRule.get(rule);
        if (category) {
            this.executeMerchantRuleCategoryUpdate(new Array(rule), category);
        }
    }

    updateMerchantFromEntryId = (entryId: number, merchant: string) : void => {
        const category: string = this.state.content.entriesAndMerchantsData.categorySelectionsByEntry.get(entryId);
        if (category) {
            this.executeMerchantCategoryUpdate(new Array(merchant), category);
        }
    }

    deleteMerchantRule = (rule: string) : void => {
        this.executeMerchantRuleDelete(new Array(rule));
    }

    deleteIgnoredMerchantRule = (rule: string) : void => {
        this.executeIgnoreRuleDelete(new Array<string>(rule));
    }

    deleteCategory = (category: string) : void => {
        this.executeCategoryDeletion(new Array(category));
    }

    batchDeleteCategories = () : void => {
        const checkedCategories: Set<string> = this.state.content.categoriesState.checkedCategories;
        if (checkedCategories.size > 0) {
            const categoriesArray: Array<string> = new Array();
            checkedCategories.forEach(category => categoriesArray.push(category));
            this.executeCategoryDeletion(categoriesArray);
        }
    }

    deleteAndReplaceCategory = (category: string) : void => {
        const replacementCategory: string = this.state.content.categoriesState.categoriesToReplacements.get(category);
        if (replacementCategory !== null) {
            this.executeCategoryReplacementAndDeletion(new Array(category), replacementCategory);
        }
    }

    batchDeleteAndReplaceCategories = () : void => {
        const checkedCategories: Set<string> = this.state.content.categoriesState.checkedCategories;
        const batchReplacementCategory: string = this.state.content.categoriesState.batchReplacementCategory;
        if (checkedCategories.size > 0 && batchReplacementCategory !== null) {
            const categoriesArray: Array<string> = new Array();
            checkedCategories.forEach(category => categoriesArray.push(category));
            this.executeCategoryReplacementAndDeletion(categoriesArray, batchReplacementCategory);
        }
    }

    addRule = () : void => {
        const data: FormData = new FormData();
        const ruleString: string = this.state.content.addRuleContent.rule;
        const category: string = this.state.content.addRuleContent.category;
        if (ruleString !== null
            && ruleString.length > 0
            && category !== null) {
            data.append(JsonKey.RULE, ruleString);
            data.append(JsonKey.CATEGORY, category);
            ServerCalls.postThenGetRequest("http://localhost:8080/merchants/rules/add",
                                           "http://localhost:8080/manage/",
                                          data,
                                          (data: object) : void => {
                                                const result: ManageServletResponse = data as ManageServletResponse;
                                                const generatedState: ManageContainerContent = ManageContainerContentProcessor.generateState(result)[0];
                                                const newContent: ManageContainerContent = this.state.content;
                                                newContent.addRuleContent.rule = null;
                                                newContent.entriesAndMerchantsData.expenditures = generatedState.entriesAndMerchantsData.expenditures;
                                                newContent.entriesAndMerchantsData.merchantsByCategory = generatedState.entriesAndMerchantsData.merchantsByCategory;
                                                newContent.merchantRulesState = generatedState.merchantRulesState;
                                                newContent.merchantRulesState.shouldDisplay = true;
                                                this.setState( { content: newContent });
                                          },
                                          "Failed to add merchant rule",
                                          "Failed to retrieve new data after merchant rule creation");
        }
        else {
            console.log("Aborting addRule call due to invalid rule string (" + ruleString + ") or category (" + category + ")");
        }
    }        

    addIgnoreRule = () : void => {
        const data: FormData = new FormData();
        const ruleString: string = this.state.content.addIgnoreRuleContent.rule;
        if (ruleString !== null
            && ruleString.length > 0) {
            data.append(JsonKey.RULE, ruleString);
            ServerCalls.postThenGetRequest("http://localhost:8080/merchants/rules/ignore/add",
                                           "http://localhost:8080/manage/",
                                          data,
                                          (data: object) : void => {
                                                const result: ManageServletResponse = data as ManageServletResponse;
                                                const generatedState: ManageContainerContent = ManageContainerContentProcessor.generateState(result)[0];
                                                const newContent: ManageContainerContent = this.state.content;
                                                newContent.addIgnoreRuleContent.rule = null;
                                                newContent.entriesAndMerchantsData.expenditures = generatedState.entriesAndMerchantsData.expenditures;
                                                newContent.entriesAndMerchantsData.merchantsByCategory = generatedState.entriesAndMerchantsData.merchantsByCategory;
                                                newContent.ignoreRulesState = generatedState.ignoreRulesState;
                                                newContent.ignoreRulesState.shouldDisplay = true;
                                                this.setState( { content: newContent });
                                          },
                                          "Failed to add ignore rule",
                                          "Failed to retrieve new data after ignore rule creation");
        }
        else {
            console.log("Aborting addIgnoreRule call due to invalid rule string (" + ruleString + ")");
        }
    }

    addCategory = () : void => {
        const data: FormData = new FormData();
        const categoryString: string = this.state.content.categoriesState.addedCategoryString;
        if (categoryString !== null
            && categoryString.length) {
            data.append(JsonKey.CATEGORY, categoryString);
            ServerCalls.singlePostRequest("http://localhost:8080/categories/add",
                                          data,
                                          (data: object) : void => {
                                                const newContent: ManageContainerContent = this.state.content;
                                                const updatedCategories: Array<string> = this.state.allCategories;
                                                updatedCategories.push(categoryString);
                                                updatedCategories.sort();
                                                newContent.categoriesState.addedCategoryString = null;
                                                this.setState( { content: newContent,
                                                                 allCategories: updatedCategories});
                                          },
                                          "Failed to retrieve new data after merchant rule creation");
        }
        else {
            console.log("Aborting addCategory call due to invalid category string (" + categoryString+ ")");
        }
    } 

    executeEntryCategoryUpdate = (entryIds: Array<number>, category: string) : void => {
        const data: FormData = new FormData();
        console.log(category);
        console.log(entryIds);
        for (const id of entryIds) {
            data.append(JsonKey.EXPENDITURE_ID, id.toString());
        }
        data.append(JsonKey.NEW_CATEGORY, category);

        ServerCalls.singlePostRequest("http://localhost:8080/merchants/updateExpenditures",
                                      data,
                                      (data: object) : void => {
                                            const newResultsData: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.updateCategoriesForEntries(entryIds, category, this.state.content.entriesAndMerchantsData);
                                            const newContent = this.state.content;
                                            newContent.entriesAndMerchantsData = newResultsData;
                                            this.setState({ content: newContent });
                                      },
                                      "Failed to update expenditure category");
    }

    executeEntryDeletion = (entryIds: Array<number>) : void => {
        const data: FormData = new FormData();
        console.log(entryIds);
        for (const id of entryIds) {
            data.append(JsonKey.EXPENDITURE_ID, id.toString());
        }
        ServerCalls.singlePostRequest("http://localhost:8080/expenditures/delete",
                                      data,
                                      (data: object) : void => {
                                            const newResultsData: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.generateContentWithoutEntries(entryIds, this.state.content.entriesAndMerchantsData);
                                            const newContent: ManageContainerContent = this.state.content;
                                            newContent.entriesAndMerchantsData = newResultsData;
                                            this.setState({ content: newContent });
                                      },
                                      "Failed to delete expenditures");
    }

    executeMerchantCategoryUpdate = (merchants: Array<string>, category: string) : void => {
        const data: FormData = new FormData();
        for (const merchant of merchants) {
            data.append(JsonKey.MERCHANTS, merchant);
        }
        data.append(JsonKey.NEW_CATEGORY, category);
        ServerCalls.singlePostRequest("http://localhost:8080/merchants/updateMerchants",
                                      data,
                                      (data: object) : void => {
                                            const newResultsData: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.updateStateWithNewMerchantCategories(merchants, category, this.state.content.entriesAndMerchantsData);
                                            const newContent = this.state.content;
                                            newContent.entriesAndMerchantsData = newResultsData;
                                            this.setState({ content: newContent });
                                      },
                                      "Failed to update merchant category");
    }

    executeIgnoredMerchantCategoryUpdate = (merchants: Array<string>, category: string) : void => {
        const data: FormData = new FormData();
        for (const merchant of merchants) {
            data.append(JsonKey.MERCHANTS, merchant);
        }
        data.append(JsonKey.NEW_CATEGORY, category);
        ServerCalls.singlePostRequest("http://localhost:8080/merchants/unignoreMerchants",
                                      data,
                                      (data: object) : void => {
                                            const newIgnoredMerchantsState: IgnoredMerchantsState = IgnoredMerchantsState.removeMerchants(this.state.content.ignoredMerchantsState, new Set(merchants));
                                            const newMerchantsByCategory: Map<string, string> = this.state.content.entriesAndMerchantsData.merchantsByCategory;
                                            merchants.forEach(merchant => newMerchantsByCategory.set(merchant, category));
                                            const newContent = this.state.content;
                                            newContent.ignoredMerchantsState = newIgnoredMerchantsState;
                                            newContent.entriesAndMerchantsData.merchantsByCategory = newMerchantsByCategory;
                                            this.setState({ content: newContent });
                                      },
                                      "Failed to update ignored merchant with category");
    }

    executeMerchantRuleCategoryUpdate = (rules: Array<string>, category: string) : void => {
        const data: FormData = new FormData();
        for (const rule of rules) {
            data.append(JsonKey.PATTERNS, rule);
        }
        data.append(JsonKey.CATEGORY, category);
        ServerCalls.postThenGetRequest("http://localhost:8080/merchants/rules/update/category",
                                       "http://localhost:8080/manage/",
                                       data,
                                       (data: object) : void => {
                                            const result: ManageServletResponse = data as ManageServletResponse;
                                            const newContentAndCategories: [ManageContainerContent, Array<string>] = ManageContainerContentProcessor.generateState(result);
                                            const newContent: ManageContainerContent = this.state.content;
                                            newContent.entriesAndMerchantsData.expenditures = newContentAndCategories[0].entriesAndMerchantsData.expenditures;
                                            newContent.entriesAndMerchantsData.merchantsByCategory = newContentAndCategories[0].entriesAndMerchantsData.merchantsByCategory;
                                            rules.forEach(rule => newContent.merchantRulesState.merchantRules.set(rule, category));
                                            this.setState( { content: newContent });
                                       },
                                       "Failed to update merchant rule categories",
                                       "Failed to retrieve new data after merchant rule update");
    }

    executeMerchantRuleDelete = (rules: Array<string>) : void => {
        const data: FormData = new FormData();
        for (const rule of rules) {
            data.append(JsonKey.PATTERNS, rule);
        }
        ServerCalls.postThenGetRequest("http://localhost:8080/merchants/rules/delete",
                                       "http://localhost:8080/manage/",
                                       data,
                                       (data: object) : void => {
                                            const result: ManageServletResponse = data as ManageServletResponse;
                                            const newContentAndCategories: [ManageContainerContent, Array<string>] = ManageContainerContentProcessor.generateState(result);
                                            const newContent: ManageContainerContent = this.state.content;
                                            newContent.entriesAndMerchantsData.expenditures = newContentAndCategories[0].entriesAndMerchantsData.expenditures;
                                            newContent.entriesAndMerchantsData.merchantsByCategory = newContentAndCategories[0].entriesAndMerchantsData.merchantsByCategory;
                                            newContent.merchantRulesState = MerchantRulesState.removeRules(newContent.merchantRulesState, new Set(rules));
                                            this.setState( { content: newContent });
                                       },
                                       "Failed to delete merchant rules",
                                       "Failed to retrieve new data after merchant rule deletion");
    }

    executeIgnoreRuleDelete = (rules: Array<string>) : void => {
        const data: FormData = new FormData();
        for (const rule of rules) {
            data.append(JsonKey.PATTERNS, rule);
        }
        ServerCalls.singlePostRequest("http://localhost:8080/merchants/rules/ignore/delete",
                                       data,
                                       (data: object) : void => {
                                            const newContent: ManageContainerContent = this.state.content;
                                            newContent.ignoreRulesState = IgnoreRulesState.removeRules(newContent.ignoreRulesState, new Set(rules));
                                            this.setState( { content: newContent });
                                       },
                                       "Failed to delete ignore rules");
    }

    executeCategoryDeletion = (categories: Array<string>) : void => {
        const data: FormData = new FormData();
        for (const category of categories) {
            data.append(JsonKey.CATEGORIES, category);
        }
        ServerCalls.postThenGetRequest("http://localhost:8080/categories/delete",
                                       "http://localhost:8080/manage/",
                                       data,
                                       (data: object) : void => {
                                            const result: ManageServletResponse = data as ManageServletResponse;
                                            const newContentAndCategories: [ManageContainerContent, Array<string>] = ManageContainerContentProcessor.generateState(result);
                                            const newContent: ManageContainerContent = this.state.content;
                                            const parsedData: ManageContainerContent = newContentAndCategories[0];
                                            // merchants and expenditures will have categories updated. Same with merchant rules 
                                            newContent.entriesAndMerchantsData.merchantsByCategory = parsedData.entriesAndMerchantsData.merchantsByCategory;
                                            newContent.entriesAndMerchantsData.expenditures = parsedData.entriesAndMerchantsData.expenditures;
                                            newContent.merchantRulesState.merchantRules = parsedData.merchantRulesState.merchantRules;
                                            for (const category of categories) {
                                                newContent.categoriesState.checkedCategories.delete(category);
                                                newContent.categoriesState.categoriesToReplacements.delete(category);
                                            }
                                            this.setState( { content: newContent,
                                                             allCategories: newContentAndCategories[1] });
                                       },
                                       "Failed to delete categories",
                                       "Failed to retrieve new data after category deletion");
    }

    executeCategoryReplacementAndDeletion = (categories: Array<string>, replacementCategory: string) : void => {
        const data: FormData = new FormData();
        for (const category of categories) {
            data.append(JsonKey.CATEGORIES, category);
        }
        data.append(JsonKey.REPLACEMENT_CATEGORY, replacementCategory);
        ServerCalls.postThenGetRequest("http://localhost:8080/categories/reassign",
                                       "http://localhost:8080/manage/",
                                       data,
                                       (data: object) : void => {
                                            const result: ManageServletResponse = data as ManageServletResponse;
                                            const newContentAndCategories: [ManageContainerContent, Array<string>] = ManageContainerContentProcessor.generateState(result);
                                            const newContent: ManageContainerContent = this.state.content;
                                            const parsedData: ManageContainerContent = newContentAndCategories[0];
                                            // merchants and expenditures will have categories updated. Same with merchant rules 
                                            newContent.entriesAndMerchantsData.merchantsByCategory = parsedData.entriesAndMerchantsData.merchantsByCategory;
                                            newContent.entriesAndMerchantsData.expenditures = parsedData.entriesAndMerchantsData.expenditures;
                                            newContent.merchantRulesState.merchantRules = parsedData.merchantRulesState.merchantRules;
                                            for (const category of categories) {
                                                newContent.categoriesState.checkedCategories.delete(category);
                                                newContent.categoriesState.categoriesToReplacements.delete(category);
                                            }
                                            this.setState( { content: newContent,
                                                             allCategories: newContentAndCategories[1] });
                                       },
                                       "Failed to replace and delete categories",
                                       "Failed to retrieve new data after category replacement and deletion");
    }

    ignoreMerchant = (merchant: string) : void => {
        const data: FormData = new FormData();
        data.append(JsonKey.MERCHANTS, merchant);
        ServerCalls.singlePostRequest("http://localhost:8080/merchants/ignoreMerchant",
                                      data,
                                      (data: object) : void => {
                                            const newResultsData: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.generateContentWithoutMerchants(new Array(merchant), this.state.content.entriesAndMerchantsData);
                                            const newIgnoredMerchantsState: IgnoredMerchantsState = this.state.content.ignoredMerchantsState;
                                            newIgnoredMerchantsState.ignoredMerchants.push(merchant);
                                            newIgnoredMerchantsState.ignoredMerchants.sort();
                                            const newContent = this.state.content;
                                            newContent.entriesAndMerchantsData = newResultsData;
                                            newContent.ignoredMerchantsState = newIgnoredMerchantsState;
                                            this.setState({ content: newContent });
                                      },
                                      "Failed to ignore merchant");
    }

    onEntryCheckbox = (id: number) : void => {
            const entries: Set<number> = this.state.content.entriesAndMerchantsData.checkedEntries;
            const updatedEntries: Set<number> = this.updateCheckboxState<number>(id, entries);
            const newContent: ManageContainerContent = this.state.content;
            newContent.entriesAndMerchantsData.checkedEntries = updatedEntries;
            this.setState({ content: newContent });
    }

    onMerchantCheckbox = (name: string) : void => {
        const checkedMerchants: Set<string> = this.state.content.entriesAndMerchantsData.checkedMerchants;
        const updatedCheckedMerchants: Set<string> = this.updateCheckboxState<string>(name, checkedMerchants);
        const newContent: ManageContainerContent = this.state.content;
        newContent.entriesAndMerchantsData.checkedMerchants = updatedCheckedMerchants;
        this.setState({ content: newContent });
    }

    onIgnoredMerchantCheckbox = (name: string) : void => {
        const checkedIgnoredMerchants: Set<string> = this.state.content.ignoredMerchantsState.checkedMerchants;
        const updatedCheckedIgnoredMerchants: Set<string> = this.updateCheckboxState<string>(name, checkedIgnoredMerchants);
        const newContent: ManageContainerContent = this.state.content;
        newContent.ignoredMerchantsState.checkedMerchants = updatedCheckedIgnoredMerchants;
        console.log(name);
        console.log(updatedCheckedIgnoredMerchants);
        this.setState({ content: newContent });
    }

    onMerchantRulesCheckbox = (name: string) : void => {
        const checkedRules: Set<string> = this.state.content.merchantRulesState.checkedRules;
        const updatedCheckedRules: Set<string> = this.updateCheckboxState<string>(name, checkedRules);
        const newContent: ManageContainerContent = this.state.content;
        newContent.merchantRulesState.checkedRules = updatedCheckedRules;
        this.setState({ content: newContent });
    }

    updateIgnoredMerchantRuleCheckbox = (name: string) : void => {
        const checkedRules: Set<string> = this.state.content.ignoreRulesState.checkedRules;
        const updatedCheckedRules: Set<string> = this.updateCheckboxState<string>(name, checkedRules);
        const newContent: ManageContainerContent = this.state.content;
        newContent.ignoreRulesState.checkedRules = updatedCheckedRules;
        this.setState({ content: newContent });
    }

    updateCategoryCheckbox = (category: string) : void => {
        const checkedCategories: Set<string> = this.state.content.categoriesState.checkedCategories;
        const updatedCheckedCategories: Set<string> = this.updateCheckboxState<string>(category, checkedCategories);
        const newContent: ManageContainerContent = this.state.content;
        newContent.categoriesState.checkedCategories = updatedCheckedCategories;
        this.setState({ content: newContent });   
    }

    updateCheckboxState = <T,>(value: T, checkedSet: Set<T>) : Set<T> => {
        if (checkedSet.has(value)) {
            checkedSet.delete(value);
        }
        else {
            checkedSet.add(value);
        }
        return checkedSet;
    }

    toggleMerchantsDisplay = () : void => {
        const newEntriesAndMerchantsState: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.toggleMerchantsDisplay(this.state.content.entriesAndMerchantsData);
        const newContent: ManageContainerContent = this.state.content;
        newContent.entriesAndMerchantsData = newEntriesAndMerchantsState;
        this.setState( { content: newContent });
    }

    toggleEntriesDisplay = () : void => {
        const newEntriesAndMerchantsState: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.toggleExpendituresDisplay(this.state.content.entriesAndMerchantsData);
        const newContent: ManageContainerContent = this.state.content;
        newContent.entriesAndMerchantsData = newEntriesAndMerchantsState;
        this.setState( { content: newContent });
    }

    toggleIgnoredMerchantsDisplay = () : void => {
        const newIgnoredMerchantsState: IgnoredMerchantsState = this.state.content.ignoredMerchantsState.toggleDisplay();
        const newContent: ManageContainerContent = this.state.content;
        newContent.ignoredMerchantsState = newIgnoredMerchantsState;
        this.setState( { content: newContent });
    }

    toggleMerchantRulesDisplay = () : void => {
        const newMerchantRulesState: MerchantRulesState = this.state.content.merchantRulesState.toggleDisplay();
        const newContent: ManageContainerContent = this.state.content;
        newContent.merchantRulesState = newMerchantRulesState;
        this.setState( { content: newContent });
    }

    toggleIgnoreRulesDisplay = () : void => {
        const newIgnoreRulesState: IgnoreRulesState = this.state.content.ignoreRulesState.toggleDisplay();
        const newContent: ManageContainerContent = this.state.content;
        newContent.ignoreRulesState = newIgnoreRulesState;
        this.setState( { content: newContent });
    }

    toggleCategoriesDisplay = () : void => {
        const newCategoriesState: CategoriesState = this.state.content.categoriesState.toggleDisplay();
        const newContent: ManageContainerContent = this.state.content;
        newContent.categoriesState = newCategoriesState;
        this.setState( { content: newContent });
    }

    packageEntriesFunctions = () : EntriesFunctions => {
        return new EntriesFunctions(this.onEntryCheckbox,
                                            this.batchUpdateEntries,
                                            this.updateCategorySelectionForEntry,
                                            this.updateCategoryForEntry,
                                            this.updateMerchantFromEntryId,
                                            this.updateBatchEntrySelection,
                                            this.toggleEntriesDisplay,
                                            this.deleteEntry);
    }

    packageMerchantsFunctions = () : MerchantsFunctions => {
        return new MerchantsFunctions(this.onMerchantCheckbox,
                                      this.batchUpdateMerchants,
                                      this.updateCategorySelectionForMerchant,
                                      this.updateCategoryForMerchant,
                                      this.updateBatchMerchantSelection,
                                      this.ignoreMerchant,
                                      this.toggleMerchantsDisplay);
    }

    packageIgnoredMerchantsFunctions = () : IgnoredMerchantsFunctions => {
        return new IgnoredMerchantsFunctions(this.onIgnoredMerchantCheckbox,
                                             this.batchUpdateIgnoredMerchants,
                                             this.updateCategorySelectionForIgnoredMerchant,
                                             this.updateCategoryForIgnoredMerchant,
                                             this.updateBatchIgnoredMerchantSelection,
                                             this.toggleIgnoredMerchantsDisplay);
    }

    packageMerchantRulesFunctions = () : MerchantRulesFunctions => {
        return new MerchantRulesFunctions(this.onMerchantRulesCheckbox,
                                          this.updateCategorySelectionForMerchantRule,
                                          this.updateCategoryForMerchantRule,
                                          this.batchUpdateMerchantRules,
                                          this.updateBatchMerchantRuleSelection,
                                          this.deleteMerchantRule,
                                          this.batchDeleteMerchantRules,
                                          this.toggleMerchantRulesDisplay);
    }

    packageAddRuleFunctions = () : AddRuleFunctions => {
        return new AddRuleFunctions(this.addRule,
                                    this.updateAddRuleCategory,
                                    this.updateAddRuleString);
    }

    packageAddCategoryFunctions = () : AddCategoryFunctions => {
        return new AddCategoryFunctions(this.addCategory,
                                        this.updateCategoryString);
    }

    packageManageCategoryFunctions = () : ManageCategoriesFunctions => {
        return new ManageCategoriesFunctions(this.deleteCategory,
                                             this.deleteAndReplaceCategory,
                                             this.updateCategoryCheckbox,
                                             this.updateReplacementForCategory,
                                             this.updateBatchReplacementCategory,
                                             this.batchDeleteCategories,
                                             this.batchDeleteAndReplaceCategories,
                                             this.toggleCategoriesDisplay);
    }

    packageAddIgnoreRuleFunctions = () : AddIgnoreRuleFunctions => {
        return new AddIgnoreRuleFunctions(this.addIgnoreRule,
                                          this.updateIgnoreRuleString);
    }

    packageIgnoredMerchantRulesFunctions = () : IgnoredMerchantRulesFunctions => {
        return new IgnoredMerchantRulesFunctions(this.updateIgnoredMerchantRuleCheckbox,
                                                 this.deleteIgnoredMerchantRule,
                                                 this.batchDeleteIgnoreMerchantRules,
                                                 this.toggleIgnoreRulesDisplay);
    }

    generateContent = () : JSX.Element => {
        let content: JSX.Element = null;
        if (this.state !== null) {
            content = <div className={localStyles.manageContentContainer}>
                        <Columns>
                            <Columns.Column size={12}>
                                <ManageContentWrapper title={SectionTitle.RESULTS}
                                      data={this.state.content}
                                      categories={this.state.allCategories}
                                      merchantsFunctions={this.packageMerchantsFunctions()}
                                      entriesFunctions={this.packageEntriesFunctions()}
                                      ignoredMerchantsFunctions={this.packageIgnoredMerchantsFunctions()}
                                      merchantRulesFunctions={this.packageMerchantRulesFunctions()}
                                      addRuleFunctions={this.packageAddRuleFunctions()}
                                      addCategoryFunctions={this.packageAddCategoryFunctions()}
                                      manageCategoriesFunctions={this.packageManageCategoryFunctions()}
                                      addIgnoreRuleFunctions={this.packageAddIgnoreRuleFunctions()}
                                      ignoredMerchantRulesFunctions={this.packageIgnoredMerchantRulesFunctions()} />
                            </Columns.Column>
                        </Columns>
                       </div>
                    ;

        }
        return content;
    }

    public render() {
//         console.log("Rendering AddFiles content container");
        return this.generateContent();
    }
}
