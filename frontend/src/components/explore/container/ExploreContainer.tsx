import * as React from "react";
import { Columns } from "react-bulma-components";
// import { FileResultsContainer } from "../../addfiles/fileresultscontainer/FileResultsContainer"
// import { ManageContainerContent } from "./ManageContainerContent";
// import { ManageServletResponse } from "./ManageServletResponse";
import * as MapUtils from "../../common/MapUtils";
// import * as ManageContainerContentProcessor from "./ManageContainerContentProcessor";
import { Expenditure } from "../../common/entries/Expenditure"
// import { ManageContentWrapper } from "../wrapper/ManageContentWrapper";
import * as ServerCalls from "../../common/ServerCalls";

const localStyles = require('./ExploreContainer.module.less');

const enum JsonKey {
    CATEGORIES = "categories",
};

const enum SectionTitle {
    EXPLORE_SPENDING = "Explore spending"
};

export interface ExploreContainerProps { }

export class ExploreContainer extends React.Component<ExploreContainerProps, { content: string;
                                                                                           allCategories: Array<string>; }> {

    constructor(props: ExploreContainerProps){
        super(props);
        this.state = null;
    }

    componentDidMount() {
        console.log("In explore page")
        // ServerCalls.singleGetRequest("http://localhost:8080/explore/",
        //                               (data: object) : void => {
        //                                     const result: ManageServletResponse = data as ManageServletResponse;
        //                                     const newContentAndCategories: [ManageContainerContent, Array<string>] = ManageContainerContentProcessor.generateState(result);
        //                                     this.setState( { content: newContentAndCategories[0],
        //                                                      allCategories: newContentAndCategories[1] });
        //                               },
        //                               "Failed to mount ExploreContainer");
    }

 
    generateContent = () : JSX.Element => {
        let content: JSX.Element = null;
        if (this.state !== null) {
            content = <div className={localStyles.ExploreContainer}>
                        <Columns>
                            <Columns.Column size={12}>
                                <p>Sample text</p>
                            </Columns.Column>
                        </Columns>
                       </div>
                    ;

        }
        return content;
    }

    public render() {
//         console.log("Rendering AddFiles content container");
        return this.generateContent();
    }
}
