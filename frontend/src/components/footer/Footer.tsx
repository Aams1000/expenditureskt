import * as React from "react";
import { Columns } from "react-bulma-components";

const localStyles = require('./Footer.module.less');
const currentYear: number = new Date().getFullYear()

export interface FooterProps { }

export class Footer extends React.Component<FooterProps, {}> {
    render() {
        return <div id="footerContainer" className={localStyles.footerContainer}>
            <Columns>
              <Columns.Column size={3}>
                  <div id="footer" className={localStyles.footer}>
                      <span className={localStyles.copyrightText}>Created by Andrew, ©{currentYear}. </span>
                      <span className={localStyles.disclaimerText}>Expenditures tool is not a licensed financial advisor
                      and does not constitute financial advice. Please consult an expert for financial planning.</span>
                  </div>
              </Columns.Column>
            </Columns>
        </div>;
    }
}
