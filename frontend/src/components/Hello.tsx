import * as React from "react";

// import "./styles.css"
const styles = require('./Hello.module.less');

export interface HelloProps { }

// 'HelloProps' describes the shape of props.
// State is never set so we use the '{}' type.
export class Hello extends React.Component<HelloProps, {}> {
    render() {
        return <div>


            {/*<div className={styles.header}>Hello from {this.props.compiler} and {this.props.framework}!</div>
 
            <Container>
              <Row>
                <Col sm={4}>
                  One of three columns
                </Col>
                <Col sm={4}>
                  One of three columns
                </Col>
                <Col sm={4}>
                  One of three columns
                </Col>
              </Row>
            </Container>
            */}
        </div>;
    }
}
