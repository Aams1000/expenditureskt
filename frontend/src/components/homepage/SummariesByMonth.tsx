import { CategorySummary } from "./CategorySummary"

export interface SummariesByMonth {
    spendingByMonth: Map<string, CategorySummary>;
}

