export interface CategorySummary {
    totalSpending: number;
    spendingByCategory: Map<string, number>;
}

