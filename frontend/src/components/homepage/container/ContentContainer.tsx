import * as React from "react";
import * as moment from "moment";
import { Columns } from "react-bulma-components";
import { TopSectionContainer } from "../sectionContainers/topSectionContainer/TopSectionContainer"
import { AreaLineGraphContainer } from "../sectionContainers/areaLineGraphContainer/AreaLineGraphContainer"
import { DoughnutGraphContainer } from "../sectionContainers/doughnutGraphContainer/DoughnutGraphContainer"
import { DatePicker } from "../datepicker/DatePicker"
import { FocusedInputShape } from "react-dates";
import * as MapUtils from "../../common/MapUtils"
import { SummariesByMonth } from "../SummariesByMonth"
import { SpendingResult } from "../SpendingResult"
import { SummariesByCategory } from "../SummariesByCategory"
import { CategorySummary } from "../CategorySummary"
import { ChartType } from "../../common/ChartType"
import { ChartTypeToggleGroup } from "../../common/chartTypeToggleGroup/ChartTypeToggleGroup"

const localStyles = require('./ContentContainer.module.less');
const POST_REQUEST_DATE_FORMAT: string = "YYYY-MM-DD";

const enum DataKeys {
    BY_CATEGORY = "spendingByCategory",
    BY_MONTH = "spendingByMonth",
    MONTHLY_SPENDING = "totalSpending"
};

const enum SectionTitles {
    SPENDING_BY_CATEGORY = "Spending by category",
    SPENDING_BY_MONTH = "Spending by month",
    MONTHLY_AVERAGES_BY_CATEGORY = "Monthly averages by category",
    CATEGORY_SPENDING_OVER_TIME = "Spending by month",
    PERCENT_CHANGES = "Percent changes by category"
};

export interface ContentContainerProps { title: string; spendingSummaries: SpendingResult; }


export class ContentContainer extends React.Component<ContentContainerProps, {title: string,
                                                                              spendingSummaries: SpendingResult,
                                                                              startDate: moment.Moment,
                                                                              endDate: moment.Moment,
                                                                              datepickerFocus: FocusedInputShape,
                                                                              chartType: ChartType }> {
                                                                              // detailedSpendingData: DetailedSpendingData }> {

    constructor(props: ContentContainerProps){
        super(props);

        const spendingSummaries: SpendingResult = this.props.spendingSummaries !== null ? this.props.spendingSummaries : null;
        this.state = { title: this.props.title, spendingSummaries: spendingSummaries, startDate: null, endDate: null, datepickerFocus: null, chartType: ChartType.LINE_GRAPH };
         // detailedSpendingData: null };
    }

    onDateChange = (startDate: moment.Moment, endDate: moment.Moment) : void => {
        const newSpendingSummaries: Promise<object> = this.retrieveNewData(startDate, endDate);
        newSpendingSummaries.then((response: Response) => {
            if (!response.ok) {
                console.log("POST request failed");
                console.log(response);
            }
            else {
                console.log("POST request succeeded.");
            }
            return response;
        })
        .then((response: Response) => {
            console.log("Inside second .then function");
            response.json().then(data => {
                console.log(data);
                const parsedResult: SpendingResult = data as any as SpendingResult;
                console.log(parsedResult)
                const newState: object = {title: this.props.title,
                                        startDate: startDate,
                                        endDate: endDate,
                                        datepickerFocus: this.state.datepickerFocus,
                                        spendingSummaries: parsedResult };
            this.setState(newState);
            });
        })
        .catch((error: Error) => console.log("Failed to execute request and update: " + error.toString()));
    }

    onDatepickerFocusChange = (newFocus: FocusedInputShape) : void => {
        this.setState({ datepickerFocus : newFocus});
    }

    retrieveNewData = (startDate: moment.Moment, endDate: moment.Moment) : Promise<object> => {
        return fetch("http://localhost:8080/"
            + "?startDate=" + startDate.format(POST_REQUEST_DATE_FORMAT)
            + "&endDate=" + endDate.format(POST_REQUEST_DATE_FORMAT), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            referrer: 'no-referrer',
        });
    }

    getSpendingByMonth = (summariesByMonth: Map<string, CategorySummary>) : Map<string, number> => {
        let spendingMap: Map<string, number> = new Map();
        if (summariesByMonth) {
            spendingMap = new Map<string, number>(Array.from(summariesByMonth, (monthAndSummary) => {
                return [monthAndSummary[0], monthAndSummary[1].totalSpending]
            }));
        }
        return spendingMap;
    }

    updateTopChartType = (chartType: ChartType) : void => {
        this.setState({ chartType: chartType} );
    }

    retrieveSpendingDataForCategory = (category: string) : void => {
        console.log("Retrieving spending data for category " + category);

    }

    public render() {
        const areSummariesPresent: boolean = this.state.spendingSummaries !== null;
        const spendingByCategory: Map<string, number> = areSummariesPresent ? new Map<string, number>(Object.entries(this.state.spendingSummaries.spendingByCategory.spendingByCategory)) : null;
        // console.log("Parsed categories");
        const summariesByMonth: Map<string, CategorySummary> = areSummariesPresent ? new Map<string, CategorySummary>(Object.entries(this.state.spendingSummaries.spendingByMonth)) : null;
        const orderedMonths: Array<string> = summariesByMonth !== null ? Array.from(summariesByMonth.keys()) : null;
        // console.log("Parsed monthly summaries");
        // console.log(summariesByMonth);
        const spendingByMonth: Map<string, number> = summariesByMonth !== null ? this.getSpendingByMonth(summariesByMonth) : null;
        const monthlyAverageByCategory: Map<string, number> = areSummariesPresent ? new Map<string, number>(Object.entries(this.state.spendingSummaries.monthlyCategoryAverages)) : null;
        const categorySpendingOverTime: Map<string, Array<number>> = areSummariesPresent ? new Map<string, Array<number>>(Object.entries(this.state.spendingSummaries.categorySpendingOverTime)) : null;
        // it seems that when the result from the server doesn't contain the key,
        // typescript parses the JSON to the interface but leaves this field as undefined
        // instead of null
        const percentChangesByCategory: Map<string, number> = areSummariesPresent && this.state.spendingSummaries.percentChanges !== undefined ? new Map<string, number>(Object.entries(this.state.spendingSummaries.percentChanges.changesByCategory)) : null;
        // console.log("Parsed spendingByMonth");
        // console.log(spendingByMonth);
        const overallChartSubtitle: string = areSummariesPresent ? "$" + this.state.spendingSummaries.spendingByCategory.totalSpending.toLocaleString("en-US", {"minimumFractionDigits": 2}) + " total" : "";
        var overallChangeInSpending: string = "";
        if (areSummariesPresent && percentChangesByCategory !== null) {
            const changeInSpending: number = this.state.spendingSummaries.percentChanges.absoluteChange;
            if (changeInSpending > 0) {
                overallChangeInSpending = "$" + changeInSpending.toLocaleString("en-US", {"minimumFractionDigits": 2});
            }
            else {
                overallChangeInSpending = "-$" + (-changeInSpending).toLocaleString("en-US", {"minimumFractionDigits": 2});
            }
        }
        return <div className={localStyles.contentContainer}>
            <Columns>
              <Columns.Column size={8}>
                  <DatePicker  startDate={this.state.startDate}
                               endDate={this.state.endDate}
                               datepickerFocus={this.state.datepickerFocus}
                               parentOnChangeFocus={this.onDatepickerFocusChange}
                               parentOnChange={this.onDateChange} />

                   <ChartTypeToggleGroup title="Top chart type" onValueChange={this.updateTopChartType} />
                  {(() => {
                    switch(this.state.chartType) {
                      case ChartType.LINE_GRAPH:
                        return <AreaLineGraphContainer title={SectionTitles.CATEGORY_SPENDING_OVER_TIME} subtitle={overallChartSubtitle} data={categorySpendingOverTime} labels={orderedMonths} />
                      case ChartType.BAR_CHART:
                        return <TopSectionContainer title={SectionTitles.SPENDING_BY_MONTH} subtitle={overallChartSubtitle} data={spendingByMonth} />
                      case ChartType.DOUGHNUT_CHART:
                        return <DoughnutGraphContainer title={SectionTitles.SPENDING_BY_CATEGORY} subtitle={overallChartSubtitle} data={spendingByCategory} />
                      default:
                          console.error("Unknown ChartType " + this.state.chartType + ". Not rendering top graph");
                        return null
                    }
                  })()}
                  { percentChangesByCategory !== null && 
                    <TopSectionContainer title={SectionTitles.PERCENT_CHANGES} subtitle={"Overall " + this.state.spendingSummaries.percentChanges.percentChange + "% or " + overallChangeInSpending + " as compared to " + this.state.spendingSummaries.percentChanges.startDate + " to " + this.state.spendingSummaries.percentChanges.endDate} data={percentChangesByCategory} />

                  }
                  <TopSectionContainer  title={SectionTitles.MONTHLY_AVERAGES_BY_CATEGORY} subtitle={null} data={monthlyAverageByCategory} />
                  <TopSectionContainer  title={SectionTitles.SPENDING_BY_CATEGORY} subtitle={null} data={spendingByCategory} />
              </Columns.Column>
            </Columns>
        </div>;
    }
}
