export interface PercentChanges {
    totalSpending: number;
    percentChange: number;
    absoluteChange: number;
    changesByCategory: Map<string, number>;
    startDate: string;
    endDate: string;
}
