import * as React from "react";
import { Columns } from "react-bulma-components";
import 'chart.js/auto';
import type { ChartData, ChartOptions } from 'chart.js';
import { Chart } from "react-chartjs-2";
import * as ChartColors from "../../../common/ChartColors"

const localStyles = require('./HalfWidthPieChart.module.less');

const BACKGROUND_COLOR: string = 'rgb(255, 99, 132)';
const BORDER_COLOR: string = BACKGROUND_COLOR;

interface DoughnutProps {
  options: ChartOptions<'doughnut'>;
  data: ChartData<'doughnut'>;
}

export interface HalfWidthPieChartProps { title: string; graphId: string; data: Map<string, number>; }

export class HalfWidthPieChart extends React.Component<HalfWidthPieChartProps, {}> {

    constructor(props: HalfWidthPieChartProps){
        super(props);
    }

    prepareData = () : ChartData<"doughnut"> => {
        const haveCustomData: boolean = this.props.data !== null && this.props.data.size > 0;
        const data = {
            labels: haveCustomData ? Array.from(this.props.data.keys()) : ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [{
                label: haveCustomData ? this.props.title : "My test dataset",
                backgroundColor: haveCustomData ? ChartColors.getColorNames(Array.from(this.props.data.values()).length) : BACKGROUND_COLOR,
                data: haveCustomData ? Array.from(this.props.data.values()) : [0, 10, 5, 2, 20, 30, 45],
            }]
        };
        if (haveCustomData) {
            console.log(this.props.data);
            console.log(Array.from(this.props.data.keys()));
            console.log(Array.from(this.props.data.values()));
        }
        return data;
    }
    getOptions = () : object => {
        const options = {
            // responsive: true,
            maintainAspectRatio: false    // Don't maintain w/h ratio
        };
    return options;
    }


    public render() {
        return <span className={localStyles.graphWrapper}>
                  <span className={localStyles.graph}>
                      <Chart type="doughnut" data={this.prepareData()} options={this.getOptions()} />
                  </span>
        </span>;
    }
}
