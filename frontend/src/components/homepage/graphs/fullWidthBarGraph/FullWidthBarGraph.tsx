import * as React from "react";
import { Columns } from "react-bulma-components";
import 'chart.js/auto';
import type { ChartData, ChartOptions, ScriptableScaleContext, GridLineOptions } from 'chart.js';
import { Chart } from "react-chartjs-2";

const localStyles = require('./FullWidthBarGraph.module.less');

// const BACKGROUND_COLOR: string = '#D295BF';
const BACKGROUND_COLOR: string = "#7A9E7E" // themeCambridgeBlue
// const BACKGROUND_COLOR: string = "#005F73"; // themeMidnightGreen
// const POSITIVE_BACKGROUND_COLOR: string = "#F48C06" // themeCambridgeBlue
const POSITIVE_BACKGROUND_COLOR: string = "#CA6702"; // themeAlloyOrange
const NEGATIVE_BACKGROUND_COLOR: string = "#94D2BD";
const BORDER_COLOR: string = BACKGROUND_COLOR;

interface BarProps {
  options: ChartOptions<'bar'>;
  data: ChartData<'bar'>;
}

export interface FullWidthBarGraphProps { title: string; subtitle: string; graphId: string; data: Map<string, number>; }

export class FullWidthBarGraph extends React.Component<FullWidthBarGraphProps, {}> {

    constructor(props: FullWidthBarGraphProps){
        super(props);
    }

    prepareData = () : ChartData<"bar"> => {
        const haveCustomData: boolean = this.props.data !== null && this.props.data.size > 0;
        const backgroundColors: Array<string> = Array();
        const borderColors: Array<string> = Array();
        var isPositiveNegativeGraph: boolean = false;
        for (const value of this.props.data.values()) {
            if (value < 0.0) {
                isPositiveNegativeGraph = true;
            }
        }
        const positiveColor: string = isPositiveNegativeGraph ? POSITIVE_BACKGROUND_COLOR : BACKGROUND_COLOR;
        const negativeColor: string = isPositiveNegativeGraph ? NEGATIVE_BACKGROUND_COLOR : BACKGROUND_COLOR;
        for (const value of this.props.data.values()) {
            if (value >= 0.0) {
                backgroundColors.push(positiveColor);
                borderColors.push(positiveColor);
            }
            else {
                backgroundColors.push(negativeColor);
                borderColors.push(negativeColor);
            }
        }
        const data = {
            labels: haveCustomData ? Array.from(this.props.data.keys()) : ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [{
                label: haveCustomData ? this.props.title : "My test dataset",
                backgroundColor: backgroundColors,
                borderColor: borderColors,
                data: haveCustomData ? Array.from(this.props.data.values()) : [0, 10, 5, 2, 20, 30, 45],
            }]
        };
        return data;
    }

    getOptions = () : object => {
        const options = {
                maintainAspectRatio: false,    // Don't maintain w/h ratio
                plugins: {
                    legend: {
                        display: false
                    },
                  title: {
                    display: true,
                    text: this.props.title
                  },
                  subtitle: {
                      display: this.props.subtitle !== null,
                      text: this.props.subtitle
                  },
                  tooltip: {
                    mode: "index"
                  },
                },
                interaction: {
                  mode: "nearest",
                  axis: "x",
                  intersect: false
                },
                scales: {
                  y: {
                    // ticks: {
                    //   min: 0,
                    // },
                    title: {
                      display: true,
                      text: "Amount"
                    },
                    // grid: {
                    //   color: function(context: ScriptableScaleContext) {
                    //     if (context.tick.value == 0) {
                    //         return "#000000";
                    //     }
                        // else {
                        //     return rgba(0, 0, 0, 0.1);
                        // }
                      // },
        // },
                  }
                }
            };
        return options;
    }

    public render() {
        return <span className={localStyles.graphWrapper}>
                  <span className={localStyles.graph}>
                      <Chart type="bar" data={this.prepareData()} options={this.getOptions()} />
                  </span>
        </span>;
    }
}
