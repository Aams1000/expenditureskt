import * as React from "react";
import { MouseEvent } from "react";
import { Columns } from "react-bulma-components";
import 'chart.js/auto';
import type { ChartData, ChartOptions, TooltipItem, InteractionItem, Chart as ChartJS } from 'chart.js';
import { Chart, getElementAtEvent } from "react-chartjs-2";
import * as ChartColors from "../../../common/ChartColors"

const localStyles = require('./DoughnutGraph.module.less');

const BACKGROUND_COLOR: string = 'rgb(255, 99, 132)';
const BORDER_COLOR: string = BACKGROUND_COLOR;

interface DoughnutProps {
  options: ChartOptions<'doughnut'>;
  data: ChartData<'doughnut'>;
}

export interface DoughnutGraphProps { title: string; subtitle: string; graphId: string; data: Map<string, number>; }
// parentOnClick: (category: string) => void; }

export class DoughnutGraph extends React.Component<DoughnutGraphProps, {}> {

    private chartRef = React.createRef<ChartJS>();

    constructor(props: DoughnutGraphProps){
        super(props);
    }

    prepareData = () : ChartData<"doughnut"> => {
        const haveCustomData: boolean = this.props.data !== null && this.props.data.size > 0;
        const data = {
            labels: Array.from(this.props.data.keys()),
            datasets: [{
                label: this.props.title,
                backgroundColor: ChartColors.getColorNames(Array.from(this.props.data.values()).length),
                // borderColor: BORDER_COLOR,
                data: Array.from(this.props.data.values()),
            }]
        };
        return data;
    }

    getOptions = () : object => {
        const options = {
            // responsive: true,
            maintainAspectRatio: false,    // Don't maintain w/h ratio
            plugins: {
              title: {
                display: true,
                text: this.props.title
              },
              subtitle: {
                  display: this.props.subtitle !== null,
                  text: this.props.subtitle
              },
              tooltip: {
                mode: "index",
                callbacks: {
                    label: function (context: TooltipItem<"doughnut">) {
                        const amount: number = Number(context.formattedValue.replace(",", ""));
                        let sum: number = 0;
                        context.dataset.data.forEach(value => sum += value);
                        const percentage: string = sum > 0 ? ((amount / sum) * 100).toFixed(1) : "0.00";
                        return " " + context.formattedValue + " (" + percentage + "%)";
                    }
                }
              },
            }
        };
    return options;
    }


    printElementAtEvent = (element: InteractionItem[]) : void => {
        console.log("In print element function");
        console.log(element);
        if (!element.length) return;

        const { datasetIndex, index } = element[0];
        // FIXME - rework the data and options preparation so that the class
        // receives the data as a prop. This will avoid recomputing the data on each click
        const data: ChartData<"doughnut"> = this.prepareData();
        console.log(data.labels[index], data.datasets[datasetIndex].data[index]);
      }

      onClick = (event: MouseEvent<HTMLCanvasElement>) : void => {
          console.log("In onclick function");
          console.log(this.chartRef);
        const { current: chart } = this.chartRef;

        if (chart !== null) {
          this.printElementAtEvent(getElementAtEvent(chart, event));
        }
      }

    public render() {
        return <span className={localStyles.graphWrapper}>
                  <span className={localStyles.graph}>
                      <Chart ref={this.chartRef} type="doughnut" data={this.prepareData()} options={this.getOptions()} onClick={this.onClick} />
                  </span>
        </span>;
    }
}

