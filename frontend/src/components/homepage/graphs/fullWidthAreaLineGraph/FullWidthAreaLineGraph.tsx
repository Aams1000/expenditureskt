import * as React from "react";
import { Columns } from "react-bulma-components";
import 'chart.js/auto';
import type { ChartData, ChartOptions } from 'chart.js';
import { Chart } from "react-chartjs-2";
import * as Colors from "../../../../global/Colors";
import * as ChartColors from "../../../common/ChartColors"

const localStyles = require("./FullWidthAreaLineGraph.module.less");

const BACKGROUND_COLOR: string = "rgb(255, 99, 132)";
const BORDER_COLOR: string = BACKGROUND_COLOR;

interface LineProps {
  options: ChartOptions<'line'>;
  data: ChartData<'line'>;
}

export interface FullWidthAreaLineGraphProps { title: string; subtitle: string; graphId: string; data: Map<string, Array<number>>; labels: Array<string>; }

export class FullWidthAreaLineGraph extends React.Component<FullWidthAreaLineGraphProps, {}> {

    constructor(props: FullWidthAreaLineGraphProps){
        super(props);
    }

    prepareData = () : ChartData<"line"> => {
        const data: ChartData<"line"> = {datasets: null}
        data["labels"] = this.props.labels;
        const datasets = [];
        const colorCodes: Array<string> = ChartColors.getColorNames(this.props.data.size);
        let datasetCounter: number = 0;
        for (const categoryAndSpendingArray of this.props.data.entries()) {
            const dataset: any = {};
            
            dataset["label"] = categoryAndSpendingArray[0];
            dataset["data"] = categoryAndSpendingArray[1];
            const colorName: string = colorCodes[datasetCounter];
            dataset["borderColor"] = colorName;
            dataset["backgroundColor"] = Colors.transparentize(colorName);
            if (datasets.length == 0) {
                dataset["fill"] = true;
                dataset["hidden"] = false;
            }
            else {
                dataset["fill"] = "-1";
            }
            datasets.push(dataset);
            datasetCounter++;
        }
        data["datasets"] = datasets;
        return data;
    }

    prepareConfig = () : any => {
      const config = {
            maintainAspectRatio: false,
            plugins: {
              title: {
                display: true,
                text: this.props.title
              },
              subtitle: {
                  display: this.props.subtitle !== null,
                  text: this.props.subtitle
              },
              tooltip: {
                mode: "index"
              },
            },
            interaction: {
              mode: "nearest",
              axis: "x",
              intersect: false
            },
            scales: {
              y: {
                stacked: true,
                title: {
                  display: true,
                  text: "Amount"
                }
              }
            }
        };
        return config;
    }

    getOptions = () : object => {
        const options = {
            // responsive: true,
            maintainAspectRatio: false,    // Don"t maintain w/h ratio
            scales : {
            xAxes : [ {
                    gridLines : {
                        display : false
                    }
                } ]
            }
        };
    return options;
    }

    public render() {
        return <span className={localStyles.graphWrapper}>
                  <span className={localStyles.graph}>
                    <Chart type="line" data={this.prepareData()} options={this.prepareConfig()} />
                  </span>
        </span>;
    }
}
