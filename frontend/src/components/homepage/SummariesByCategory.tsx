export interface SummariesByCategory {
    totalSpending: number;
    spendingByCategory: Map<string, number>;
}

