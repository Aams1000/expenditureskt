import * as React from 'react';
import { DateRangePicker, FocusedInputShape } from 'react-dates';
// needed for the react date picker to work
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css'
import moment from "moment";
import { Moment } from "moment";

const localStyles = require('./DatePicker.module.less');
const TODAY: Moment = moment();
const DEFAULT_END_DATE: Moment = TODAY
const DEFAULT_START_DATE: Moment = moment().startOf("month").subtract(1, "year");

export interface DatePickerProps {  startDate: Moment;
                                    endDate: Moment;
                                    datepickerFocus: FocusedInputShape;
                                    parentOnChangeFocus: (focus: FocusedInputShape) => void;
                                    parentOnChange: (startDate: Moment, endDate: Moment) => void; }

export class DatePicker extends React.Component<DatePickerProps, {startDate: Moment, endDate: Moment}> {

    constructor(props: DatePickerProps){
        super(props);
        const startDate: Moment = this.props.startDate !== null ? this.props.startDate : DEFAULT_START_DATE;
        const endDate: Moment = this.props.endDate !== null ? this.props.endDate : DEFAULT_END_DATE;
        console.log("Constructor start date: " + startDate);
        console.log("Constructor end date: " + endDate);
        //tell parent we've set dates
        this.props.parentOnChange(startDate, endDate);
    }

  onChange = (startDate: Moment, endDate: Moment) : void => {
    if (startDate !== null
        && endDate !== null) {
        this.props.parentOnChange(startDate, endDate);
    }
  }
  
  render() {
    return (
        <div className={localStyles.datePickerWrapper}>
          <DateRangePicker
              startDate={this.props.startDate !== null ? this.props.startDate : DEFAULT_START_DATE}
              startDateId="your_unique_start_date_id"
              endDate={this.props.endDate !== null ? this.props.endDate : DEFAULT_END_DATE}
              endDateId="your_unique_end_date_id"
              onDatesChange={({ startDate, endDate }) => this.onChange(startDate, endDate)}
              focusedInput={this.props.datepickerFocus}
              onFocusChange={focusedInput => this.props.parentOnChangeFocus(focusedInput)}
              isOutsideRange={() => false}
            />
      </div>
    )
  }
}


