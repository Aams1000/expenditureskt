import { SummariesByMonth } from "./SummariesByMonth"
import { SummariesByCategory } from "./SummariesByCategory"
import { PercentChanges } from "./PercentChanges"

export interface SpendingResult {
    spendingByCategory: SummariesByCategory;
    spendingByMonth: SummariesByMonth;
    monthlyCategoryAverages: Map<string, number>;
    categorySpendingOverTime: Map<string, Array<number>>;
    percentChanges: PercentChanges;
}

