import * as React from "react";
import { Columns } from "react-bulma-components";
import { FullWidthAreaLineGraph } from "../../graphs/fullWidthAreaLineGraph/FullWidthAreaLineGraph";

const localStyles = require('./AreaLineGraphContainer.module.less');
const defaultTitle: string = "Loading your recent spending...";

export interface AreaLineGraphContainerProps { title: string; subtitle: string; labels: Array<string>; data: Map<string, Array<number>>; }

export class AreaLineGraphContainer extends React.Component<AreaLineGraphContainerProps, {}> {

    constructor(props: AreaLineGraphContainerProps, initialState: object){
        super(props);
    }

    public render() {
        return <div className={localStyles.AreaLineGraphContainer}>
            <Columns className={localStyles.columnsClassName}>
              {this.props.data !== null && this.props.labels !== null &&
                  <Columns.Column size={8}>
                      <span className={localStyles.topGraphWrapper}>
                          <FullWidthAreaLineGraph title={this.props.title} subtitle={this.props.subtitle} graphId="firstGraph" data={this.props.data} labels={this.props.labels}/>
                      </span>
                  </Columns.Column>
              }
            </Columns>
        </div>;
    }
}
