import * as React from "react";
import { Columns } from "react-bulma-components";
import { FullWidthBarGraph } from "../../graphs/fullWidthBarGraph/FullWidthBarGraph";
import { HalfWidthPieChart } from "../../graphs/halfWidthPieChart/HalfWidthPieChart";

const localStyles = require('./TopSectionContainer.module.less');
const defaultTitle: string = "Loading your recent spending...";

export interface TopSectionContainerProps { title: string; subtitle: string; data: Map<string, number>; }

export class TopSectionContainer extends React.Component<TopSectionContainerProps, {}> {

    constructor(props: TopSectionContainerProps, initialState: object){
        super(props);
    }

    public render() {
//         console.log("Rendering top section container");
        return <div className={localStyles.topSectionContainer}>
            <Columns className={localStyles.columnsClassName}>
            {this.props.data !== null &&
              <Columns.Column size={8}>
                  <span className={localStyles.topGraphWrapper}>
                      <FullWidthBarGraph title={this.props.title} subtitle={this.props.subtitle} graphId="firstGraph" data={this.props.data}/>
                  </span>
              </Columns.Column>
              }
            </Columns>
        </div>;
    }
}
