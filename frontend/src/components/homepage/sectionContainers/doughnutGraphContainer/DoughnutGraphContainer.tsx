import * as React from "react";
import { Columns } from "react-bulma-components";
import { DoughnutGraph } from "../../graphs/doughnutGraph/DoughnutGraph";

const localStyles = require('./DoughnutGraphContainer.module.less');
const defaultTitle: string = "Loading your recent spending...";

export interface DoughnutGraphContainerProps { title: string; subtitle: string; data: Map<string, number>; }

export class DoughnutGraphContainer extends React.Component<DoughnutGraphContainerProps, {}> {

    constructor(props: DoughnutGraphContainerProps, initialState: object){
        super(props);
    }

    public render() {
//         console.log("Rendering top section container");
        return <div className={localStyles.DoughnutGraphContainer}>
            <Columns className={localStyles.columnsClassName}>
            {this.props.data !== null &&
              <Columns.Column size={8}>
                  <span className={localStyles.topGraphWrapper}>
                      <DoughnutGraph title={this.props.title} subtitle={this.props.subtitle} graphId="firstGraph" data={this.props.data}/>
                  </span>
              </Columns.Column>
            }
            </Columns>
        </div>;
    }
}
