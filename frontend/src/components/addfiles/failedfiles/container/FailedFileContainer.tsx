import * as React from "react";
import { Columns } from "react-bulma-components";
import { FailedFile } from "../../failedfiles/FailedFile"

const localStyles = require('./FailedFileContainer.module.less');

export interface FailedFileContainerProps { titles: Array<string>; data: Array<FailedFile>; }

export class FailedFileContainer extends React.Component<FailedFileContainerProps, {categorizedCheckboxes: Array<boolean>,
                                                                                    uncategorizedCheckboxes: Array<boolean>,
                                                                                    merchantCheckboxes: Array<boolean> }> {

    generateContent = () : Array<JSX.Element> => {
        let content: Array<JSX.Element> = new Array();
        if (this.props.data !== null) {
            if (this.props.data.length === 0) {
                content.push(<div className={localStyles.failedFileContainer}>
                                  <span className={localStyles.descriptionText}>{this.props.titles[0]}</span>
                                </div>);
            }
            else {
                for (const failedFile of this.props.data) {
                    content.push(
                        <tr>
                           <td scope="row" data-label={this.props.titles[2]}>{failedFile.fileName}</td>
                           <td data-label={this.props.titles[3]}>{failedFile.reason}</td>
                         </tr>
                         );
                }
                const newContent: Array<JSX.Element> = new Array<JSX.Element>();
                newContent.push(
                    <div className={localStyles.failedFileContainer}>
                        <table className={localStyles.table}>
                           <caption>{this.props.titles[1]}</caption>
                           <thead>
                             <tr>
                               <th scope="col">{this.props.titles[2]}</th>
                               <th scope="col">{this.props.titles[3]}</th>
                             </tr>
                           </thead>
                           <tbody>
                             {content}
                           </tbody>
                       </table>
                   </div>
                   );
                content = newContent;
            }
        }
        return content;
    }

    public render() {
//         console.log("Rendering FailedFileContainer");
        return this.generateContent();
    }
}
