export interface FailedFile {
    fileName: string;
    reason: string;
}
