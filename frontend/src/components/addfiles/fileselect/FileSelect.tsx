import * as React from "react";
import { Columns } from "react-bulma-components";

const localStyles = require('./FileSelect.module.less');

export interface FileSelectProps { name: string; directions: string; onUpload: (fileList: FileList) => void }

export class FileSelect extends React.Component<FileSelectProps, {}> {

    public render() {
        return <div className={localStyles.fileSelectContainer}>
            <span className={localStyles.directions}>{this.props.directions}</span>
    {/*Can't use webkitdirectory on this. Right now just accepting files, not directories*/}
            <input type="file" className={localStyles.input} name={this.props.name} multiple={true} onChange={ (e) => this.props.onUpload(e.target.files) } />
        </div>;
    }
}
