import * as React from "react";
import { Columns } from "react-bulma-components";
import { FileSelect } from "../fileselect/FileSelect"
import { FileResultsContainer } from "../fileresultscontainer/FileResultsContainer"
import { FileResultsContainerContent } from "../fileresultscontainer/FileResultsContainerContent";
import { AddFileResult } from "../AddFileResult"
import * as MapUtils from "../../common/MapUtils";
import * as FileResultsProcessor from "../fileresultscontainer/FileResultsProcessor";
import { Expenditure } from "../../common/entries/Expenditure"
import { EntriesFunctions } from "../../common/entries/EntriesFunctions";
import { MerchantsFunctions } from "../../common/merchants/MerchantsFunctions";
import { EntriesAndMerchantsState } from "../../common/containers/EntriesAndMerchantsState";
import * as EntriesAndMerchantsStateProcessor from "../../common/containers/EntriesAndMerchantsStateProcessor";
import * as ServerCalls from "../../common/ServerCalls"
import { AddRuleFunctions } from "../../manage/merchantcategoryrules/AddRuleFunctions";
import { AddIgnoreRuleFunctions } from "../../manage/ignoredMerchants/rules/AddIgnoreRuleFunctions";
import { AddRule } from "../../manage/merchantcategoryrules/addcategoryrule/AddRule";
import { AddIgnoreRule } from "../../manage/ignoredMerchants/rules/addignorerule/AddIgnoreRule";
const localStyles = require('./AddFilesContentContainer.module.less');

const enum JsonKey {
    FILES = "files[]",
    MERCHANTS = "merchants",
    CATEGORY = "category",
    NEW_CATEGORY = "newCategory",
    EXPENDITURE_ID = "expenditureIds",
    RULE = "pattern",
};

const enum SectionTitle {
    FILE_SELECT = "Select file(s) to upload:",
    RESULTS = "Upload results"
};

const FILE_NAME: string = "files";

export interface AddFilesContentContainerProps { }

export class AddFilesContentContainer extends React.Component<AddFilesContentContainerProps, { content: FileResultsContainerContent;
                                                                                                categories: Array<string>; }> {

    constructor(props: AddFilesContentContainerProps){
        super(props);
        this.state = { content: null,
                        categories: null };
    }

    retrieveNewCategories = () : Promise<Response> => {
        return fetch("http://localhost:8080/merchants/getCategories", {
            method: 'GET',
            referrer: 'no-referrer',
        });
    }

    updateBatchMerchantSelection = (category: string) : void => {
        const newContent: FileResultsContainerContent = this.state.content;
        newContent.resultsData.batchMerchantCategory = category;
        this.setState({ content: newContent });
    }

    updateBatchEntrySelection = (category: string) : void => {
        const newContent: FileResultsContainerContent = this.state.content;
        newContent.resultsData.batchEntryCategory = category;
        this.setState({ content: newContent });
    }

    updateCategorySelectionForEntry = (id: number, category: string) : void => {
        const newContent: FileResultsContainerContent = this.state.content;
        newContent.resultsData.categorySelectionsByEntry.set(id, category);
        this.setState({ content: newContent })
    }

    updateCategorySelectionForMerchant = (merchant: string, category: string) : void => {
        const newContent: FileResultsContainerContent = this.state.content;
        newContent.resultsData.categorySelectionsByMerchant.set(merchant, category);
        this.setState({ content: newContent });
    }

    batchUpdateMerchants = () : void => {
        const category: string = this.state.content.resultsData.batchMerchantCategory;
        if (category && this.state.content.resultsData.checkedMerchants && this.state.content.resultsData.checkedMerchants.size > 0) {
            console.log(category);
            console.log(this.state.content.resultsData.checkedMerchants);
            const merchants: Array<string> = new Array();
            this.state.content.resultsData.checkedMerchants.forEach(merchant => merchants.push(merchant));
            this.executeMerchantCategoryUpdate(merchants, category);
        }
    }

    batchUpdateEntries = () : void => {
        const category: string = this.state.content.resultsData.batchEntryCategory;
        if (category && this.state.content.resultsData.checkedEntries && this.state.content.resultsData.checkedEntries.size > 0) {
            const entryIds: Array<number> = new Array();
            this.state.content.resultsData.checkedEntries.forEach(id => entryIds.push(id));
            this.executeEntryCategoryUpdate(entryIds, category);
        }
    }

    updateCategoryForEntry = (id: number) : void => {
        const category: string = this.state.content.resultsData.categorySelectionsByEntry.get(id);
        if (category) {
            const entryIds: Array<number> = new Array();
            entryIds.push(id);
            this.executeEntryCategoryUpdate(entryIds, category);
        }
    }

    deleteEntry = (id: number) : void => {
        if (id) {
            const entryIds: Array<number> = new Array();
            entryIds.push(id);
            this.executeEntryDeletion(entryIds);
        }
        else {
            console.log("Invalid (null or undefined) category ID " + id + ". Not executing delete call")
        }
    }

    updateCategoryForMerchant = (merchant: string) : void => {
        const category: string = this.state.content.resultsData.categorySelectionsByMerchant.get(merchant);
        if (category) {
            this.executeMerchantCategoryUpdate(new Array(merchant), category);
        }
    }

    updateMerchantFromEntryId = (entryId: number, merchant: string) : void => {
        const category: string = this.state.content.resultsData.categorySelectionsByEntry.get(entryId);
        if (category) {
            this.executeMerchantCategoryUpdate(new Array(merchant), category);
        }
    }

    executeEntryCategoryUpdate = (entryIds: Array<number>, category: string) : void => {
        const data: FormData = new FormData();
        console.log(category);
        console.log(entryIds);
        for (const id of entryIds) {
            data.append(JsonKey.EXPENDITURE_ID, id.toString());
        }
        data.append(JsonKey.NEW_CATEGORY, category);
        
        ServerCalls.singlePostRequest("http://localhost:8080/merchants/updateExpenditures",
                                      data,
                                      (data: object) : void => {
                                            const newResultsData: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.updateCategoriesForEntries(entryIds, category, this.state.content.resultsData);
                                            const newContent: FileResultsContainerContent = new FileResultsContainerContent(this.state.content.isDataPresent,
                                                                                                                            this.state.content.failedFiles,
                                                                                                                            newResultsData,
                                                                                                                            this.state.content.addRuleContent,
                                                                                                                            this.state.content.addIgnoreRuleContent);
                                            this.setState({ content: newContent });
                                      },
                                      "Failed to update expenditure category");
    }

    executeEntryDeletion = (entryIds: Array<number>) : void => {
        const data: FormData = new FormData();
        console.log(entryIds);
        for (const id of entryIds) {
            data.append(JsonKey.EXPENDITURE_ID, id.toString());
        }
        ServerCalls.singlePostRequest("http://localhost:8080/expenditures/delete",
                                      data,
                                      (data: object) : void => {
                                            const newResultsData: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.generateContentWithoutEntries(entryIds, this.state.content.resultsData);
                                            const newContent: FileResultsContainerContent = new FileResultsContainerContent(this.state.content.isDataPresent,
                                                                                                                            this.state.content.failedFiles,
                                                                                                                            newResultsData,
                                                                                                                            this.state.content.addRuleContent,
                                                                                                                            this.state.content.addIgnoreRuleContent);
                                            this.setState({ content: newContent });
                                      },
                                      "Failed to delete expenditures");
    }

    executeMerchantCategoryUpdate = (merchants: Array<string>, category: string) : void => {
        const data: FormData = new FormData();
        for (const merchant of merchants) {
            data.append(JsonKey.MERCHANTS, merchant);
        }
        data.append(JsonKey.NEW_CATEGORY, category);
        ServerCalls.singlePostRequest("http://localhost:8080/merchants/updateMerchants",
                                      data,
                                      (data: object) : void => {
                                            const newResultsData: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.updateStateWithNewMerchantCategories(merchants, category, this.state.content.resultsData);
                                            const newContent: FileResultsContainerContent = new FileResultsContainerContent(this.state.content.isDataPresent,
                                                                                                                            this.state.content.failedFiles,
                                                                                                                            newResultsData,
                                                                                                                            this.state.content.addRuleContent,
                                                                                                                            this.state.content.addIgnoreRuleContent);
                                            this.setState({ content: newContent });
                                      },
                                      "Failed to update merchant category");
    }

    ignoreMerchant = (merchant: string) : void => {
        const data: FormData = new FormData();
        data.append(JsonKey.MERCHANTS, merchant);
        ServerCalls.singlePostRequest("http://localhost:8080/merchants/ignoreMerchant",
                              data,
                              (data: object) : void => {
                                    const newResultsData: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.generateContentWithoutMerchants(new Array(merchant), this.state.content.resultsData);
                                    const newContent: FileResultsContainerContent = new FileResultsContainerContent(this.state.content.isDataPresent,
                                                                                                                    this.state.content.failedFiles,
                                                                                                                    newResultsData,
                                                                                                                    this.state.content.addRuleContent,
                                                                                                                    this.state.content.addIgnoreRuleContent);
                                    this.setState({ content: newContent });                              
                              },
                              "Failed to ignore merchant");
    }

    onFileSelect = (files: FileList) : void => {
        console.log("Inside parent onFileUpload");
        const newData: Promise<Response> = this.uploadFiles(files);
        newData.then((response: Response) => {
            response.json().then(data => {
                console.log(data);
                if (response.ok) {
                    const result: AddFileResult = data as AddFileResult;
                    console.log(result);
                    const contentAndCategories: [FileResultsContainerContent, Array<string>] = FileResultsProcessor.generateStateFromResult(result);
                    this.setState({ content: contentAndCategories[0],
                                    categories: contentAndCategories[1] });
                }
            });
        })
        .catch((error: Error) => console.log("Failed to execute request and update: " + error.toString()));
    }

    addRule = () : void => {
        const data: FormData = new FormData();
        const ruleString: string = this.state.content.addRuleContent.rule;
        const category: string = this.state.content.addRuleContent.category;
        if (ruleString !== null
            && ruleString.length > 0
            && category !== null) {
            data.append(JsonKey.RULE, ruleString);
            data.append(JsonKey.CATEGORY, category);
            ServerCalls.singlePostRequest("http://localhost:8080/merchants/rules/add",
                                          data,
                                          (data: object) : void => {
                                                const affectedMerchants: Array<string> = this.state.content.resultsData.expenditures.map(expenditure => expenditure.merchant)
                                                                                                                                    .filter(merchant => merchant.toLowerCase().includes(ruleString.toLowerCase()));
                                                const newResultsData: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.updateStateWithNewMerchantCategories(affectedMerchants, category, this.state.content.resultsData);
                                                const newContent: FileResultsContainerContent = new FileResultsContainerContent(this.state.content.isDataPresent,
                                                                                                                            this.state.content.failedFiles,
                                                                                                                            newResultsData,
                                                                                                                            this.state.content.addRuleContent,
                                                                                                                            this.state.content.addIgnoreRuleContent);
                                                newContent.addRuleContent.rule = null;
                                                this.setState( { content: newContent });
                                          },
                                          "Failed to add merchant rule");
        }
        else {
            console.log("Aborting addRule call due to invalid rule string (" + ruleString + ") or category (" + category + ")");
        }
    }

    addIgnoreRule = () : void => {
        const data: FormData = new FormData();
        const ruleString: string = this.state.content.addIgnoreRuleContent.rule;
        if (ruleString !== null
            && ruleString.length > 0) {
            data.append(JsonKey.RULE, ruleString);
            ServerCalls.singlePostRequest("http://localhost:8080/merchants/rules/ignore/add",
                                          data,
                                          (data: object) : void => {
                                                const affectedMerchants: Array<string> = this.state.content.resultsData.expenditures.map(expenditure => expenditure.merchant)
                                                                                                                                    .filter(merchant => merchant.toLowerCase().includes(ruleString.toLowerCase()));
                                                const newResultsData: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.generateContentWithoutMerchants(affectedMerchants, this.state.content.resultsData);
                                                const newContent: FileResultsContainerContent = new FileResultsContainerContent(this.state.content.isDataPresent,
                                                                                                                    this.state.content.failedFiles,
                                                                                                                    newResultsData,
                                                                                                                    this.state.content.addRuleContent,
                                                                                                                    this.state.content.addIgnoreRuleContent);
   

                                                newContent.addIgnoreRuleContent.rule = null;
                                                this.setState( { content: newContent });
                                          },
                                          "Failed to add ignore rule");
        }
        else {
            console.log("Aborting addIgnoreRule call due to invalid rule string (" + ruleString + ")");
        }
    }

    updateAddRuleCategory = (category: string) : void => {
        const newContent: FileResultsContainerContent = this.state.content;
        newContent.addRuleContent.category = category;
        this.setState({ content: newContent });
    }

    updateAddRuleString = (ruleString: string) : void => {
        const newContent: FileResultsContainerContent = this.state.content;
        newContent.addRuleContent.rule = ruleString;
        this.setState({ content: newContent });
    }

    updateIgnoreRuleString = (ruleString: string) : void => {
        const newContent: FileResultsContainerContent = this.state.content;
        newContent.addIgnoreRuleContent.rule = ruleString;
        this.setState({ content: newContent });
    }

    onEntryCheckbox = (id: number) : void => {
        if (this.state.content.resultsData.expenditures.find(item => item.id === id)) {
            const updatedEntries: Set<number> = this.state.content.resultsData.checkedEntries;
            if (updatedEntries.has(id)) {
                updatedEntries.delete(id);
            }
            else {
                updatedEntries.add(id);
            }
            const newContent: FileResultsContainerContent = this.state.content;
            newContent.resultsData.checkedEntries = updatedEntries;
            this.setState({ content: newContent })
        }
    }

    onMerchantCheckbox = (name: string) : void => {
        if (Array.from(this.state.content.resultsData.merchantsByCategory.keys()).find(item => item === name)) {
            const updatedCheckedMerchants: Set<string> = this.state.content.resultsData.checkedMerchants;
            if (updatedCheckedMerchants.has(name)) {
                updatedCheckedMerchants.delete(name);
            }
            else {
                updatedCheckedMerchants.add(name);
            }
            const newContent: FileResultsContainerContent = this.state.content;
            newContent.resultsData.checkedMerchants = updatedCheckedMerchants;
            this.setState({ content: newContent })
        }
    }

    uploadFiles = (fileList: FileList) : Promise<Response> => {
        const data: FormData = new FormData();
        for (let i = 0; i < fileList.length; i++) {
            data.append(JsonKey.FILES, fileList.item(i));
        }
        return fetch("http://localhost:8080/parse", {
            method: 'POST',
            body: data,
            referrer: 'no-referrer',
        });
    }


    toggleMerchantsDisplay = () : void => {
        const newEntriesAndMerchantsState: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.toggleMerchantsDisplay(this.state.content.resultsData);
        const newContent: FileResultsContainerContent = this.state.content;
        newContent.resultsData = newEntriesAndMerchantsState;
        this.setState( { content: newContent });
    }

    toggleEntriesDisplay = () : void => {
        const newEntriesAndMerchantsState: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.toggleExpendituresDisplay(this.state.content.resultsData);
        const newContent: FileResultsContainerContent = this.state.content;
        newContent.resultsData = newEntriesAndMerchantsState;
        this.setState( { content: newContent });
    }

    packageEntriesFunctions = () : EntriesFunctions => {
        return new EntriesFunctions(this.onEntryCheckbox,
                                            this.batchUpdateEntries,
                                            this.updateCategorySelectionForEntry,
                                            this.updateCategoryForEntry,
                                            this.updateMerchantFromEntryId,
                                            this.updateBatchEntrySelection,
                                            this.toggleEntriesDisplay,
                                            this.deleteEntry);
    }

    packageMerchantsFunctions = () : MerchantsFunctions => {
        return new MerchantsFunctions(this.onMerchantCheckbox,
            this.batchUpdateMerchants,
            this.updateCategorySelectionForMerchant,
            this.updateCategoryForMerchant,
            this.updateBatchMerchantSelection,
            this.ignoreMerchant,
            this.toggleMerchantsDisplay);
    }

    packageAddRuleFunctions = () : AddRuleFunctions => {
        return new AddRuleFunctions(this.addRule,
                                    this.updateAddRuleCategory,
                                    this.updateAddRuleString);
    }

    packageAddIgnoreRuleFunctions = () : AddIgnoreRuleFunctions => {
        return new AddIgnoreRuleFunctions(this.addIgnoreRule,
                                          this.updateIgnoreRuleString);
    }

    public render() {
//         console.log("Rendering AddFiles content container");
        const hasFileBeenLoaded: boolean = this.state.content !== null;
        return <div className={localStyles.addFilesContentContainer}>
        <div className={localStyles.fileSelectContainer}>
            <Columns>
              <Columns.Column size={12}>
                  <FileSelect  name={FILE_NAME} 
                               directions={SectionTitle.FILE_SELECT}
                               onUpload={this.onFileSelect} />
              </Columns.Column>
            </Columns>
        </div>
        {hasFileBeenLoaded && 
            <div className={localStyles.resultsContainer}>
                <Columns>
                  <Columns.Column size={12}>
                      <AddRule rule={this.state.content.addRuleContent.rule}
                                                  category={this.state.content.addRuleContent.category}
                                                  categories={this.state.categories}
                                                  parentFunctions={this.packageAddRuleFunctions()}
                                                  />
                      <AddIgnoreRule rule={this.state.content.addIgnoreRuleContent.rule}
                                                        parentFunctions={this.packageAddIgnoreRuleFunctions()}
                                                        />
                      <FileResultsContainer title={SectionTitle.RESULTS}
                                          data={this.state.content}
                                          categories={this.state.categories}
                                          merchantsFunctions={this.packageMerchantsFunctions()}
                                          entriesFunctions={this.packageEntriesFunctions()} />
                  </Columns.Column>
                </Columns>
            </div>
        }
        </div>;
    }
}
