import { FailedFile } from "./failedfiles/FailedFile"
import { Expenditure } from "./../common/entries/Expenditure"

export interface AddFileResult {
    failedFiles: Array<FailedFile>;
    insertedExpenditures: Array<Expenditure>;
    uncategorizedMerchants: Array<string>;
    categories: Array<string>;
}