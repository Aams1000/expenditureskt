import { FailedFile } from "../failedfiles/FailedFile";
import { Expenditure } from "../../common/entries/Expenditure";
import { EntriesAndMerchantsState } from "../../common/containers/EntriesAndMerchantsState";
import { AddRuleContent } from "../../manage/merchantcategoryrules/AddRuleContent";
import { AddIgnoreRuleContent } from "../../manage/ignoredMerchants/rules/AddIgnoreRuleContent";

export class FileResultsContainerContent {

     isDataPresent: boolean;
     failedFiles: Array<FailedFile>;
     resultsData: EntriesAndMerchantsState;
     addRuleContent: AddRuleContent;
     addIgnoreRuleContent: AddIgnoreRuleContent;

     constructor(isDataPresent: boolean,
                failedFiles: Array<FailedFile>,
                resultsData: EntriesAndMerchantsState,
                addRuleContent: AddRuleContent,
                addIgnoreRuleContent: AddIgnoreRuleContent) {
        this.isDataPresent = isDataPresent;
        this.failedFiles = failedFiles;
        this.resultsData = resultsData;
        this.addRuleContent = addRuleContent;
        this.addIgnoreRuleContent = addIgnoreRuleContent;
     }
}

