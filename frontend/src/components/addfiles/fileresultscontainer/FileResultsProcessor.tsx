import { FileResultsContainerContent } from "./FileResultsContainerContent";
import { Expenditure } from "../../common/entries/Expenditure";
import { FailedFile } from "../failedfiles/FailedFile";
import { AddFileResult } from "../AddFileResult";
import * as MapUtils from "../../common/MapUtils";
import { EntriesAndMerchantsState } from "../../common/containers/EntriesAndMerchantsState";
import * as EntriesAndMerchantsStateProcessor from "../../common/containers/EntriesAndMerchantsStateProcessor";
import { AddRuleContent } from "../../manage/merchantcategoryrules/AddRuleContent";
import { AddIgnoreRuleContent } from "../../manage/ignoredMerchants/rules/AddIgnoreRuleContent";

export function generateStateFromResult(data: AddFileResult) : [FileResultsContainerContent, Array<string>] {
    const isDataPresent: boolean = data !== null;
    const failedFiles: Array<FailedFile> = isDataPresent ? data.failedFiles : null;
    const expenditures: Array<Expenditure> = isDataPresent ? data.insertedExpenditures : null;
    const merchants: Array<string> = isDataPresent ? data.uncategorizedMerchants : null;
    const categories: Array<string> = isDataPresent ? data.categories : new Array();
    const shouldDisplay: boolean = true;
    const entriesAndMerchantsState: EntriesAndMerchantsState = EntriesAndMerchantsStateProcessor.generateInitialState(isDataPresent, expenditures, merchants, shouldDisplay);
    const content: FileResultsContainerContent = new FileResultsContainerContent(
               isDataPresent,
               failedFiles,
               entriesAndMerchantsState,
               AddRuleContent.generateDefault(),
               AddIgnoreRuleContent.generateDefault());
    return [content, categories];
}

