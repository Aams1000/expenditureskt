import * as React from "react";
import { Columns } from "react-bulma-components";
import { AddFileResult } from "../AddFileResult";
import { FailedFileContainer } from "../failedfiles/container/FailedFileContainer";
import { FailedFile } from "../failedfiles/FailedFile";
import { EntriesContainer } from "../../common/entries/container/EntriesContainer";
import { Expenditure } from "../../common/entries/Expenditure";
import { FileResultsContainerContent } from "./FileResultsContainerContent";
import { MerchantsContainer } from "../../common/merchants/container/MerchantsContainer";
import * as MapUtils from "../../common/MapUtils";
import * as FileResultsProcessor from "./FileResultsProcessor";
import { EntriesFunctions } from "../../common/entries/EntriesFunctions";
import { MerchantsFunctions } from "../../common/merchants/MerchantsFunctions";
import * as MerchantsHeaders from "../../common/merchants/MerchantsHeaders";
import * as ExpendituresHeaders from "../../common/entries/ExpendituresHeaders";
import * as FailedFileHeaders from "../failedFiles/FailedFileHeaders"
import * as CheckboxSetup from "../../common/containers/CheckboxSetup";

const localStyles = require('./FileResultsContainer.module.less');

export interface FileResultsContainerProps { title: string;
                                            categories: Array<string>;
                                            data: FileResultsContainerContent;
                                            merchantsFunctions: MerchantsFunctions;
                                            entriesFunctions: EntriesFunctions; }

export class FileResultsContainer extends React.Component<FileResultsContainerProps, {}> {

    generateContent = () : JSX.Element => {
        let content: JSX.Element = null;
        if (this.props.data.isDataPresent) {
            content = <div className={localStyles.resultsContainer}>
                        <div className={localStyles.failedFileContainer}>
                            <Columns className={localStyles.fullWidthColumns}>
                              <Columns.Column size={12}>
                                  <FailedFileContainer
                                      titles={FailedFileHeaders.HEADERS}
                                      data={this.props.data.failedFiles}
                                  />
                              </Columns.Column>
                            </Columns>
                        </div>
                        <div className={localStyles.newMerchantsContainer}>
                            <Columns className={localStyles.fullWidthColumns}>
                              <Columns.Column size={12}>
                                  <MerchantsContainer
                                      titles={MerchantsHeaders.NEW_MERCHANTS}
                                      merchantsByCategory={this.props.data.resultsData.merchantsByCategory}
                                      shouldShowCategory={false}
                                      merchantAndBoxStatus={CheckboxSetup.generateEntitiesAndStatus(Array.from(this.props.data.resultsData.merchantsByCategory.keys()), this.props.data.resultsData.checkedMerchants)}
                                      categories={this.props.categories}
                                      isDisplayed={this.props.data.resultsData.shouldShowMerchants}
                                      parentFunctions={this.props.merchantsFunctions}
                                  />
                              </Columns.Column>
                            </Columns>
                        </div>
                        <div className={localStyles.entriesContainer}>
                            <Columns className={localStyles.fullWidthColumns}>
                              <Columns.Column size={12}>
                                  <EntriesContainer
                                      titles={ExpendituresHeaders.NEW_EXPENDITURES}
                                      data={CheckboxSetup.generateExpendituresAndStatus(this.props.data.resultsData.expenditures, this.props.data.resultsData.checkedEntries)}
                                      categories={this.props.categories}
                                      isDisplayed={this.props.data.resultsData.shouldShowEntries}
                                      parentFunctions={this.props.entriesFunctions}
                                  />
                              </Columns.Column>
                            </Columns>
                        </div>
                    </div>;

        }
        return content;
    }

    public render() {
//         console.log("Rendering FileResultsContainer");
        return this.generateContent();
    }
}
