import tinycolor from "tinycolor2";

export function transparentize(value: string, opacity: number = 0.5) : string {
  var alpha = 1 - opacity;
  return tinycolor(value).setAlpha(alpha).toRgbString();
}
