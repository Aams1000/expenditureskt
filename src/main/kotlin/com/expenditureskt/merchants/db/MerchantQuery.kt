package com.expenditureskt.merchants.db

import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam

enum class MerchantQuery(override val query: String,
                         override val queryIndicesByParam: Map<QueryParam, List<Int>>,
                         override val resultIndexByParam: Map<QueryParam, Int>) : QueryInfo {

    UPDATE_MERCHANT_CATEGORY_AND_EXPENDITURES("update t_merchants_by_category set category = ? where merchant = any(?::text[])",
            mapOf(QueryParam.CATEGORY_ID to listOf(1),
                    QueryParam.MERCHANT_ARRAY to listOf(2)),
            mapOf()
    ),

    RETRIEVE_MERCHANTS_TO_IGNORE("select merchant from t_merchants_to_ignore order by merchant",
        mapOf(),
        mapOf(QueryParam.MERCHANT to 1)
    ),

    QUERY_MERCHANTS_BY_CATEGORY("select merchant, category, insertion_date " +
            "from t_merchants_by_category m " +
            "where not exists (select 1 from t_merchants_to_ignore i where m.merchant = i.merchant) " +
            "order by insertion_date desc",

            mapOf(),
            mapOf(QueryParam.MERCHANT to 1,
                    QueryParam.CATEGORY_ID to 2)
    ),

    BACKUP_MERCHANTS_BY_CATEGORY("drop table if exists t_merchants_by_category_backup; " +
            "create table t_merchants_by_category_backup (like t_merchants_by_category including all); " +
            "insert into t_merchants_by_category_backup select * from t_merchants_by_category;",

            mapOf(),
            mapOf()
    ),

    INSERT_INTO_MERCHANTS_BY_CATEGORY("insert into t_merchants_by_category values (?, ?, ?) on conflict do nothing",

            mapOf(QueryParam.MERCHANT to listOf(1),
                    QueryParam.CATEGORY_ID to listOf(2),
                    QueryParam.INSERTION_DATE to listOf(3)),
            mapOf()
    ),

    INSERT_MERCHANTS_TO_IGNORE("insert into t_merchants_to_ignore values (?)",

            mapOf(QueryParam.MERCHANT to listOf(1)),
            mapOf()
    ),

    ADD_MERCHANT_IGNORE_PATTERN("insert into t_merchant_ignore_rules values (default, ?)",

            mapOf(QueryParam.MERCHANT_PATTERN to listOf(1)),
            mapOf()
    ),

    UPDATE_MERCHANT_IGNORE_PATTERN("update t_merchant_ignore_rules set pattern = ? where pattern = ?",

            mapOf(QueryParam.MERCHANT_PATTERN_NEW to listOf(1),
                    QueryParam.MERCHANT_PATTERN to listOf(2)),
            mapOf()
    ),

    DELETE_MERCHANT_IGNORE_RULE("delete from t_merchant_ignore_rules where pattern = ?",

            mapOf(QueryParam.MERCHANT_PATTERN to listOf(1)),
            mapOf()

    ),

    RETRIEVE_MERCHANT_IGNORE_RULES("select pattern from t_merchant_ignore_rules order by pattern",

            mapOf(),
            mapOf(QueryParam.MERCHANT_PATTERN to 1)
    ),

    UNIGNORE_MERCHANTS("with deletion_query as (delete from t_merchants_to_ignore where merchant = ? returning merchant) " +
                             "update t_merchants_by_category set category = ? where merchant = (select merchant from deletion_query);",

            mapOf(QueryParam.MERCHANT to listOf(1),
                  QueryParam.CATEGORY_ID to listOf(2)
            ),
            mapOf()
    ),

    DELETE_MERCHANT("delete from t_merchants_by_category where merchant = ?",
        mapOf(QueryParam.MERCHANT to listOf(1)),
        mapOf()
    ),

    DELETE_MERCHANT_TO_IGNORE("delete from t_merchants_to_ignore where merchant = ?",

            mapOf(QueryParam.MERCHANT to listOf(1)),
            mapOf()
    ),

    RETRIEVE_MERCHANT_RULES("select pattern, category from t_merchant_pattern_rules",

            mapOf(),
            mapOf(QueryParam.MERCHANT_PATTERN to 1,
                  QueryParam.CATEGORY_ID to 2)
    ),

    INSERT_MERCHANT_RULE("insert into t_merchant_pattern_rules values (default, ?, ?)",
            mapOf(QueryParam.MERCHANT_PATTERN to listOf(1),
                  QueryParam.CATEGORY_ID to listOf(2)),
            mapOf()
    ),

    UPDATE_MERCHANT_RULE_PATTERN("update t_merchant_pattern_rules set pattern = ? where pattern = ?",
            mapOf(QueryParam.MERCHANT_PATTERN_NEW to listOf(1),
                  QueryParam.MERCHANT_PATTERN to listOf(2)),
            mapOf()
    ),

    UPDATE_MERCHANT_RULE_CATEGORIES("update t_merchant_pattern_rules set category = ? where category = any(?::integer[])",
            mapOf(QueryParam.REPLACEMENT_ID to listOf(1),
                  QueryParam.IDS_TO_REPLACE to listOf(2)),
            mapOf()
    ),

    UPDATE_MERCHANT_RULE_CATEGORY("update t_merchant_pattern_rules set category = ? where pattern = ?",
            mapOf(QueryParam.CATEGORY_ID to listOf(1),
                  QueryParam.MERCHANT_PATTERN to listOf(2)),
            mapOf()
    ),

    DELETE_MERCHANT_RULE("delete from t_merchant_pattern_rules where pattern = ?",
            mapOf(QueryParam.MERCHANT_PATTERN to listOf(1)),
            mapOf()
    ),

    COUNT_EXPENDITURES_BY_MERCHANTS("select count(*) from t_expenditures where merchant = any(?::text[])",

            mapOf(QueryParam.MERCHANT_ARRAY to listOf(1)),
            mapOf(QueryParam.COUNT to 1)
    );
}