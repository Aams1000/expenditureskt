package com.expenditureskt.merchants.db

import com.expenditureskt.categories.Category
import com.expenditureskt.common.db.DatabaseConnections
import com.expenditureskt.common.db.PreparedStatements
import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException

object MerchantsToIgnore {

    fun addMerchantsToIgnore(merchantsToIgnore: Set<String>) : Int {
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                addMerchants(connection, merchantsToIgnore)
                val numExpendituresAffected: Int = MerchantsUpdates.deleteMerchants(connection, merchantsToIgnore)
                return numExpendituresAffected
            }
        }
        catch (e: SQLException) {
            println("Failed to add merchants to ignore due to SQLException: $e")
            throw e
        }
    }

    fun unignoreMerchants(merchants: Set<String>, category: Category) : Int {
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                val queryInfo: QueryInfo = MerchantQuery.UNIGNORE_MERCHANTS
                connection.prepareStatement(queryInfo.query).use { statement ->
                    val categoryId: Int = category.id
                    for (merchant: String in merchants) {
                        PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT), merchant)
                        PreparedStatements.setInts(statement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), categoryId)
                        statement.addBatch()
                    }
                    statement.executeBatch()
                    return statement.updateCount
                }
            }
        }
        catch (e: SQLException) {
            println("Failed to unignore merchants due to SQLException $e")
            throw e
        }
    }

    fun addNewRule(pattern: String) : Int {
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                val queryInfo: QueryInfo = MerchantQuery.ADD_MERCHANT_IGNORE_PATTERN
                connection.prepareStatement(queryInfo.query).use { statement ->
                    PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT_PATTERN), pattern)
                    statement.executeUpdate()
                    return statement.updateCount
                }
            }
        }
        catch (e: Exception) {
            println("Failed to insert merchant ignore rule $pattern due to exception $e")
            throw e
        }
    }

    fun updateRulePattern(oldPattern: String, newPattern: String) : Int {
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                val queryInfo: QueryInfo = MerchantQuery.UPDATE_MERCHANT_IGNORE_PATTERN
                connection.prepareStatement(queryInfo.query).use { statement ->
                    PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT_PATTERN_NEW), newPattern)
                    PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT_PATTERN), oldPattern)
                    statement.executeUpdate()
                    return statement.updateCount
                }
            }
        }
        catch (e: Exception) {
            println("Failed to update merchant ignore rule $oldPattern to $newPattern due to exception $e")
            throw e
        }
    }

    fun deleteRules(patterns: Array<String>) : Int {
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                val queryInfo: QueryInfo = MerchantQuery.DELETE_MERCHANT_IGNORE_RULE
                connection.prepareStatement(queryInfo.query).use { statement ->
                    for (pattern: String in patterns) {
                        PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT_PATTERN), pattern)
                        statement.addBatch()
                    }
                    statement.executeBatch()
                    return statement.updateCount
                }
            }
        }
        catch (e: Exception) {
            println("Failed to delete merchant ignore rules $patterns due to exception $e")
            throw e
        }
    }

    fun getMerchantsToIgnore() : Set<String> {
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                val queryInfo: QueryInfo = MerchantQuery.RETRIEVE_MERCHANTS_TO_IGNORE
                connection.prepareStatement(queryInfo.query). use { statement ->
                    val merchants: MutableList<String> = mutableListOf()
                    val resultSet: ResultSet = statement.executeQuery()
                    while (resultSet.next()) {
                        merchants.add(resultSet.getString(queryInfo.getResultIndex(QueryParam.MERCHANT)))
                    }
                    return merchants.toSet()
                }
            }
        }
        catch (e: SQLException) {
            println("Failed to add merchants to ignore due to SQLException: $e")
            throw e
        }
    }

    fun getMerchantIgnoreRules() : Set<String> {
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                val queryInfo: QueryInfo = MerchantQuery.RETRIEVE_MERCHANT_IGNORE_RULES
                connection.prepareStatement(queryInfo.query).use { statement ->
                    val rules: MutableSet<String> = mutableSetOf()
                    val resultSet: ResultSet = statement.executeQuery()
                    while (resultSet.next()) {
                        rules.add(resultSet.getString(queryInfo.getResultIndex(QueryParam.MERCHANT_PATTERN)))
                    }
                    return rules.toSet()
                }
            }
        }
        catch (e: SQLException) {
            println("Failed to retrieve merchant ignore rules due to SQLException: $e")
            throw e
        }
    }

    private fun addMerchants(connection: Connection, merchantsToIgnore: Set<String>) : Unit {
        val queryInfo: QueryInfo = MerchantQuery.INSERT_MERCHANTS_TO_IGNORE
        try {
            connection.prepareStatement(queryInfo.query).use { statement ->
                for (merchant: String in merchantsToIgnore) {
                    PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT), merchant)
                    statement.addBatch()
                }
                statement.executeBatch()
            }
        }
        catch (e: SQLException) {
            println("Failed to insert new merchants to ignore due to SQLException: $e")
            throw e
        }
    }

    fun countExpendituresByMerchant(connection: Connection, merchants: Set<String>) : Int {
        val queryInfo: QueryInfo = MerchantQuery.COUNT_EXPENDITURES_BY_MERCHANTS
        try {
           connection.prepareStatement(queryInfo.query).use { statement ->
               PreparedStatements.setArrays(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT_ARRAY), DatabaseConnections.convertStringListToSqlArray(connection, merchants.toList()))
               val resultSet: ResultSet = statement.executeQuery()
               return resultSet.getInt(queryInfo.getResultIndex(QueryParam.COUNT))
           }
        }
        catch (e: SQLException) {
            println("Failed to query expenditures by merchant due to ignore due to SQLException: $e")
            throw e
        }
    }
}
