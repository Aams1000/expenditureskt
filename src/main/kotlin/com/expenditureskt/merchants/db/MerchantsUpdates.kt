package com.expenditureskt.merchants.db

import com.expenditureskt.categories.Category
import com.expenditureskt.categories.db.CategoryLookup
import com.expenditureskt.common.db.DatabaseConnections
import com.expenditureskt.common.db.PreparedStatements
import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam
import com.expenditureskt.parsers.ParsingServlet
import com.expenditureskt.parsers.db.SpendingUpdates
import java.lang.Exception
import java.sql.Connection
import java.sql.SQLException
import java.time.LocalDateTime

object MerchantsUpdates {

    // leaving this name explicit so as to convey the fact that all expenditures from this merchant
    // are updated after the merchant table update
    fun updateMerchantsAndExpenditures(merchants: Set<String>,
                                       newCategory: Category) : Int {
        try {
            var numUpdatedRows: Int = -1
            DatabaseConnections.getNonAutoCommitExpendituresConnection().use { connection ->
                backupMerchantsTable(connection)
                SpendingUpdates.backupExpendituresTable(connection)
                val categoryId: Int = newCategory.id
                numUpdatedRows = updateCategoriesAndEntriesForMerchants(connection, merchants, categoryId)
                try {
                    connection.commit()
                }
                catch (e: SQLException) {
                    println("Failed to execute update due to SQLException: $e")
                    try {
                        connection.rollback()
                    }
                    catch (e2: SQLException) {
                        println("Rollback failed. Something is very wrong $e2")
                        throw e2
                    }
                }
            }
            return numUpdatedRows
        }
        catch (e: SQLException) {
            println("Failed to update merchants due to exception $e")
            throw e
        }
    }

    // NOTE - no backup for this right now
    fun deleteMerchants(connection: Connection, merchants: Set<String>) : Int {
        val queryInfo: QueryInfo = MerchantQuery.DELETE_MERCHANT
        try {
            connection.prepareStatement(queryInfo.query).use { statement ->
                for (merchant: String in merchants) {
                    PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT), merchant)
                    statement.addBatch()
                }
                statement.executeBatch()
                return statement.updateCount
            }
        }
        catch (e: Exception) {
            println("Failed to delete merchants due to exception: $e")
            throw e
        }
    }

    private fun updateCategoriesAndEntriesForMerchants(connection: Connection, merchants: Set<String>, categoryId: Int) : Int {
        println("Updating merchants $merchants to category ID $categoryId")
        val queryInfo: QueryInfo = MerchantQuery.UPDATE_MERCHANT_CATEGORY_AND_EXPENDITURES
        try {
            connection.prepareStatement(queryInfo.query).use { statement ->
                PreparedStatements.setArrays(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT_ARRAY), DatabaseConnections.convertStringListToSqlArray(connection, merchants.toList()))
                PreparedStatements.setInts(statement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), categoryId)
                println(statement)
                statement.executeUpdate()
                return statement.updateCount
            }
        }
        catch (e: Exception) {
            println("Failed to update merchant categories due to exception: $e")
            throw e
        }
    }

    fun addNewMerchants(connection: Connection, merchantsByDateTime: Map<LocalDateTime, List<ParsingServlet.MerchantAndId>>) : Unit {
        try {
            val queryInfo: QueryInfo = MerchantQuery.INSERT_INTO_MERCHANTS_BY_CATEGORY
            connection.prepareStatement(queryInfo.query).use { statement ->
                for ((dateTime, merchants) in merchantsByDateTime) {
                    for (merchantAndId in merchants) {
                        PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT), merchantAndId.name)
                        PreparedStatements.setInts(statement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), merchantAndId.id)
                        PreparedStatements.setTimestamps(statement, queryInfo.getQueryIndices(QueryParam.INSERTION_DATE), dateTime)
                        statement.addBatch()
                    }
                }
                statement.executeBatch()
            }
        }
        catch (e: Exception) {
            println("Failed to add new merchants due to exception: $e")
            throw e
        }
    }

    fun backupMerchantsTable(connection: Connection) : Unit {
        try {
            connection.prepareStatement(MerchantQuery.BACKUP_MERCHANTS_BY_CATEGORY.query).use { statement ->
                statement.execute()
            }
        }
        catch (e: SQLException) {
            println("Failed to backup merchants table due to SQLException: $e")
            throw e
        }
    }
}