package com.expenditureskt.merchants.db

import com.expenditureskt.categories.Category
import com.expenditureskt.common.db.DatabaseConnections
import com.expenditureskt.common.db.PreparedStatements
import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam
import java.sql.Connection

object MerchantRuleUpdates {

    fun addNewRule(pattern: String, category: Category) : Int {
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                val queryInfo: QueryInfo = MerchantQuery.INSERT_MERCHANT_RULE
                connection.prepareStatement(queryInfo.query).use { statement ->
                    PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT_PATTERN), pattern)
                    PreparedStatements.setInts(statement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), category.id)
                    statement.executeUpdate()
                    return statement.updateCount
                }
            }
        }
        catch (e: Exception) {
            println("Failed to insert merchant rule $pattern to category ${category.name} due to exception $e")
            throw e
        }
    }

    fun updateRulePattern(oldPattern: String, newPattern: String) : Int {
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                val queryInfo: QueryInfo = MerchantQuery.UPDATE_MERCHANT_RULE_PATTERN
                connection.prepareStatement(queryInfo.query).use { statement ->
                    PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT_PATTERN_NEW), newPattern)
                    PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT_PATTERN), oldPattern)
                    statement.executeUpdate()
                    return statement.updateCount
                }
            }
        }
        catch (e: Exception) {
            println("Failed to update merchant rule $oldPattern to $newPattern due to exception $e")
            throw e
        }
    }

    fun updateRuleCategory(patterns: Array<String>, category: Category) : Int {
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                val queryInfo: QueryInfo = MerchantQuery.UPDATE_MERCHANT_RULE_CATEGORY
                connection.prepareStatement(queryInfo.query).use { statement ->
                    for (pattern: String in patterns) {
                        PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT_PATTERN), pattern)
                        PreparedStatements.setInts(statement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), category.id)
                        statement.addBatch()
                    }
                    statement.executeBatch()
                    return statement.updateCount
                }
            }
        }
        catch (e: Exception) {
            println("Failed to update merchant rules $patterns to category ${category.name} due to exception $e")
            throw e
        }
    }

    fun updateRuleCategories(connection: Connection, categoriesToUpdate: List<Int>, replacementId: Int) : Int {
        val queryInfo: QueryInfo = MerchantQuery.UPDATE_MERCHANT_RULE_CATEGORIES
        connection.prepareStatement(queryInfo.query).use { statement ->
            PreparedStatements.setInts(statement, queryInfo.getQueryIndices(QueryParam.REPLACEMENT_ID), replacementId)
            PreparedStatements.setArrays(statement, queryInfo.getQueryIndices(QueryParam.IDS_TO_REPLACE), DatabaseConnections.convertIntListToSqlArray(connection, categoriesToUpdate))
            statement.executeUpdate()
            return statement.updateCount
        }
    }

    fun deleteRules(patterns: Array<String>) : Int {
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                val queryInfo: QueryInfo = MerchantQuery.DELETE_MERCHANT_RULE
                connection.prepareStatement(queryInfo.query).use { statement ->
                    for (pattern: String in patterns) {
                        PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT_PATTERN), pattern)
                        statement.addBatch()
                    }
                    statement.executeBatch()
                    return statement.updateCount
                }
            }
        }
        catch (e: Exception) {
            println("Failed to delete merchant rules $patterns due to exception $e")
            throw e
        }
    }
}

