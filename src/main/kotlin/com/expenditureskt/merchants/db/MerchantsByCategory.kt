package com.expenditureskt.merchants.db

import com.expenditureskt.categories.Category
import com.expenditureskt.categories.db.CategoryLookup
import com.expenditureskt.common.db.DatabaseConnections
import com.expenditureskt.common.db.PreparedStatements
import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam
import com.expenditureskt.common.db.QueryResult
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException

object MerchantsByCategory {

    fun queryMerchantsByCategory() : Map<String, Category> {
        val queryInfo: QueryInfo = MerchantQuery.QUERY_MERCHANTS_BY_CATEGORY
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                connection.prepareStatement(queryInfo.query).use { statement ->
                    println(statement)
                    val resultSet: ResultSet = statement.executeQuery()
                    val merchantsToCategory: MutableMap<String, Category> = mutableMapOf()
                    while (resultSet.next()) {
                        val merchant: String = resultSet.getString(queryInfo.getResultIndex(QueryParam.MERCHANT))
                        val categoryId: Int = resultSet.getInt(queryInfo.getResultIndex(QueryParam.CATEGORY_ID))
                        val category: Category? = CategoryLookup.getCategoryForId(categoryId)
                        if (category != null) {
                            merchantsToCategory[merchant] = category
                        } else {
                            println("Unknown expenditure category $categoryId retrieved from database.")
                        }
                    }
                    return merchantsToCategory
                }
            }
        }
        catch (e: SQLException) {
            println("Failed to execute query due to exception: $e")
            throw e
        }
    }

    fun insertMerchantsByCategory(merchantsByCategory: Map<String, Category>) : QueryResult {
        val queryInfo: QueryInfo = MerchantQuery.INSERT_INTO_MERCHANTS_BY_CATEGORY
        var result: QueryResult = QueryResult.SUCCESS
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                connection.prepareStatement(queryInfo.query).use { statement ->
                    backupMerchantsByCategory(connection)
                    for ((merchant: String, category: Category) in merchantsByCategory) {
                        PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT), merchant)
                        PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), category.name)
                        statement.addBatch()
                    }
                    statement.executeBatch()
                }
            }
        }
        catch (e: SQLException) {
            println("Failed to insert merchants by category due to exception: $e")
            result = QueryResult.FAILURE
        }
        return result
    }

    private fun backupMerchantsByCategory(connection: Connection) : Unit {
        try {
            connection.prepareStatement(MerchantQuery.BACKUP_MERCHANTS_BY_CATEGORY.query).use { statement ->
                statement.execute()
            }
        }
         catch (e: SQLException) {
                println("Failed to backup merchants by category due to SQLSQLException: $e")
                throw e
        }
    }
}