package com.expenditureskt.merchants

import com.expenditureskt.categories.Category
import com.expenditureskt.categories.db.CategoryLookup
import com.expenditureskt.common.db.DatabaseConnections
import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam
import com.expenditureskt.merchants.db.MerchantQuery
import com.expenditureskt.merchants.db.MerchantsByCategory
import com.expenditureskt.merchants.db.MerchantsToIgnore
import java.security.InvalidKeyException
import java.sql.ResultSet
import java.sql.SQLException

object MerchantRules {

    enum class RuleType {
        CATEGORY,
        IGNORE
    }

    // Initially wanted to make these regexes, but when adding a new rule I'd have to verify that
    // the new pattern is not a "subset" of any other pattern (and vice versa)
    // This is actually far more complicated and expensive than it sounds, so I'm restricting
    // the functionality to simple substring rules for the moment
    private var m_patternsByCategory: Map<String, Category> = mapOf()
    private var m_ignorePatterns: Set<String> = setOf()

    init {
        refreshRulesFromDatabase()
    }

    fun refreshRulesFromDatabase() : Unit {
        val patternAndIgnoreRules: PatternAndIgnoreRules = retrievePatternsFromDatabase()
        m_patternsByCategory = patternAndIgnoreRules.patternRules
        m_ignorePatterns = patternAndIgnoreRules.ignoreRules
    }

    private fun retrievePatternsFromDatabase(): PatternAndIgnoreRules {
        val patternsToCategory: MutableMap<String, Category> = mutableMapOf()
        val ignorePatterns: MutableSet<String> = mutableSetOf()
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                val categoryQueryInfo: QueryInfo = MerchantQuery.RETRIEVE_MERCHANT_RULES
                connection.prepareStatement(categoryQueryInfo.query).use { statement ->
                    val resultSet: ResultSet = statement.executeQuery()
                    while (resultSet.next()) {
                        val pattern: String = resultSet.getString(categoryQueryInfo.getResultIndex(QueryParam.MERCHANT_PATTERN))
                        val categoryId: Int = resultSet.getInt(categoryQueryInfo.getResultIndex(QueryParam.CATEGORY_ID))
                        val category: Category = CategoryLookup.getCategoryForId(categoryId)!!
                        patternsToCategory[pattern] = category
                    }
                }
                val ignoreQueryInfo: QueryInfo = MerchantQuery.RETRIEVE_MERCHANT_IGNORE_RULES
                connection.prepareStatement(ignoreQueryInfo.query).use { statement ->
                    val resultSet: ResultSet = statement.executeQuery()
                    while (resultSet.next()) {
                        val pattern: String = resultSet.getString(categoryQueryInfo.getResultIndex(QueryParam.MERCHANT_PATTERN))
                        ignorePatterns.add(pattern)
                    }
                }
            }
            return PatternAndIgnoreRules(patternsToCategory.toMap(), ignorePatterns.toSet())
        }
        catch (e: SQLException) {
            println("Failed to retrieve merchant rules due to SQLException $e")
            throw e
        }
        catch (e: InvalidKeyException) {
            println("Failed to retrieve merchant rules due to InvalidKeyException $e")
            throw e
        }
    }

    fun findCategoryForMerchant(merchant: String) : Category? {
//        println("Merchant $merchant")
        val merchantCategory: Category? = m_patternsByCategory.asSequence().firstOrNull { merchant.toLowerCase().contains(it.key) }?.value
//        println("Found category: $merchantCategory")
//        println(m_patternsByCategory)
        return merchantCategory
    }

    fun shouldIgnoreMerchant(merchant: String) : Boolean {
        return m_ignorePatterns.any { merchant.toLowerCase().contains(it) }
    }

    fun isValidFormat(pattern: String) : Boolean {
        return pattern.isNotEmpty() && pattern.isNotBlank()
    }

    fun getRulesToCategories() : Map<String, Category> {
        return m_patternsByCategory
    }

    fun getIgnoreRules() : Set<String> {
        return m_ignorePatterns
    }

    // when is this a problem?
    // Any merchant matches two category rules -> yes
    // Merchant matches category rule and ignore rule -> no
    // Merchant matches two ignore rules -> no
    fun findDoubleMatchingMerchants(replacementPattern: String, oldPattern: String = "") : OverlappingMerchantResult {
        val regularMerchants: Set<String> = MerchantsByCategory.queryMerchantsByCategory().keys
        val ignoredMerchants: Set<String> = MerchantsToIgnore.getMerchantsToIgnore()
        val categoryRules: Set<String> = m_patternsByCategory.keys
        val regularMerchantsToOverlappingCategoryRules: Map<String, List<String>> = findDoubleMatches(regularMerchants, replacementPattern, oldPattern, categoryRules)
        val ignoredMerchantsToOverlappingCategoryRules: Map<String, List<String>> = findDoubleMatches(ignoredMerchants, replacementPattern, oldPattern, categoryRules)
        return OverlappingMerchantResult(regularMerchantsToOverlappingCategoryRules, ignoredMerchantsToOverlappingCategoryRules)
    }

    private fun findDoubleMatches(merchants: Set<String>,
                                  replacementPattern: String,
                                  oldPattern: String,
                                  rules: Set<String>) : Map<String, List<String>> {
        val merchantToOverlappingRules: MutableMap<String, MutableList<String>> = mutableMapOf()
        for (merchant: String in merchants) {
            val lowerCaseMerchant: String = merchant.toLowerCase()
            for (rule: String in rules) {
                if (rule != oldPattern && lowerCaseMerchant.contains(rule) && lowerCaseMerchant.contains(replacementPattern)) {
                    merchantToOverlappingRules.getOrPut(merchant, { mutableListOf() }).add(rule)
                }
            }
        }
        val result: Map<String, List<String>> = merchantToOverlappingRules.entries.associate { (merchant, overlappingRules) -> merchant to overlappingRules.toList() }
        return result
    }

    fun isExistingPattern(pattern: String, ruleType: RuleType) : Boolean {
        return switchOnRuleType(ruleType, { isExistingCategoryRulePattern(pattern) }, { isExistingIgnoreRulePattern(pattern) })
    }

    fun isExistingPattern(pattern: String) : Boolean {
        return isExistingCategoryRulePattern(pattern) || isExistingIgnoreRulePattern(pattern)
    }

    fun findOverlappingRulesPatterns(pattern: String) : List<String> {
        val overlappingCategoryPatterns: List<String> = findOverlappingCategoryRulePatterns(pattern)
        val overlappingIgnorePatterns: List<String> = findOverlappingIgnoreRulePatterns(pattern)
        return overlappingCategoryPatterns + overlappingIgnorePatterns
    }

    fun findOverlappingRulesPatterns(pattern: String, ruleType: RuleType) : List<String> {
        return switchOnRuleType(ruleType, { findOverlappingCategoryRulePatterns(pattern) }, { findOverlappingIgnoreRulePatterns(pattern) })
    }

    fun findOverlappingRulesPatterns(oldPattern: String, replacementPattern: String) : List<String> {
        val categoryRuleOverlaps: List<String> = findOverlappingCategoryRulePatterns(oldPattern, replacementPattern)
        val ignoreRuleOverlaps: List<String> = findOverlappingIgnoreRulePatterns(oldPattern, replacementPattern)
        return categoryRuleOverlaps + ignoreRuleOverlaps
    }

    fun findOverlappingRulesPatterns(oldPattern: String, replacementPattern: String, ruleType: RuleType) : List<String> {
        return switchOnRuleType(ruleType, { findOverlappingCategoryRulePatterns(oldPattern, replacementPattern) }, { findOverlappingIgnoreRulePatterns(oldPattern, replacementPattern) })
    }

    private fun <T> switchOnRuleType(ruleType: RuleType, valueIfCategoryRule: () -> T,  valueIfIgnoreRule: () -> T) : T {
        val result: T = when (ruleType) {
            RuleType.CATEGORY -> valueIfCategoryRule()
            RuleType.IGNORE -> valueIfIgnoreRule()
        }
        return result
    }

    private fun isExistingCategoryRulePattern(pattern: String) : Boolean {
        return m_patternsByCategory.keys.contains(pattern)
    }

    private fun findOverlappingCategoryRulePatterns(pattern: String) : List<String> {
        return m_patternsByCategory.keys.filter{ arePatternsOverlapping(it, pattern) }
    }

    private fun findOverlappingCategoryRulePatterns(oldPattern: String, replacementPattern: String) : List<String> {
        return m_patternsByCategory.keys.filter { it != oldPattern }
                                        .filter { arePatternsOverlapping(it, replacementPattern) }
    }

    private fun isExistingIgnoreRulePattern(pattern: String) : Boolean {
        return pattern in m_ignorePatterns
    }

    private fun findOverlappingIgnoreRulePatterns(pattern: String) : List<String> {
        return m_ignorePatterns.filter{ arePatternsOverlapping(it, pattern) }
    }

    private fun findOverlappingIgnoreRulePatterns(oldPattern: String, replacementPattern: String) : List<String> {
        return m_ignorePatterns.filter { it != oldPattern }
                               .filter { arePatternsOverlapping(it, replacementPattern) }
    }

    private fun arePatternsOverlapping(patternOne: String, patternTwo: String) : Boolean {
        return patternOne.contains(patternTwo) || patternTwo.contains(patternOne)
    }

    private data class PatternAndIgnoreRules(val patternRules: Map<String,Category>, val ignoreRules: Set<String>)

    data class OverlappingMerchantResult(val regularMerchantsAndRules: Map<String, List<String>>, val ignoredMerchantsAndRules: Map<String, List<String>>)
}
