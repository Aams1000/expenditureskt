package com.expenditureskt.merchants

import com.expenditureskt.categories.Category
import com.expenditureskt.categories.db.CategoryLookup
import com.expenditureskt.common.JsonKeys
import com.expenditureskt.common.requests.RequestResponses
import com.expenditureskt.merchants.db.MerchantRuleUpdates
import com.expenditureskt.merchants.db.MerchantsToIgnore
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
class MerchantsRulesServlet {

    @PostMapping("/merchants/rules/add")
    fun addCategoryRule(@RequestParam("pattern") rawPattern: String,
                          @RequestParam("category") categoryString: String) : ResponseEntity<String> {
        val category: Category? = CategoryLookup.getCategoryForName(categoryString)
        val pattern: String = rawPattern.toLowerCase()
        val isValidFormat: Boolean = MerchantRules.isValidFormat(pattern)
        val overlappingPatterns: List<String> = MerchantRules.findOverlappingRulesPatterns(pattern, MerchantRules.RuleType.CATEGORY)
        val response: ResponseEntity<String> = if (category != null && isValidFormat && overlappingPatterns.isEmpty()) {
            handleMerchantRuleChange( { MerchantRuleUpdates.addNewRule(pattern, category) }, listOf { warnOfOverlappingCategoryRules(pattern) } )
        }
        else {
            if (category == null) {
                RequestResponses.createErrorResponse("Invalid category $categoryString", HttpStatus.BAD_REQUEST)
            }
            else if (!isValidFormat) {
                RequestResponses.createErrorResponse("Candidate pattern $pattern's format is invalid'", HttpStatus.BAD_REQUEST)
            }
            else {
                RequestResponses.createErrorResponse("Candidate pattern $pattern overlaps with $overlappingPatterns", HttpStatus.CONFLICT)
            }
        }
        return response
    }

    @PostMapping("/merchants/rules/ignore/add")
    fun addIgnoreRule(@RequestParam("pattern") rawPattern: String) : ResponseEntity<String> {
        val pattern: String = rawPattern.toLowerCase()
        val isValidFormat: Boolean = MerchantRules.isValidFormat(pattern)
        val overlappingPatterns: List<String> = MerchantRules.findOverlappingRulesPatterns(pattern, MerchantRules.RuleType.CATEGORY)
        val response: ResponseEntity<String> = if (isValidFormat && overlappingPatterns.isEmpty()) {
            handleMerchantRuleChange( { MerchantsToIgnore.addNewRule(pattern) } )
        }
        else {
            if (!isValidFormat) {
                RequestResponses.createErrorResponse("Candidate pattern $pattern's format is invalid'", HttpStatus.BAD_REQUEST)
            }
            else {
                RequestResponses.createErrorResponse("Candidate pattern $pattern overlaps with $overlappingPatterns", HttpStatus.CONFLICT)
            }
        }
        return response
    }

    @PostMapping("/merchants/rules/update/category")
    fun updateRuleCategory(@RequestParam("patterns") rawPatterns: Array<String>,
                                      @RequestParam("category") categoryString: String) : ResponseEntity<String> {
        val patterns: Array<String> = rawPatterns.map { it.toLowerCase() }.toTypedArray()
        val category: Category? = CategoryLookup.getCategoryForName(categoryString)
        val invalidPatterns: List<String> = patterns.filterNot { MerchantRules.isExistingPattern(it, MerchantRules.RuleType.CATEGORY) }
        val response: ResponseEntity<String> = if (category != null && patterns.isNotEmpty() && invalidPatterns.isEmpty()) {
            handleMerchantRuleChange( { MerchantRuleUpdates.updateRuleCategory(patterns, category) } )
        }
        else {
            val message: String = when {
                category == null -> "Invalid category $categoryString"
                patterns.isEmpty() -> "No patterns specified"
                else -> "Pattern(s) $invalidPatterns does not exist"
            }
            return RequestResponses.createErrorResponse(message, HttpStatus.BAD_REQUEST)
        }
        return response
    }

    @PostMapping("/merchants/rules/update/pattern")
    fun updateCategoryRulePattern(@RequestParam("oldPattern") rawOldPattern: String,
                                    @RequestParam("replacementPattern") rawReplacementPattern: String) : ResponseEntity<String> {
        return updateRulePattern(rawOldPattern, rawReplacementPattern, MerchantRules.RuleType.CATEGORY)
    }

    @PostMapping("/merchants/rules/ignore/update")
    fun updateIgnorePattern(@RequestParam("oldPattern") rawOldPattern: String,
                                      @RequestParam("replacementPattern") rawReplacementPattern: String) : ResponseEntity<String> {
        return updateRulePattern(rawOldPattern, rawReplacementPattern, MerchantRules.RuleType.IGNORE)
    }

    @PostMapping("/merchants/rules/delete")
    fun deleteCategoryRule(@RequestParam("patterns") rawPatterns: Array<String>) : ResponseEntity<String> {
        return deleteRule(rawPatterns, MerchantRules.RuleType.CATEGORY)
    }

    @PostMapping("/merchants/rules/ignore/delete")
    fun deleteIgnoreRule(@RequestParam("patterns") rawPatterns: Array<String>) : ResponseEntity<String> {
        return deleteRule(rawPatterns, MerchantRules.RuleType.IGNORE)
    }

    @GetMapping("/merchants/rules")
    fun getMerchantRules() : ResponseEntity<String> {
        val response: ResponseEntity<String> = retrieveMerchantRules()
        return response
    }

    companion object {

        fun updateRulePattern(rawOldPattern: String, rawReplacementPattern: String, ruleType: MerchantRules.RuleType) : ResponseEntity<String> {
            val oldPattern: String = rawOldPattern.toLowerCase()
            val replacementPattern: String = rawReplacementPattern.toLowerCase()
            val isReplacementValid: Boolean = MerchantRules.isValidFormat(replacementPattern)
            val doesOldPatternExist: Boolean = MerchantRules.isExistingPattern(oldPattern, ruleType)
            val overlappingPatterns: List<String> = MerchantRules.findOverlappingRulesPatterns(oldPattern, replacementPattern, MerchantRules.RuleType.CATEGORY)
            val updatePatternFunction: () -> Int = when(ruleType) {
                MerchantRules.RuleType.CATEGORY -> { { MerchantRuleUpdates.updateRulePattern(oldPattern, replacementPattern) } }
                MerchantRules.RuleType.IGNORE -> { { MerchantsToIgnore.updateRulePattern(oldPattern, replacementPattern) } }
            }
            val warningChecks: List<() -> List<Any>> = if (ruleType == MerchantRules.RuleType.CATEGORY) listOf { warnOfOverlappingCategoryRules(replacementPattern, oldPattern) } else listOf()
            val response: ResponseEntity<String> = if (doesOldPatternExist && isReplacementValid && overlappingPatterns.isEmpty()) {
                handleMerchantRuleChange( { updatePatternFunction() }, warningChecks )
            }
            else {
                if (!doesOldPatternExist) {
                    RequestResponses.createErrorResponse("Pattern $oldPattern does not exist", HttpStatus.BAD_REQUEST)
                }
                else if (!isReplacementValid) {
                    RequestResponses.createErrorResponse("Candidate replacement $replacementPattern's format is invalid", HttpStatus.BAD_REQUEST)
                }
                else {
                    RequestResponses.createErrorResponse("Candidate pattern $replacementPattern overlaps with $overlappingPatterns", HttpStatus.CONFLICT)
                }
            }
            return response
        }

        fun deleteRule(rawPatterns: Array<String>, ruleType: MerchantRules.RuleType) : ResponseEntity<String> {
            val patterns: Array<String> = rawPatterns.map { it.toLowerCase() }.toTypedArray()
            val invalidPatterns: List<String> = patterns.filterNot { MerchantRules.isExistingPattern(it, ruleType) }
            val deleteFunction: () -> Int = when(ruleType) {
                MerchantRules.RuleType.CATEGORY -> { { MerchantRuleUpdates.deleteRules(patterns) } }
                MerchantRules.RuleType.IGNORE -> { { MerchantsToIgnore.deleteRules(patterns) } }
            }
            val response: ResponseEntity<String> = if (patterns.isNotEmpty() && invalidPatterns.isEmpty()) {
                handleMerchantRuleChange( { deleteFunction() } )
            }
            else if (patterns.isEmpty()){
                RequestResponses.createErrorResponse("No patterns specified", HttpStatus.BAD_REQUEST)
            }
            else {
                RequestResponses.createErrorResponse("Pattern(s) $invalidPatterns does not exist", HttpStatus.BAD_REQUEST)
            }
            return response
        }

        fun retrieveMerchantRules() : ResponseEntity<String> {
            val responseMap: MutableMap<String, Any> = mutableMapOf()
            val response: ResponseEntity<String> = try {
                val merchantRules: Map<String, Int> = MerchantRules.getRulesToCategories().map { it.key to it.value.id }.toMap()
                responseMap[JsonKeys.MERCHANT_RULES.key] = merchantRules
                RequestResponses.createResponseEntity(responseMap)
            }
            catch (e: Exception) {
                RequestResponses.createErrorResponse(responseMap, e)
            }
            return response
        }

        fun warnOfOverlappingCategoryRules(replacementPattern: String, oldPattern: String = "") : List<Any> {
            val overlappingCategoryRules: MerchantRules.OverlappingMerchantResult = MerchantRules.findDoubleMatchingMerchants(replacementPattern, oldPattern)
            val warnings: MutableList<String> = mutableListOf()
            overlappingCategoryRules.regularMerchantsAndRules.forEach { (merchant, rules) -> warnings.add("Merchant $merchant matches more than one rule: $replacementPattern and $rules") }
            overlappingCategoryRules.ignoredMerchantsAndRules.forEach { (merchant, rules) -> warnings.add("Ignored merchant $merchant matches more than one rule: $replacementPattern and $rules") }
            return warnings.toList()
        }

        fun handleMerchantRuleChange(tableUpdate: () -> Int, warningChecks: List<() -> List<Any>> = listOf()) : ResponseEntity<String> {
            val responseMap: MutableMap<String, Any> = mutableMapOf()
            val response: ResponseEntity<String> = try {
                val numAffectedRows: Int = tableUpdate()
                MerchantRules.refreshRulesFromDatabase()
                responseMap[JsonKeys.AFFECTED_ENTRIES.key] = numAffectedRows
                val warnings: MutableList<Any> = mutableListOf()
                warningChecks.forEach { check -> warnings.addAll(check()) }
                if (warnings.isNotEmpty()) {
                    responseMap[JsonKeys.WARNINGS.key] = warnings
                }
                RequestResponses.createResponseEntity(responseMap)
            }
            catch (e: Exception) {
                RequestResponses.createErrorResponse(responseMap, e)
            }
            return response
        }
    }
}

