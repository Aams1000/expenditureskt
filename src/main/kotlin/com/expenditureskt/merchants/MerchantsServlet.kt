package com.expenditureskt.merchants

import com.expenditureskt.categories.Category
import com.expenditureskt.categories.db.CategoryLookup
import com.expenditureskt.common.JsonKeys
import com.expenditureskt.common.requests.RequestResponses
import com.expenditureskt.merchants.db.MerchantsByCategory
import com.expenditureskt.merchants.db.MerchantsToIgnore
import com.expenditureskt.merchants.db.MerchantsUpdates
import com.expenditureskt.parsers.db.SpendingUpdates
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import java.sql.SQLException
import kotlin.Exception

@Controller
class MerchantsServlet {

    @GetMapping("/merchants")
    fun testGet() : ResponseEntity<String> {
        return ResponseEntity("Get request working", HttpStatus.OK)
    }

    @PostMapping("/merchants/updateMerchants")
    fun handlePostUpdateMerchants(@RequestParam("merchants") merchants: Array<String>,
                                  @RequestParam("newCategory") newCategory: String) : ResponseEntity<String> {
        val category: Category? = CategoryLookup.getCategoryForName(newCategory)
        val response: ResponseEntity<String> = if (category != null) {
            updateMerchants(merchants.toSet(), category)
        }
        else {
            ResponseEntity("", HttpStatus.BAD_REQUEST)
        }
        return response
    }

    @PostMapping("/merchants/updateExpenditures")
    fun handlePostExpenditures(@RequestParam("expenditureIds") expenditureIds: Array<Int>,
                               @RequestParam("newCategory") newCategory: String) : ResponseEntity<String> {
        val responseMap: MutableMap<String, Any> = mutableMapOf()
        val objectWriter: ObjectWriter = ObjectMapper().registerKotlinModule().writer().withDefaultPrettyPrinter()
            val category: Category? = CategoryLookup.getCategoryForName(newCategory)
            val response: ResponseEntity<String> = if (category != null) {
                updateExpenditures(expenditureIds.toList(), category)
            }
            else {
                responseMap[JsonKeys.ERROR.key] = "Category $newCategory is not valid"
                ResponseEntity(objectWriter.writeValueAsString(responseMap), HttpStatus.BAD_REQUEST)
            }
        return response
    }

    @GetMapping("/merchants/byCategory")
    fun handlePostByCategory() : ResponseEntity<String> {
        val response: ResponseEntity<String> = getMerchantsByCategory()
        return response
    }

    @GetMapping("/merchants/getCategories")
    fun handlePostGetMerchants() : ResponseEntity<String> {
        val response: ResponseEntity<String> = getCategories()
        return response
    }

    @PostMapping("/merchants/ignoreMerchant")
    fun handlePostIgnoreMerchant(@RequestParam("merchants") merchants: Array<String>) : ResponseEntity<String> {
        val response: ResponseEntity<String> = ignoreMerchant(merchants)
        return response
    }

    @PostMapping("/merchants/unignoreMerchants")
    fun handlePostUnignoreMerchants(@RequestParam("merchants") merchants: Array<String>,
                                    @RequestParam("newCategory") categoryName: String) : ResponseEntity<String> {
        val category: Category? = CategoryLookup.getCategoryForName(categoryName)
        val response: ResponseEntity<String> = if (category != null && merchants.isNotEmpty()) {
            unignoreMerchants(merchants, category)
        }
        else if (merchants.isEmpty()) {
            RequestResponses.createErrorResponse("Merchants list is empty", HttpStatus.BAD_REQUEST)
        }
        else {
            RequestResponses.createErrorResponse("Category $categoryName is not a known category", HttpStatus.BAD_REQUEST)
        }
        return response
    }

    companion object {

        fun unignoreMerchants(merchants: Array<String>, category: Category) : ResponseEntity<String> {
            val responseMap: MutableMap<String, Any> = mutableMapOf()
            val response: ResponseEntity<String> = try {
                val numAffectedRows: Int = MerchantsToIgnore.unignoreMerchants(merchants.toSet(), category)
                responseMap[JsonKeys.AFFECTED_ENTRIES.key] = numAffectedRows
                RequestResponses.createResponseEntity(responseMap)
            }
            catch (e: Exception) {
                RequestResponses.createErrorResponse(responseMap, e)
            }
            return response
        }

        fun ignoreMerchant(merchants: Array<String>) : ResponseEntity<String> {
            val responseMap: MutableMap<String, Any> = mutableMapOf()
            val response: ResponseEntity<String> = try {
                val numAffectedRows: Int = MerchantsToIgnore.addMerchantsToIgnore(merchants.toSet())
                responseMap[JsonKeys.AFFECTED_ENTRIES.key] = numAffectedRows
                RequestResponses.createResponseEntity(responseMap)
            }
            catch (e: Exception) {
                RequestResponses.createErrorResponse(responseMap, e)
            }
            return response
        }

        fun getCategories() : ResponseEntity<String> {
            val responseMap: MutableMap<String, Any> = mutableMapOf()
            val response: ResponseEntity<String> = try {
                val categories: List<String> = CategoryLookup.getAllCategories().map { it.name }
                responseMap[JsonKeys.CATEGORIES.key] = categories
               RequestResponses.createResponseEntity(responseMap)
            }
            catch (e: Exception) {
                RequestResponses.createErrorResponse(responseMap, e)
            }
            return response
        }

        fun getMerchantsByCategory() : ResponseEntity<String> {
            val responseMap: MutableMap<String, Any> = mutableMapOf()
            val response: ResponseEntity<String> = try {
                val merchantsByCategory: Map<String, Category> = MerchantsByCategory.queryMerchantsByCategory()
                responseMap[JsonKeys.MERCHANTS_BY_CATEGORY.key] = merchantsByCategory
                RequestResponses.createResponseEntity(responseMap)
            }
            catch (e: Exception) {
                RequestResponses.createErrorResponse(responseMap, e)
            }
            return response
        }

        fun updateExpenditures(ids: List<Int>, category: Category) : ResponseEntity<String> {
            val responseMap: MutableMap<String, Any> = mutableMapOf()
            val response: ResponseEntity<String> = try {
                val numUpdatedRows: Int = SpendingUpdates.updateCategoriesForEntries(ids.map { it.toLong() to category }.toMap())
                responseMap[JsonKeys.AFFECTED_ENTRIES.key] = numUpdatedRows
                RequestResponses.createResponseEntity(responseMap)
            }
            catch (e: SQLException) {
                RequestResponses.createErrorResponse(responseMap, e)
            }
            return response
        }

        fun updateMerchants(merchants: Set<String>, category: Category) : ResponseEntity<String> {
            val responseMap: MutableMap<String, Any> = mutableMapOf()
            val response: ResponseEntity<String> = try {
                val numUpdatedRows: Int = MerchantsUpdates.updateMerchantsAndExpenditures(merchants, category)
                responseMap[JsonKeys.AFFECTED_ENTRIES.key] = numUpdatedRows
                if (numUpdatedRows >= 0) {
                    RequestResponses.createResponseEntity(responseMap)
                }
                else {
                    RequestResponses.createErrorResponse("Update failed but connection rolled back. Please see logs", HttpStatus.INTERNAL_SERVER_ERROR)
                }
            }
            catch (e: Exception) {
                RequestResponses.createErrorResponse(responseMap, e)
            }
            return response
        }
    }
}