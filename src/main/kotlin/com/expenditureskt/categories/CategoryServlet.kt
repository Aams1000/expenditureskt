package com.expenditureskt.categories

import com.expenditureskt.categories.db.CategoryLookup
import com.expenditureskt.categories.db.CategoryUpdates
import com.expenditureskt.common.JsonKeys
import com.expenditureskt.common.requests.RequestResponses
import com.expenditureskt.merchants.MerchantRules
import com.expenditureskt.merchants.db.MerchantsByCategory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
class CategoryServlet {

    @PostMapping("/categories/add")
    fun addCategoryForPostRequest(@RequestParam("category") category: String) : ResponseEntity<String> {
        val response: ResponseEntity<String> = if (!CategoryLookup.isExistingCategory(category)) {
            addCategory(category)
        }
        else {
            RequestResponses.createErrorResponse("Category $category already exists", HttpStatus.BAD_REQUEST)
        }
        return response
    }

    @PostMapping("categories/delete")
    fun deleteCategoriesForPostRequest(@RequestParam("categories") categoryNames: Array<String>) : ResponseEntity<String> {
        val areExistingAndCustomCategories: Boolean = categoryNames.all { CategoryLookup.isExistingAndCustomCategory(it) }
        val response: ResponseEntity<String> = if (areExistingAndCustomCategories) {
            val categoryIds: List<Int> = categoryNames.map { CategoryLookup.getCategoryForName(it)!!.id }
            deleteCategories(categoryIds)
        }
        else {
            val nonExistentCategories: List<String> = categoryNames.filterNot { CategoryLookup.isExistingCategory(it) }
            val nonCustomCategories: List<String> = categoryNames.filter { CategoryLookup.isExistingCategory(it) }
                                                                 .filterNot { CategoryLookup.isCustomCategory(CategoryLookup.getCategoryForName(it)!!.id) }
            RequestResponses.createErrorResponse("Request contains non-existent categories ($nonExistentCategories) or non-deletable categories ($nonCustomCategories)", HttpStatus.BAD_REQUEST)
        }
        return response
    }

    @PostMapping("categories/reassign")
    fun reassignAndDeleteCategoriesForPostRequest(@RequestParam("categories") deletionCategoryNames: Array<String>,
                                                  @RequestParam("replacementCategory") replacementCategoryName: String) : ResponseEntity<String>  {
        val areDeltionCategoriesValid: Boolean = deletionCategoryNames.all { CategoryLookup.isExistingAndCustomCategory(it) }
        val replacementCategory: Category? = CategoryLookup.getCategoryForName(replacementCategoryName)
        val response: ResponseEntity<String> = if (areDeltionCategoriesValid
                                                    && replacementCategory != null
                                                    && replacementCategoryName !in deletionCategoryNames) {
            val deletionIds: List<Int> = deletionCategoryNames.map { CategoryLookup.getCategoryForName(it)!!.id }
            replaceAndDeleteCategory(deletionIds, replacementCategory.id)
        }
        else {
            val invalidDeletionCategories: List<String> = deletionCategoryNames.filterNot { CategoryLookup.isExistingCategory(it) }
            val nonDeletableCategories: List<String> = deletionCategoryNames.filter { CategoryLookup.isExistingCategory(it) && !CategoryLookup.isExistingAndCustomCategory(it) }
            val message: String = when {
                replacementCategory == null -> "Replacement category $replacementCategoryName does not exist"
                invalidDeletionCategories.isNotEmpty() -> "Categories $invalidDeletionCategories do not exist"
                nonDeletableCategories.isNotEmpty() -> "Categories $nonDeletableCategories cannot be deleted or reassigned"
                else -> "Replacement category $replacementCategoryName cannot be included in those to reassign/delete"
                }
            ResponseEntity(message, HttpStatus.BAD_REQUEST)
            }
        return response
    }

    companion object {

        private fun replaceAndDeleteCategory(deletionIds: List<Int>, replacementId: Int): ResponseEntity<String> {
            val responseMap: MutableMap<String, Any> = mutableMapOf()
            val response: ResponseEntity<String> = try {
                val numAffectedRows: Int = CategoryUpdates.replaceAndDeleteCategory(deletionIds, replacementId)
                responseMap[JsonKeys.AFFECTED_ENTRIES.key] = numAffectedRows
                RequestResponses.createResponseEntity(responseMap)
            }
            catch (e: Exception) {
                RequestResponses.createErrorResponse(responseMap, e)
            }
            CategoryLookup.refreshFromDatabase()
            MerchantRules.refreshRulesFromDatabase()
            return response
        }

        private fun addCategory(category: String): ResponseEntity<String> {
            val responseMap: MutableMap<String, Any> = mutableMapOf()
            val response: ResponseEntity<String> = try {
                val numAffectedRows: Int = CategoryUpdates.insertCategory(category)
                responseMap[JsonKeys.AFFECTED_ENTRIES.key] = numAffectedRows
                RequestResponses.createResponseEntity(responseMap)
            }
            catch (e: Exception) {
                RequestResponses.createErrorResponse(responseMap, e)
            }
            CategoryLookup.refreshFromDatabase()
            return response
        }

        private fun deleteCategories(categoryIds: List<Int>): ResponseEntity<String> {
            val responseMap: MutableMap<String, Any> = mutableMapOf()
            val response: ResponseEntity<String> = try {
                val numAffectedRows: Int = CategoryUpdates.deleteCategories(categoryIds)
                responseMap[JsonKeys.AFFECTED_ENTRIES.key] = numAffectedRows
                RequestResponses.createResponseEntity(responseMap)
            }
            catch (e: Exception) {
                RequestResponses.createErrorResponse(responseMap, e)
            }
            CategoryLookup.refreshFromDatabase()
            MerchantRules.refreshRulesFromDatabase()
            return response
        }
    }
}

