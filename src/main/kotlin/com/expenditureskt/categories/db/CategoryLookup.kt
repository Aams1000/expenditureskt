package com.expenditureskt.categories.db

import com.expenditureskt.categories.Category
import com.expenditureskt.common.db.DatabaseConnections
import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam
import java.security.InvalidKeyException
import java.sql.ResultSet
import java.sql.SQLException

object CategoryLookup {

    val UNKNOWN_CATEGORY_NAME: String = "Unknown"

    private var m_idsToName: Map<Int, String> = mutableMapOf()
    private var m_namesToId: Map<String, Int> = mutableMapOf()
    private var m_customCategories: Set<Int> = mutableSetOf()

    init {
        refreshFromDatabase()
    }

    fun refreshFromDatabase() : Unit {
        val queryInfo: QueryInfo = CategoryQuery.QUERY_IDS_AND_NAMES
        val idToNameMap: MutableMap<Int, String> = mutableMapOf()
        val nameToIdMap: MutableMap<String, Int> = mutableMapOf()
        val customCategorySet: MutableSet<Int> = mutableSetOf()
        DatabaseConnections.getExpendituresConnection().use {connection ->
            try {
                connection.prepareStatement(queryInfo.query).use {statement ->
                    val resultSet: ResultSet = statement.executeQuery()
                    while (resultSet.next()) {
                        val id: Int = resultSet.getInt(queryInfo.getResultIndex(QueryParam.EXPENDITURE_ID))
                        val name: String = resultSet.getString(queryInfo.getResultIndex(QueryParam.CATEGORY_NAME))
                        val isCustom: Boolean = resultSet.getBoolean(queryInfo.getResultIndex(QueryParam.IS_CUSTOM))
                        idToNameMap[id] = name
                        nameToIdMap[name] = id
                        if (isCustom) {
                            customCategorySet.add(id)
                        }
                    }
                }
            }
            catch (e: SQLException) {
                println("Failed to update CategoryLookup due to SQLException: $e")
                throw e
            }
            catch (e: InvalidKeyException) {
                println("Failed to update CategoryLookup due to InvalidKeyException: $e")
                throw e
            }
        }
        m_idsToName = idToNameMap
        m_namesToId = nameToIdMap
        m_customCategories = customCategorySet
    }

    fun getCategoryForId(id: Int) : Category? {
        val name: String? = m_idsToName[id]
        val category: Category? = if (name != null) Category(id, name) else null
        return category
    }

    fun getCategoryForName(name: String) : Category? {
        val id: Int? = m_namesToId[name]
        val category: Category? = if (id != null) Category(id, name) else null
        return category
    }

    fun getUnknownCategory() : Category {
        val unknownCategory: Category? = getCategoryForName(UNKNOWN_CATEGORY_NAME)
        return unknownCategory ?: throw RuntimeException("Unknown category is not present. Something is wrong")
    }

    fun isExistingCategory(id: Int) : Boolean {
        return id in m_idsToName
    }

    fun isExistingCategory(name: String) : Boolean {
        return name in m_namesToId
    }

    fun isExistingAndCustomCategory(name: String) : Boolean {
        return isExistingCategory(name) && isCustomCategory(m_namesToId.getValue(name))
    }

    fun isCustomCategory(id: Int) : Boolean {
        return id in m_customCategories
    }

    fun getAllCategories() : List<Category> {
        return m_idsToName.keys.map { id -> getCategoryForId(id)!! }.toList()
    }
}
