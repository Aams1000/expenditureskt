package com.expenditureskt.categories.db

import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam

enum class CategoryQuery(override val query: String,
                         override val queryIndicesByParam: Map<QueryParam, List<Int>>,
                         override val resultIndexByParam: Map<QueryParam, Int>) : QueryInfo {

    QUERY_IDS_AND_NAMES("select id, name, is_custom from t_categories",
            mapOf(),
            mapOf(QueryParam.EXPENDITURE_ID to 1,
                    QueryParam.CATEGORY_NAME to 2,
                    QueryParam.IS_CUSTOM to 3)),

    REPLACE_CATEGORIES_WITH_ANOTHER("update t_merchants_by_category set category = ? where category = any(?::integer[])",
            mapOf(QueryParam.REPLACEMENT_ID to listOf(1),
                  QueryParam.DELETION_IDS to listOf(2)),
            mapOf()
    ),

    INSERT_NEW_CATEGORY("insert into t_categories values (default, ?, TRUE)",

            mapOf(QueryParam.CATEGORY_NAME to listOf(1)),
            mapOf()
    ),

    DELETE_CUSTOM_CATEGORIES("delete from t_categories where id = any(?::integer[]) and is_custom = TRUE",

            mapOf(QueryParam.DELETION_IDS to listOf(1)),
            mapOf()
    );

}

