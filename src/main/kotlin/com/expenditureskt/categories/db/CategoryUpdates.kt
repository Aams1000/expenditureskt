package com.expenditureskt.categories.db

import com.expenditureskt.common.db.DatabaseConnections
import com.expenditureskt.common.db.PreparedStatements
import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam
import com.expenditureskt.homepage.db.ExpendituresDAO
import com.expenditureskt.homepage.db.ExpendituresQuery
import com.expenditureskt.merchants.db.MerchantRuleUpdates
import java.sql.Connection

object CategoryUpdates {

    fun replaceAndDeleteCategory(deletionIds: List<Int>, replacementId: Int) : Int {
        var numAffectedRows: Int = -1
        try {
            DatabaseConnections.getNonAutoCommitExpendituresConnection().use { connection ->
                numAffectedRows = replaceMerchantCategories(connection, deletionIds, replacementId)
                // for those expenditures whose merchant is not assigned to the deleting category (e.g. is unknown)
                numAffectedRows += ExpendituresDAO.replaceExpenditureCategories(connection, deletionIds, replacementId)
                MerchantRuleUpdates.updateRuleCategories(connection, deletionIds, replacementId)
                deleteCategoryWithExistingConnection(connection, deletionIds)
                try {
                    connection.commit()
                }
                catch (e1: Exception) {
                    println("Failed to commit transaction, rolling back $e1")
                    try {
                        connection.rollback()
                    }
                    catch (e2: Exception) {
                        println("Failed to rollback transaction. Something is very wrong")
                        throw e2
                    }
                }
            }
        }
        catch (e: Exception) {
            println("Failed to replace and delete category $deletionIds with $replacementId due to Exception $e")
            throw e
        }
        return numAffectedRows
    }

    private fun replaceMerchantCategories(connection: Connection, deletionIds: List<Int>, replacementId: Int) : Int {
        val queryInfo: QueryInfo = CategoryQuery.REPLACE_CATEGORIES_WITH_ANOTHER
        connection.prepareStatement(queryInfo.query).use { statement ->
            PreparedStatements.setArrays(statement, queryInfo.getQueryIndices(QueryParam.DELETION_IDS), DatabaseConnections.convertIntListToSqlArray(connection, deletionIds))
            PreparedStatements.setInts(statement, queryInfo.getQueryIndices(QueryParam.REPLACEMENT_ID), replacementId)
            statement.executeUpdate()
            return statement.updateCount
        }
    }

    fun insertCategory(category: String) : Int {
        val queryInfo: QueryInfo = CategoryQuery.INSERT_NEW_CATEGORY
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                connection.prepareStatement(queryInfo.query).use { statement ->
                    PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.CATEGORY_NAME), category)
                    statement.executeUpdate()
                    return statement.updateCount
                }
            }
        }
        catch (e: Exception) {
            println("Failed to insert new category $category due to Exception $e")
            throw e
        }
    }

    fun deleteCategories(categoryIds: List<Int>) : Int {
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                val updateCount: Int = deleteCategoryWithExistingConnection(connection, categoryIds)
                return updateCount
            }
        }
        catch (e: Exception) {
            println("Failed to delete categories $categoryIds due to Exception $e")
            throw e
        }
    }

    private fun deleteCategoryWithExistingConnection(connection: Connection, categoryIds: List<Int>) : Int {
        val queryInfo: QueryInfo = CategoryQuery.DELETE_CUSTOM_CATEGORIES
        connection.prepareStatement(queryInfo.query).use { statement ->
            PreparedStatements.setArrays(statement, queryInfo.getQueryIndices(QueryParam.DELETION_IDS), DatabaseConnections.convertIntListToSqlArray(connection, categoryIds))
            println(statement)
            statement.executeUpdate()
            return statement.updateCount
        }
    }
}

