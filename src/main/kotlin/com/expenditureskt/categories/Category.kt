package com.expenditureskt.categories

import com.fasterxml.jackson.annotation.JsonProperty

data class Category(@JsonProperty("id") val id: Int, @JsonProperty("name") val name: String) {

//    override fun equals(other: Any?): Boolean {
//        return other != null
//                && other::javaClass == this::javaClass
//                && (other as Category).id == this.id
//                && other.name == this.name
//    }
//
//    override fun hashCode(): Int {
//        var result = id
//        result = 31 * result + name.hashCode()
//        return result
//    }
}
