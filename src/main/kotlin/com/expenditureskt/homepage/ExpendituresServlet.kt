package com.expenditureskt.homepage

import com.expenditureskt.categories.Category
import com.expenditureskt.models.ReactInterface
import com.expenditureskt.categories.db.CategoryLookup
import com.expenditureskt.common.JsonKeys
import com.expenditureskt.homepage.db.ExpendituresDAO
import com.expenditureskt.models.CategorySummary
import com.expenditureskt.models.PercentChangesSummary
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.time.LocalDate
import java.time.temporal.ChronoUnit

@Controller
class ExpendituresServlet {

    private val ALL_CATEGORIES: List<Category> = CategoryLookup.getAllCategories()
    private val RENT_AND_INVESTMENT_CATEGORY_IDS: Set<Int> = setOf(28, 16, 21)
    private val INVESTMENT_CATEGORY_ID: Set<Int> = setOf(16)


    @GetMapping("/")
    @ResponseBody
    fun testGet(): ResponseEntity<String> {
        return ResponseEntity(ReactInterface.getBaseTemplate(ReactInterface.PageType.EXPENDITURES_GET), HttpStatus.OK)
    }

    @PostMapping("/")
    @ResponseBody
    fun testPost(@RequestParam("startDate") startDateString: String,
                 @RequestParam("endDate") endDateString: String): ResponseEntity<String> {
        val startDate: LocalDate? = LocalDate.parse(startDateString)
        val endDate: LocalDate? = LocalDate.parse(endDateString)
        val responseEntity: ResponseEntity<String> = if (startDate != null && endDate != null && startDate.isBefore(endDate)) {
//            println("Dates parsed")
            val categoriesMinusRentAndInvestments: List<Category> = ALL_CATEGORIES
//            val categoriesMinusRentAndInvestments: List<Category> = ALL_CATEGORIES.filter { category -> category.id !in RENT_AND_INVESTMENT_CATEGORY_IDS}
//            val categoriesMinusRentAndInvestments: List<Category> = ALL_CATEGORIES.filter { category -> category.id !in INVESTMENT_CATEGORY_ID }
//            val summaries: Map<String, Any> = generateSummaries(startDate, endDate, ALL_CATEGORIES)
            val summaries: Map<String, Any> = generateSummaries(startDate, endDate, categoriesMinusRentAndInvestments)
            val objectMapper: ObjectMapper = ObjectMapper().registerKotlinModule()
            ResponseEntity(objectMapper.writeValueAsString(summaries), HttpStatus.OK)
        }
        else {
            ResponseEntity(HttpStatus.BAD_REQUEST)
        }
        return responseEntity
    }

    @PostMapping("/expenditures/delete")
    @ResponseBody
    fun handleExpendituresDeletion(@RequestParam("expenditureIds") expenditureIds: List<Int>): ResponseEntity<String> {
        val responseEntity: ResponseEntity<String> = if (expenditureIds.isNotEmpty()) {
            val deletionCount: Int = ExpendituresDAO.deleteExpendituresById(expenditureIds)
            val objectMapper: ObjectMapper = ObjectMapper().registerKotlinModule()
            ResponseEntity(objectMapper.writeValueAsString(mapOf(JsonKeys.AFFECTED_ENTRIES to deletionCount)), HttpStatus.OK)
        }
        else {
            ResponseEntity(HttpStatus.BAD_REQUEST)
        }
        return responseEntity
    }

    companion object {

        private fun generateSummaries(startDate: LocalDate, endDate: LocalDate, categories: List<Category>) : Map<String, Any> {
            val resultMap: MutableMap<String, Any> = mutableMapOf()
            // going to 500 on any failure for the moment
            val categorySummary: CategorySummary = ExpendituresDAO.retrieveSpendingSummary(categories,
                                                                                            startDate,
                                                                                            endDate)
            resultMap[JsonKeys.CATEGORY_SUMMARIES.key] = categorySummary
            if (categorySummary.totalSpending > 0) {
                val percentChangesSummary: PercentChangesSummary? = retrieveAndComputePercentChanges(
                    startDate,
                    endDate,
                    categories,
                    categorySummary
                )
                if (percentChangesSummary != null) {
                    resultMap[JsonKeys.PERCENT_CHANGES.key] = percentChangesSummary
                }
            }
            val monthlySummariesAndCategoryAverages: ExpendituresDAO.MonthlyAndCategorySummaries = ExpendituresDAO.retrieveMonthlySummariesAndCategoryAverages(categories,
                                                                                                                                                                ExpendituresDAO.MonthRepresentation.DISTINCT,
                                                                                                                                                                startDate,
                                                                                                                                                                endDate)
            resultMap[JsonKeys.MONTH_SUMMARIES.key] = monthlySummariesAndCategoryAverages.monthlySummaries
            resultMap[JsonKeys.MONTHLY_CATEGORY_AVERAGES.key] = monthlySummariesAndCategoryAverages.monthlyCategoryAverages
            resultMap[JsonKeys.CATEGORY_SPENDING_OVER_TIME.key] = monthlySummariesAndCategoryAverages.categorySpendingOverTime
            return resultMap
        }

        private fun retrieveAndComputePercentChanges(startDate: LocalDate,
                                                     endDate: LocalDate,
                                                     categories: List<Category>,
                                                     newSummary: CategorySummary) : PercentChangesSummary? {
            val dateDifference: Long = startDate.until(endDate, ChronoUnit.DAYS)
            val previousStartDate: LocalDate = startDate.minusDays(dateDifference)
            val previousEndDate: LocalDate = endDate.minusDays(dateDifference)
            val previousSummary: CategorySummary = ExpendituresDAO.retrieveSpendingSummary(
                categories,
                previousStartDate,
                previousEndDate
            )
            val percentChangesSummary: PercentChangesSummary?
            if (newSummary.totalSpending > 0.0 && previousSummary.totalSpending > 0.0) {
                val percentChangesByCategory: Map<String, Double> = newSummary.spendingByCategory.filter { it.key in previousSummary.spendingByCategory.keys }
                        .filter { previousSummary.spendingByCategory[it.key]!! > 0.0 }
                        .map { (category, newAmount) -> category to computePercentDifference(newAmount, previousSummary.spendingByCategory[category]!!) }
                        .toMap()
                val overallPercentChange: Double = computePercentDifference(newSummary.totalSpending, previousSummary.totalSpending)
                percentChangesSummary = PercentChangesSummary.fromUnformattedValues(previousSummary.totalSpending,
                                                                                    overallPercentChange,
                                                                                    newSummary.totalSpending - previousSummary.totalSpending,
                                                                                    percentChangesByCategory,
                                                                                    previousStartDate,
                                                                                    previousEndDate)
            }
            else {
                println("Total spending on either selected window ($${newSummary.totalSpending}) or window for computing " +
                        "percent changes ($${previousSummary.totalSpending}) is zero. Not computing percent differences")
                percentChangesSummary = null
            }
            return percentChangesSummary
        }

        private fun computePercentDifference(newAmount: Double, oldAmount: Double) : Double {
            return ((newAmount - oldAmount) / oldAmount) * 100
        }
    }
}