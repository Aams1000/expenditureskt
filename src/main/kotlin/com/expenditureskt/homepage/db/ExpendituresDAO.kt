package com.expenditureskt.homepage.db

import com.expenditureskt.categories.Category
import com.expenditureskt.categories.db.CategoryLookup
import com.expenditureskt.categories.db.CategoryQuery
import com.expenditureskt.common.db.DBExpenditure
import com.expenditureskt.common.db.DatabaseConnections
import com.expenditureskt.common.db.PreparedStatements
import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam
import com.expenditureskt.models.CategorySummary
import com.expenditureskt.models.DisplayFormatter
import com.expenditureskt.models.MonthAndYear
import java.sql.Connection
import java.sql.ResultSet
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Month
import java.time.Year

object ExpendituresDAO {

    enum class MonthRepresentation {
        AVERAGE,
        DISTINCT
    }

    fun retrieveSpendingSummary(categories: List<Category>, startDate: LocalDate, endDate: LocalDate): CategorySummary {
        val queryInfo: QueryInfo = ExpendituresQuery.QUERY_EXPENDITURES_FOR_INTERVAL
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                connection.prepareStatement(queryInfo.query).use { statement ->
                    PreparedStatements.setDates(statement, queryInfo.getQueryIndices(QueryParam.START_DATE), startDate)
                    PreparedStatements.setDates(statement, queryInfo.getQueryIndices(QueryParam.END_DATE), endDate)
                    PreparedStatements.setArrays(statement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), DatabaseConnections.convertExpendituresToSqlArray(connection, categories))
                    println("Executing $statement")
                    val categorySummary: CategorySummary = CategorySummary.fromResultSet(statement.executeQuery(), queryInfo)
                    return categorySummary
                }
            }
        } catch (e: Exception) {
            println("Failed to execute query due to exception: $e")
            throw e
        }
    }

    fun retrieveMonthlySummariesAndCategoryAverages(categories: List<Category>,
                                                    monthRepresentation: MonthRepresentation,
                                                    startDate: LocalDate,
                                                    endDate: LocalDate): MonthlyAndCategorySummaries {
        val queryInfo: QueryInfo = ExpendituresQuery.QUERY_EXPENDITURES_BY_MONTH
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                connection.prepareStatement(queryInfo.query).use { statement ->
                    PreparedStatements.setDates(statement, queryInfo.getQueryIndices(QueryParam.START_DATE), startDate)
                    PreparedStatements.setDates(statement, queryInfo.getQueryIndices(QueryParam.END_DATE), endDate)
                    PreparedStatements.setArrays(statement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), DatabaseConnections.convertExpendituresToSqlArray(connection, categories))
                    val resultSet: ResultSet = statement.executeQuery()
                    val monthlySummariesAndCategoryAverages: MonthlyAndCategorySummaries = createDistinctMonthlySummaries(resultSet, queryInfo)
                    return monthlySummariesAndCategoryAverages
                }
            }
        } catch (e: Exception) {
            println("Failed to execute query due to exception: $e")
            throw e
        }
    }

    private fun createDistinctMonthlySummaries(resultSet: ResultSet, queryInfo: QueryInfo): MonthlyAndCategorySummaries {
        val spendingByMonthAndYear: MutableMap<MonthAndYear, Double> = mutableMapOf()
        val spendingMapByMonth: MutableMap<MonthAndYear, MutableMap<Category, Double>> = mutableMapOf()
        val spendingByCategory: MutableMap<Category, Double> = mutableMapOf()
        val allCategories: MutableSet<Category> = mutableSetOf()
        val allMonthsAndYears: MutableSet<MonthAndYear> = mutableSetOf()
        var totalMonths: Int = 0
        while (resultSet.next()) {
            val month: Month = Month.of(resultSet.getInt(queryInfo.getResultIndex(QueryParam.MONTH)))
            val year: Year = Year.of(resultSet.getInt(queryInfo.getResultIndex(QueryParam.YEAR)))
            val categoryId: Int = resultSet.getInt(queryInfo.getResultIndex(QueryParam.CATEGORY_ID))
            val amount: Double = resultSet.getDouble(queryInfo.getResultIndex(QueryParam.AMOUNT))
            val category: Category? = CategoryLookup.getCategoryForId(categoryId)
            if (category != null) {
                allCategories.add(category)
                val monthAndYear: MonthAndYear = MonthAndYear(month, year)
                allMonthsAndYears.add(monthAndYear)
                if (monthAndYear !in spendingByMonthAndYear) {
                    totalMonths++
                }
                spendingByMonthAndYear[monthAndYear] = spendingByMonthAndYear.getOrDefault(monthAndYear, 0.0) + amount
                val previousAmount: Double = spendingMapByMonth.computeIfAbsent(monthAndYear) { mutableMapOf() }.getOrDefault(category, 0.0)
                spendingMapByMonth[monthAndYear]!![category] = previousAmount + amount
                spendingByCategory[category] = spendingByCategory.getOrDefault(category, 0.0) + amount
            } else {
                println("Unknown category ID $categoryId retrieved from database")
            }
        }
        // edge case for a bad date range--setting this to one as a default to avoid dividing by zero
        if (totalMonths == 0) {
            totalMonths = 1
        }
        val categorySpendingOverTime: Map<String, List<Double>> = createCategorySpendingOverTime(allCategories, allMonthsAndYears, spendingMapByMonth)
        val monthlySummaries: Map<String, CategorySummary> = spendingMapByMonth.toList()
            .sortedWith(compareBy<Pair<MonthAndYear, MutableMap<Category, Double>>> { it.first.year }.thenBy { it.first.month })
            .associate { (monthAndYear, spendingMap) ->
                DisplayFormatter.formatMonthAndYear(monthAndYear) to CategorySummary.fromSpendingAndCategoryMap(
                    spendingByMonthAndYear[monthAndYear]!!,
                    spendingMap
                )
            }
        // FIXME - this rounding and sorting belongs in a separate layer, not here
        val categoryAveragesByMonth: Map<String, Double> =
            spendingByCategory.map { (category, amount) -> category to DisplayFormatter.roundDollarAmount(amount / totalMonths) }
                              .associate { (category, averageAmount) -> category.name to averageAmount }
                              // sorting by category for display
                              .toSortedMap()
        return MonthlyAndCategorySummaries(monthlySummaries, categoryAveragesByMonth, categorySpendingOverTime)
    }

    private fun createCategorySpendingOverTime(allCategories: Set<Category>, allMonthsAndYears: Set<MonthAndYear>, spendingMapByMonth: Map<MonthAndYear, Map<Category, Double>>): Map<String, List<Double>> {
        val orderedMonthsAndYears: List<MonthAndYear> = allMonthsAndYears.toList().sortedWith(compareBy<MonthAndYear> { it.year }.thenBy { it.month })
        val categorySpendingOverTime: MutableMap<Category, MutableList<Double>> = mutableMapOf()
        for (monthAndYear: MonthAndYear in orderedMonthsAndYears) {
            for (category: Category in allCategories) {
                val amount: Double = spendingMapByMonth[monthAndYear]!!.getOrDefault(category, 0.0)
                categorySpendingOverTime.computeIfAbsent(category) { mutableListOf() }.add(amount)
            }
        }
        // Can I call toSortedMap without toMap first?
        return categorySpendingOverTime.map { (category, monthlyTotals) -> category.name to monthlyTotals.toList() }.toMap().toSortedMap()
    }

    private fun createAverageMonthlySummaries(resultSet: ResultSet, queryInfo: QueryInfo): Map<String, CategorySummary> {
        val spendingByMonth: MutableMap<Month, Double> = mutableMapOf()
        val spendingMapByMonth: MutableMap<Month, MutableMap<Category, Double>> = mutableMapOf()
        val monthCounts: MutableMap<Month, Int> = mutableMapOf()
        while (resultSet.next()) {
            val month: Month = Month.of(resultSet.getInt(queryInfo.getResultIndex(QueryParam.MONTH)))
            val categoryId: Int = resultSet.getInt(queryInfo.getResultIndex(QueryParam.CATEGORY_ID))
            val amount: Double = resultSet.getDouble(queryInfo.getResultIndex(QueryParam.AMOUNT))
            val category: Category? = CategoryLookup.getCategoryForId(categoryId)
            if (category != null) {
                spendingByMonth[month] = spendingByMonth.getOrDefault(month, 0.0) + amount
                val previousAmount: Double = spendingMapByMonth[month]?.getOrDefault(category, 0.0) ?: 0.0
                spendingMapByMonth.computeIfAbsent(month) { mutableMapOf() }[category] = previousAmount + amount
                monthCounts[month] = monthCounts.getOrDefault(month, 0) + 1
            } else {
                println("Unknown category ID $categoryId retrieved from database")
            }
        }
        // divide by the month counts in case the range is longer than one year
        for ((month, spendingMap) in spendingMapByMonth) {
            val monthCount: Double = monthCounts[month]!!.toDouble()
            if (monthCount > 1) {
                val currentAmount: Double = spendingByMonth[month]!!
                spendingByMonth[month] = currentAmount / monthCount
                for ((category, amount) in spendingMap) {
                    spendingMapByMonth[month]!![category] = amount / monthCount
                }
            }
        }
        val monthlySummaries: Map<String, CategorySummary> = spendingMapByMonth.map { (month, spendingMap) ->
            DisplayFormatter.formatMonth(month) to CategorySummary.fromSpendingAndCategoryMap(spendingByMonth[month]!!, spendingMap)
        }.toMap()
        return monthlySummaries
    }

    fun retrieveAllExpenditures(): List<DBExpenditure> {
        val queryInfo: QueryInfo = ExpendituresQuery.RETRIEVE_EXPENDITURES_ORDERED_BY_INSERTION
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                connection.prepareStatement(queryInfo.query).use { statement ->
                    val resultSet: ResultSet = statement.executeQuery()
                    return extractEntriesFromResultSet(queryInfo, resultSet)
                }
            }
        }
        catch (e: Exception) {
            println("Failed to retrieve all expenditures due to Exception: $e")
            throw e
        }
    }

    fun extractEntriesFromResultSet(queryInfo: QueryInfo, resultSet: ResultSet) : List<DBExpenditure> {
        val entries: MutableList<DBExpenditure> = mutableListOf()
        while (resultSet.next()) {
            val id: Long = resultSet.getLong(queryInfo.getResultIndex(QueryParam.EXPENDITURE_ID))
            val merchant: String = resultSet.getString(queryInfo.getResultIndex(QueryParam.MERCHANT))
            val amount: Double = resultSet.getDouble(queryInfo.getResultIndex(QueryParam.AMOUNT))
            val transactionDate: LocalDate = resultSet.getDate(queryInfo.getResultIndex(QueryParam.DATE_OF_TRANSACTION)).toLocalDate()
            val categoryId: Int = resultSet.getInt(queryInfo.getResultIndex(QueryParam.CATEGORY_ID))
            val insertionDate: LocalDateTime = resultSet.getObject(queryInfo.getResultIndex(QueryParam.INSERTION_DATE), LocalDateTime::class.java)
            entries.add(DBExpenditure(id, transactionDate, merchant, amount, categoryId, insertionDate))
        }
        return entries.toList()
    }

    fun replaceExpenditureCategories(connection: Connection, deletionIds: List<Int>, replacementId: Int) : Int {
        val queryInfo: QueryInfo = ExpendituresQuery.UPDATE_EXPENDITURE_CATEGORIES
        connection.prepareStatement(queryInfo.query).use { statement ->
            PreparedStatements.setArrays(statement, queryInfo.getQueryIndices(QueryParam.DELETION_IDS), DatabaseConnections.convertIntListToSqlArray(connection, deletionIds))
            PreparedStatements.setInts(statement, queryInfo.getQueryIndices(QueryParam.REPLACEMENT_ID), replacementId)
            statement.executeUpdate()
            return statement.updateCount
        }
    }

    fun deleteExpendituresById(deletionIds: List<Int>) : Int {
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                val queryInfo: QueryInfo = ExpendituresQuery.DELETE_EXPENDITURES
                connection.prepareStatement(queryInfo.query).use { statement ->
                    PreparedStatements.setArrays(
                        statement,
                        queryInfo.getQueryIndices(QueryParam.DELETION_IDS),
                        DatabaseConnections.convertIntListToSqlArray(connection, deletionIds)
                    )
                    println("Executing $statement")
                    statement.executeUpdate()
                    return statement.updateCount
                }
            }
        } catch (e: Exception) {
            println("Failed to execute query due to exception: $e")
            throw e
        }
    }

    data class MonthlyAndCategorySummaries(val monthlySummaries: Map<String, CategorySummary>,
                                           val monthlyCategoryAverages: Map<String, Double>,
                                           val categorySpendingOverTime: Map<String, List<Double>>)
}
