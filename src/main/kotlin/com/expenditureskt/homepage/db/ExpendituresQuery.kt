package com.expenditureskt.homepage.db

import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam

enum class ExpendituresQuery(override val query: String,
                             override val queryIndicesByParam: Map<QueryParam, List<Int>>,
                             override val resultIndexByParam: Map<QueryParam, Int>) : QueryInfo {

    QUERY_EXPENDITURES_FOR_INTERVAL("select category, sum(amount) " +
            "from t_expenditures e " +
            "where e.transaction_date >= ? " +
            "and e.transaction_date <= ? " +
            "and e.category = any(?::integer[]) " +
            "and not exists (select 1 from t_merchants_to_ignore m where m.merchant = e.merchant) " +
            "group by category",

            mapOf(QueryParam.START_DATE to listOf(1),
                    QueryParam.END_DATE to listOf(2),
                    QueryParam.CATEGORY_ID to listOf(3)),
            mapOf(QueryParam.CATEGORY_ID to 1,
                    QueryParam.AMOUNT to 2)
    ),

    QUERY_EXPENDITURES_BY_MONTH("select extract(month from transaction_date) as month, extract(year from transaction_date) as year, category, sum(amount) " +
            "from t_expenditures e " +
            "where e.transaction_date >= ? " +
            "and e.transaction_date <= ? " +
            "and e.category = any(?::integer[]) " +
            "and not exists (select 1 from t_merchants_to_ignore m where m.merchant = e.merchant) " +
            "group by month, year, category",

            mapOf(QueryParam.START_DATE to listOf(1),
                    QueryParam.END_DATE to listOf(2),
                    QueryParam.CATEGORY_ID to listOf(3)),
            mapOf(QueryParam.MONTH to 1,
                    QueryParam.YEAR to 2,
                    QueryParam.CATEGORY_ID to 3,
                    QueryParam.AMOUNT to 4)
    ),

    QUERY_EXPENDITURES_BY_ID("select id, transaction_date, merchant, category, amount, insertion_date " +
            "from t_expenditures " +
            "where id = any(?::integer[])",

            mapOf(QueryParam.EXPENDITURE_ID to listOf(1)),
            mapOf(QueryParam.EXPENDITURE_ID to 1,
                    QueryParam.DATE_OF_TRANSACTION to 2,
                    QueryParam.MERCHANT to 3,
                    QueryParam.CATEGORY_ID to 4,
                    QueryParam.AMOUNT to 5,
                    QueryParam.INSERTION_DATE to 6)
    ),

    RETRIEVE_EXPENDITURES_ORDERED_BY_INSERTION("select id, transaction_date, merchant, category, amount, insertion_date " +
            "from t_expenditures e " +
            "where not exists (select 1 from t_merchants_to_ignore m where m.merchant = e.merchant) " +
            "order by insertion_date desc",

            mapOf(),
            mapOf(QueryParam.EXPENDITURE_ID to 1,
                    QueryParam.DATE_OF_TRANSACTION to 2,
                    QueryParam.MERCHANT to 3,
                    QueryParam.CATEGORY_ID to 4,
                    QueryParam.AMOUNT to 5,
                    QueryParam.INSERTION_DATE to 6)
    ),

    DELETE_EXPENDITURES("delete from t_expenditures where id = any(?::integer[])",

            mapOf(QueryParam.DELETION_IDS to listOf(1)),
            mapOf()
    ),

    UPDATE_EXPENDITURE_CATEGORIES("update t_expenditures set category = ? where category = any(?::integer[])",
        mapOf(QueryParam.REPLACEMENT_ID to listOf(1),
              QueryParam.DELETION_IDS to listOf(2)),
        mapOf()
    );
}
