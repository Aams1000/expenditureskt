package com.expenditureskt.parsers

data class CSVIndices(val date: Int, val merchant: Int, val amount: Int)

