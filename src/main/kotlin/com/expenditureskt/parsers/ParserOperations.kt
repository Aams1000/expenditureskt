package com.expenditureskt.parsers

import com.expenditureskt.categories.Category
import com.expenditureskt.merchants.MerchantRules
import org.apache.commons.csv.CSVRecord
import java.time.DateTimeException
import java.time.LocalDate
import java.time.format.DateTimeFormatter

object ParserOperations {

    // Right now this is a bit of a dumping ground for parser-related logic. Should be refactored if it continues to grow
    private val DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

    fun parseEntries(entries: List<CSVRecord>,
                     csvIndices: CSVIndices,
                     datePattern: Regex,
                     datetimeFormatter: DateTimeFormatter,
                     merchantsByCategory: Map<String, Category>,
                     merchantsToIgnore: Set<String>,
                     shouldNegateAmount: Boolean = false) : List<ParsedExpenditure> {
        val expenditures: MutableList<ParsedExpenditure> = mutableListOf()
        for (entry: CSVRecord in entries) {
            parseEntry(entry,
                        csvIndices,
                        datePattern,
                        datetimeFormatter,
                        merchantsByCategory,
                        merchantsToIgnore,
                        shouldNegateAmount
                    )?.let { expenditures.add(it) }
        }
        return expenditures.toList()
    }

    private fun parseEntry(entry: CSVRecord,
                   csvIndices: CSVIndices,
                   datePattern: Regex,
                   datetimeFormatter: DateTimeFormatter,
                   merchantsByCategory: Map<String, Category>,
                   merchantsToIgnore: Set<String>,
                   shouldNegateAmount: Boolean) : ParsedExpenditure? {
        var parsedExpenditure: ParsedExpenditure? = null
        try {
            // chase amounts show up as negative--flipping
            val amount: Double? = parseAmount(entry.get(csvIndices.amount), shouldNegate=shouldNegateAmount)
            val merchant: String = entry.get(csvIndices.merchant).trim()
            val dateString: String = datePattern.find(entry.get(csvIndices.date))?.value ?: ""
            val date: LocalDate = parseDateFromValue(dateString, datetimeFormatter)
            val isNewMerchant: Boolean = merchant !in merchantsByCategory
            val categoryId: Int = CategoryParser.getCategoryForMerchant(merchant, merchantsByCategory).id
            if (amount != null && areValidAmountAndMerchant(amount, merchant, merchantsToIgnore)) {
                parsedExpenditure = ParsedExpenditure(date, merchant, amount, categoryId, isNewMerchant)
            }
            else {
                val reason: String = findReasonForInvalidRow(amount, merchant, merchantsToIgnore)
                println("Skipping entry due to $reason : $entry")
            }
        }
        catch (e: Exception) {
            println("Failed to parse line $entry from due to exception: $e")
        }
        return parsedExpenditure
    }


    fun parseDateFromValue(dateString: String, formatter: DateTimeFormatter = DATE_TIME_FORMATTER): LocalDate {
        try {
            return LocalDate.parse(dateString, formatter)
        }
        catch (e: DateTimeException) {
            println("Failed to parse date $dateString")
            throw e
        }
    }

    // NOTE - this will not work for denominations that do not use the US format for decimals and commas
    fun parseAmount(amountString: String, shouldNegate: Boolean = false) : Double? {
        val amountSansDollarSign: String = if (amountString.startsWith('$')) amountString.substring(1) else amountString
        val parsedAmount: Double? = amountSansDollarSign.replace(",", "").toDoubleOrNull()
        val finalAmount: Double? = if (parsedAmount != null && shouldNegate) -parsedAmount else parsedAmount
        return finalAmount
    }

    fun areValidAmountAndMerchant(amount: Double?, merchant: String, merchantsToIgnore: Set<String>) : Boolean {
        return amount != null
                && amount > 0
                && merchant.isNotBlank()
                && !merchantsToIgnore.contains(merchant)
                && !MerchantRules.shouldIgnoreMerchant(merchant)
    }

    fun findReasonForInvalidRow(amount: Double?, merchant: String, merchantsToIgnore: Set<String>) : String {
        val reason: String = when {
            amount == null -> "null amount"
            amount <= 0 -> "invalid amount ($amount)"
            merchant.isBlank() -> "blank merchant"
            merchantsToIgnore.contains(merchant) -> "ignored merchant"
            MerchantRules.shouldIgnoreMerchant(merchant) -> "merchant ignored by rule(s) ${MerchantRules.findOverlappingRulesPatterns(merchant, MerchantRules.RuleType.IGNORE)}"
            else -> "error, unknown reason"
        }
        return reason
    }
}

