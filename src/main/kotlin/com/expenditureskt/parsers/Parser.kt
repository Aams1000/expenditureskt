package com.expenditureskt.parsers

import com.expenditureskt.categories.Category
import org.apache.commons.csv.CSVRecord

interface Parser {

    fun parse(entries: List<CSVRecord>,
              fileName: String,
              merchantsByCategory: Map<String, Category>,
              merchantsToIgnore: Set<String>) : ParsedFile
}
