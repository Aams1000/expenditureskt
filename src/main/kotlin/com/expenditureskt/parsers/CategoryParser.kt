package com.expenditureskt.parsers

import com.expenditureskt.categories.Category
import com.expenditureskt.categories.db.CategoryLookup
import com.expenditureskt.merchants.MerchantRules

object CategoryParser {

    fun getCategoryForMerchant(merchant: String, merchantsByCategory: Map<String, Category>) : Category {
        val category: Category = merchantsByCategory[merchant]
                                ?: MerchantRules.findCategoryForMerchant(merchant)
                                ?: CategoryLookup.getUnknownCategory()
        return category
    }
}