package com.expenditureskt.parsers

import com.expenditureskt.categories.Category
import org.apache.commons.csv.CSVRecord
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object CitiCreditCardParser {

    private val LINE_FORMAT: Regex = Regex(".+?(?=,),\\d{2}[/]\\d{2}[/]\\d{4},.+?(?=,),(\\d+.\\d{2}|\\d+)?,(-\\d+.\\d{2}|\\d+)?")
    private val DATE_PATTERN: Regex = Regex("\\d{2}[/]\\d{2}[/]\\d{4}")
    private val DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy")

//    Status,Date,Description,Debit,Credit
//    Cleared,09/20/2024,AUTOPAY 230820013832362RAUTOPAY AUTO-PMT,,-99.00
//    Cleared,09/08/2024,AMERICAN0012174514010 FORT WORTH TXNAME: FROSCH/ISABELLE DEPART:11/26/2024 ORD TO BOS: AA: CLASS:B : STOP: X,167.48,
    private val CSV_INDICES: CSVIndices = CSVIndices(date = 1, merchant = 2, amount = 3)

    fun isCorrectFormat(line: String): Boolean {
        return LINE_FORMAT.containsMatchIn(line)
    }

    fun parse(entries: List<CSVRecord>,
              merchantsByCategory: Map<String, Category>,
              merchantsToIgnore: Set<String>): ParsedFile {
        println("Detected Citi credit card file")
        val expenditures: List<ParsedExpenditure> = ParserOperations.parseEntries(entries,
            CSV_INDICES,
            DATE_PATTERN,
            DATE_TIME_FORMATTER,
            merchantsByCategory,
            merchantsToIgnore,
            shouldNegateAmount = false)
        return ParsedFile(expenditures, ParsedFile.AccountType.CITI_CREDIT_CARD, LocalDateTime.now())
    }

}
