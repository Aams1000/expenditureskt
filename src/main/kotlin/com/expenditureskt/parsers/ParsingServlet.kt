package com.expenditureskt.parsers

import com.expenditureskt.categories.Category
import com.expenditureskt.categories.db.CategoryLookup
import com.expenditureskt.common.JsonKeys
import com.expenditureskt.common.requests.RequestResponses
import com.expenditureskt.merchants.db.MerchantsByCategory
import com.expenditureskt.merchants.db.MerchantsToIgnore
import com.expenditureskt.models.DisplayDBExpenditure
import com.expenditureskt.models.DisplayParsedExpenditure
import com.expenditureskt.parsers.db.SpendingUpdates
import com.fasterxml.jackson.core.JsonProcessingException
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVRecord
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.multipart.MultipartFile
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.nio.charset.MalformedInputException
import java.time.LocalDateTime
import java.util.*

@Controller
class ParsingServlet {

    @GetMapping("/parse")
    @ResponseBody
    fun getTest() : ResponseEntity<String> {
        return ResponseEntity("Get request working", HttpStatus.OK)
    }

    @PostMapping("/parse")
    fun uploadFiles(@RequestParam("files[]") files: Array<MultipartFile>) : ResponseEntity<String> {
        val parsingResult: ParsingResult = parseFiles(files)
        val resultMap: Map<String, Any> = insertAndProcessResult(parsingResult)
        val response: ResponseEntity<String> = try {
            RequestResponses.createResponseEntity(resultMap)
        }
        catch (e: JsonProcessingException) {
            RequestResponses.createErrorResponse("Failed to convert object to JSON: $e", HttpStatus.INTERNAL_SERVER_ERROR)
        }
        // TODO - return various status codes depending on response
        return response
    }

    companion object {

        private fun insertAndProcessResult(parsingResult: ParsingResult) : Map<String, Any> {
            val parsedExpenditures: List<ParsedExpenditure> = parsingResult.parsedFiles.flatMap { it.parsedExpenditures }
            // Merchants can be new but have categories assigned by pre-existing rules. Not showing these as "new" merchants
            val newUncategorizedMerchants: List<String> = parsedExpenditures.filter { it.isNewMerchant }
                                                                            .filter { it.categoryId == CategoryLookup.getUnknownCategory().id }.map { it.merchant }.distinct()
            val displayParsedExpenditures: List<DisplayParsedExpenditure> = parsedExpenditures.map { it.toDisplay() }
            val potentialNewMerchantsWithTimestamps: Map<LocalDateTime, List<MerchantAndId>> = arrangeUnknownMerchantsByDate(parsingResult)
            val insertedExpenditures: List<DisplayDBExpenditure> = SpendingUpdates.insertNewMerchantsAndEntries(parsingResult.parsedFiles, potentialNewMerchantsWithTimestamps).map { it.toDisplay() }
            val failedFiles: List<FailedFileWithReason> = parsingResult.failedFiles

            val resultMap: MutableMap<String, Any> = mutableMapOf()
//            val testInsertions: List<DisplayDBExpenditure> = displayParsedExpenditures.subList(0, displayParsedExpenditures.size.coerceAtMost(5)).map { it.toDisplay() }
            resultMap[JsonKeys.FAILED_FILES.key] = failedFiles
            resultMap[JsonKeys.INSERTED_EXPENDITURES.key] = insertedExpenditures
            resultMap[JsonKeys.UNCATEGORIZED_MERCHANTS.key] = newUncategorizedMerchants
            resultMap[JsonKeys.CATEGORIES.key] = CategoryLookup.getAllCategories().map { it.name }
            return resultMap
        }

        private fun arrangeUnknownMerchantsByDate(parsingResult: ParsingResult) : Map<LocalDateTime, List<MerchantAndId>> {
            val merchantsWithTimestamps: MutableMap<LocalDateTime, MutableList<MerchantAndId>> = mutableMapOf()
            for (parsedFile in parsingResult.parsedFiles) {
                val insertionDate: LocalDateTime = parsedFile.timestamp
                merchantsWithTimestamps[insertionDate] = mutableListOf()
                for (entry in parsedFile.parsedExpenditures) {
                    if (entry.isNewMerchant) {
                        merchantsWithTimestamps[insertionDate]!!.add(MerchantAndId(entry.merchant, entry.categoryId))
                    }
                }
            }
            return merchantsWithTimestamps.map { it.key to it.value.toList() }.toMap()
        }

        private fun parseFiles(files: Array<MultipartFile>) : ParsingResult {
            val builder: ParsingResult.Builder = ParsingResult.Builder()
            val merchantsByCategory: Map<String, Category> = MerchantsByCategory.queryMerchantsByCategory()
            val merchantsToIgnore: Set<String> = MerchantsToIgnore.getMerchantsToIgnore()
            for (file: MultipartFile in files) {
                val fileName: String = file.originalFilename!!
                if (isCsv(fileName)) {
                    try {
                        val parsedFile: ParsedFile = parseFile(file, merchantsByCategory, merchantsToIgnore)
                        builder.addParsedFile(parsedFile)
                    }
                    catch (e: Exception) {
                        builder.addFailedFile(FailedFileWithReason(fileName, e.toString()))
                    }
                }
            }
            return builder.build()
        }

        private fun isCsv(fileName: String) : Boolean {
            return fileName.endsWith(".csv")
        }

        private fun parseFile(file: MultipartFile,
                              merchantsByCategory: Map<String, Category>,
                              merchantsToIgnore: Set<String>) : ParsedFile {
            val absolutePath: String = file.originalFilename ?: "Unnamed"
            println("Attempting to parse file $absolutePath")
            //assuming file has a header
            try {
                file.inputStream.use { inputStream ->
                    InputStreamReader(inputStream).use { streamReader ->
                        BufferedReader(streamReader).use { reader ->
//                            val firstLine: String = getFirstNonHeaderLineFromReader(reader)
//                            if (firstLine.isNotEmpty() && firstLine.isNotBlank()) {
//                                val entries: Iterable<CSVRecord> = CSVFormat.RFC4180.withFirstRecordAsHeader().withAllowMissingColumnNames().parse(reader)
                                val entries: List<CSVRecord> = CSVFormat.DEFAULT.withFirstRecordAsHeader().withTrim().withIgnoreEmptyLines().parse(reader).toList()
                            // this should ideally skip any empty lines if the above withIgnoreEmptyLines doesn't work
                                val firstLine: CSVRecord? = entries.firstOrNull { it.isConsistent }
                                val parsedFile: ParsedFile = when {
                                    firstLine != null -> parseEntries(
                                        firstLine,
                                        entries,
                                        merchantsByCategory,
                                        merchantsToIgnore
                                    )
                                    else -> throw UnknownFormatException("File ${file.originalFilename} is empty")
                                }
                                return parsedFile
                        }
                    }
                }
            }
            catch (e: IOException) {
                println("Failed to parse file $absolutePath due to IOException: $e")
                throw e
            }
        }

        private fun parseEntries(rawFirstLine: CSVRecord, entries: List<CSVRecord>, merchantsByCategory: Map<String, Category>, merchantsToIgnore: Set<String>) : ParsedFile {
            val firstLine: String = rawFirstLine.joinToString(separator = ",")
            println("Joined first line is ${firstLine}")
            // I'd ideally like to be able to iterate over the parsers instead of copy/pasting the method calls,
            // but with objects it doesn't really make sense (it would require some collection of singletons)
            // One option would be to make them classes instead of singletons, implement the Parser.kt interface,
            // and then iterate over class instances instead of the objects themselves (these classes would be empty
            // except for the companion objects)
            // Regardless, with so few parsers right now there's no point in worrying about this
            val parsedFile: ParsedFile = when {
                AmexParser.isCorrectFormat(firstLine) -> AmexParser.parse(entries, merchantsByCategory, merchantsToIgnore)
                SchwabParser.isCorrectFormat(firstLine) -> SchwabParser.parse(entries, merchantsByCategory, merchantsToIgnore)
                ChaseParser.isCorrectFormat(firstLine) -> ChaseParser.parse(entries, merchantsByCategory, merchantsToIgnore)
                FidelityCheckingParser.isCorrectFormat(firstLine) -> FidelityCheckingParser.parse(entries, merchantsByCategory, merchantsToIgnore)
                FidelityCreditCardParser.isCorrectFormat(firstLine) -> FidelityCreditCardParser.parse(entries, merchantsByCategory, merchantsToIgnore)
                BofACheckingParser.isCorrectFormat(firstLine) -> BofACheckingParser.parse(entries, merchantsByCategory, merchantsToIgnore)
                BofACreditCardParser.isCorrectFormat(firstLine) -> BofACreditCardParser.parse(entries, merchantsByCategory, merchantsToIgnore)
                CapitalOneCreditCardParser.isCorrectFormat(firstLine) -> CapitalOneCreditCardParser.parse(entries, merchantsByCategory, merchantsToIgnore)
                CapitalOneSavingParser.isCorrectFormat(firstLine) -> CapitalOneSavingParser.parse(entries, merchantsByCategory, merchantsToIgnore)
                CitiCreditCardParser.isCorrectFormat(firstLine) -> CitiCreditCardParser.parse(entries, merchantsByCategory, merchantsToIgnore)
                else -> throw UnknownFormatException("Unable to recognize format of file")
            }
            return parsedFile
        }
    }
//        private fun getFirstNonHeaderLineFromReader(reader: BufferedReader) : String {
//            //remove header
//            reader.readLine()
//
//            var firstNonHeaderLine: String = ""
//            var haveReachedFileEnd: Boolean = false
//            while (firstNonHeaderLine.isEmpty() && !haveReachedFileEnd) {
//                val currLine: String? = reader.readLine()
//                if (currLine != null) {
//                    firstNonHeaderLine = currLine
//                }
//                else {
//                    haveReachedFileEnd = true
//                }
//            }
//            //sets reader back to first line. This is kind of hacky, but for the moment it lets me avoid reading the file in twice
//            reader.mark(0)
//            reader.reset()
//            return firstNonHeaderLine
//        }
//    }

    data class MerchantAndId(val name: String, val id: Int)
}

