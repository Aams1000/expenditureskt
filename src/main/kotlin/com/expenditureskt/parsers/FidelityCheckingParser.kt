package com.expenditureskt.parsers

import com.expenditureskt.categories.Category
import org.apache.commons.csv.CSVRecord
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object FidelityCheckingParser {

    private val LINE_FORMAT: Regex = Regex("\\d{2}[/]\\d{2}[/]\\d{4},.+?(?=,).+?(?=,),.+?(?=,),.+?(?=,).\\d+\\.\\d+,.+?(?=,),[-]?\\d+([.]\\d{1,2})?,")
    private val DATE_PATTERN: Regex = Regex("\\d{2}[/]\\d{2}[/]\\d{4}")
    private val DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy")

    // Downloaded CSV annoyingly has a bunch of blank lines, the word "Brokerage", then the actual beginning of the CSV (delete them):
    //Brokerage
    //
    //Run Date,Action,Symbol,Security Description,Security Type,Quantity,Price ($),Commission ($),Fees ($),Accrued Interest ($),Amount ($),Settlement Date
    //06/30/2022, INTEREST EARNED FDIC INSURED DEPOSIT AT US BANK NOT ... (QUSBQ) (Cash), QUSBQ, FDIC INSURED DEPOSIT AT US BANK NOT COV,Cash,,,,,,0.12,
//06/30/2022, INTEREST EARNED FDIC INSURED DEPOSIT AT BNY MELLON N... (QBNYQ) (Cash), QBNYQ, FDIC INSURED DEPOSIT AT BNY MELLON NOT ,Cash,,,,,,5.01,
    private val CSV_INDICES: CSVIndices = CSVIndices(date = 0, merchant = 1, amount = 10)

    fun isCorrectFormat(line: String) : Boolean {
        return LINE_FORMAT.containsMatchIn(line)
    }

    fun parse(entries: List<CSVRecord>,
              merchantsByCategory: Map<String, Category>,
              merchantsToIgnore: Set<String>): ParsedFile {
        println("Detected Fidelity Checking file")
        val expenditures: List<ParsedExpenditure> = ParserOperations.parseEntries(entries,
            CSV_INDICES,
            DATE_PATTERN,
            DATE_TIME_FORMATTER,
            merchantsByCategory,
            merchantsToIgnore,
            shouldNegateAmount = true)
        return ParsedFile(expenditures, ParsedFile.AccountType.FIDELITY_CHECKING, LocalDateTime.now())
    }
}
