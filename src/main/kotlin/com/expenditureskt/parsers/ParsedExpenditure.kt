package com.expenditureskt.parsers

import com.expenditureskt.models.DisplayFormatter
import com.expenditureskt.models.DisplayParsedExpenditure
import java.time.LocalDate
import java.time.LocalDateTime

data class ParsedExpenditure(val transactionDate: LocalDate,
                             val merchant: String,
                             val amount: Double,
                             val categoryId: Int,
                             val isNewMerchant: Boolean) {

    fun toDisplay() : DisplayParsedExpenditure {
        return DisplayParsedExpenditure(DisplayFormatter.formatDate(transactionDate), merchant, amount, categoryId, DisplayFormatter.formatDate(LocalDateTime.now()))
    }
}

