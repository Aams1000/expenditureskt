package com.expenditureskt.parsers

import com.expenditureskt.categories.Category
import org.apache.commons.csv.CSVRecord
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object CapitalOneCreditCardParser {

    private val LINE_FORMAT: Regex = Regex("\\d{4}[-]\\d{2}[-]\\d{2},\\d{4}[-]\\d{2}[-]\\d{2},\\d+.,.+?(?=,).+?(?=,),(\\d+.\\d{2})?,(\\d+.\\d{2})?")
    private val DATE_PATTERN: Regex = Regex("\\d{4}[-]\\d{2}[-]\\d{2}")
    private val DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

//    Transaction Date,Posted Date,Card No.,Description,Category,Debit,Credit
//    2023-12-16,2023-12-18,6964,TST* DAGON,Dining,108.36,
    private val CSV_INDICES: CSVIndices = CSVIndices(date = 0, merchant = 3, amount = 5)

    fun isCorrectFormat(line: String): Boolean {
        return LINE_FORMAT.containsMatchIn(line)
    }

    fun parse(entries: List<CSVRecord>,
              merchantsByCategory: Map<String, Category>,
              merchantsToIgnore: Set<String>): ParsedFile {
        println("Detected Chase credit card file")
        val expenditures: List<ParsedExpenditure> = ParserOperations.parseEntries(entries,
            CSV_INDICES,
            DATE_PATTERN,
            DATE_TIME_FORMATTER,
            merchantsByCategory,
            merchantsToIgnore,
            shouldNegateAmount = false)
        return ParsedFile(expenditures, ParsedFile.AccountType.CAPITAL_ONE_CREDIT_CARD, LocalDateTime.now())
    }

}
