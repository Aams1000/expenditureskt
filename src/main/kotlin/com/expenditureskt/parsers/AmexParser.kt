package com.expenditureskt.parsers

import com.expenditureskt.categories.Category
import com.expenditureskt.merchants.MerchantRules
import org.apache.commons.csv.CSVRecord
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object AmexParser {

    private val LINE_FORMAT: Regex = Regex("\\d{2}[/]\\d{2}[/]\\d{4},[^,]+,[^,]+,[-]\\d+,[-]?\\d+.\\d{2}")
    private val DATE_PATTERN: Regex = Regex("\\d{2}[/]\\d{2}[/]\\d{4}")
    private val DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy")

    // Based off of downloading a CSV of transactions and *not* selecting the button that says
    // "Include all additional transaction details"
    //06/04/2021,EDGEWATER PRODUCE 02CHICAGO             IL,34.78,"69600068    773-275-3800
    //EDGEWATER PRODUCE 0207
    //CHICAGO
    //IL
    //Description : GROCERIES/SUNDRIES Price : 0.00
    //773-275-3800",EDGEWATER PRODUCE 02CHICAGO             IL,5515 N CLARK ST,"CHICAGO
    //IL",60640,UNITED STATES,'320211560910980218',Merchandise & Supplies-Groceries
    private val CSV_INDICES: CSVIndices = CSVIndices(date = 0, merchant = 1, amount = 4)

    fun isCorrectFormat(line: String) : Boolean {
        return LINE_FORMAT.containsMatchIn(line)
    }

    fun parse(entries: List<CSVRecord>,
              merchantsByCategory: Map<String, Category>,
              merchantsToIgnore: Set<String>): ParsedFile {
        println("Detected Amex file")
        val expenditures: List<ParsedExpenditure> = ParserOperations.parseEntries(entries,
            CSV_INDICES,
            DATE_PATTERN,
            DATE_TIME_FORMATTER,
            merchantsByCategory,
            merchantsToIgnore)
        return ParsedFile(expenditures, ParsedFile.AccountType.AMEX, LocalDateTime.now())
    }
}

