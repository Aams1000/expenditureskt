package com.expenditureskt.parsers

import com.expenditureskt.categories.Category
import org.apache.commons.csv.CSVRecord
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object FidelityCreditCardParser {

//    "Date","Transaction","Name","Memo","Amount"
//    "2022-08-15","DEBIT","Gethsemane Garden Cent Chicago       IL","24707802226030103690947; 05193; ; ; ;","-92.51"
//    "2022-08-15","DEBIT","THE DAVIS THEATER      CHICAGO       IL","24692162227100370546463; 07832; ; ; ;","-18.00"
//    2023-01-03,DEBIT,PLANNED PARENTHOOD FED 800-4304907   NY,24037623002900013200014; 08398; ; ; ;,-100.00

    private val LINE_FORMAT: Regex = Regex("\\d{4}[-]\\d{2}[-]\\d{2},[^,]+,[^,]+,[^,]+,[-]?\\d+\\.\\d{2}")
    private val DATE_PATTERN: Regex = Regex("\\d{4}[-]\\d{2}[-]\\d{2}")
    private val DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

    private val CSV_INDICES: CSVIndices = CSVIndices(date = 0, merchant = 2, amount = 4)

    fun isCorrectFormat(line: String) : Boolean {
        return LINE_FORMAT.containsMatchIn(line)
    }

    fun parse(entries: List<CSVRecord>,
              merchantsByCategory: Map<String, Category>,
              merchantsToIgnore: Set<String>): ParsedFile {
        println("Detected Fidelity credit card file")
        val expenditures: List<ParsedExpenditure> = ParserOperations.parseEntries(entries,
            CSV_INDICES,
            DATE_PATTERN,
            DATE_TIME_FORMATTER,
            merchantsByCategory,
            merchantsToIgnore,
            shouldNegateAmount = true)
        return ParsedFile(expenditures, ParsedFile.AccountType.FIDELITY_CREDIT_CARD, LocalDateTime.now())
    }
}
