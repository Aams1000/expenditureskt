package com.expenditureskt.parsers

import com.expenditureskt.categories.Category
import com.expenditureskt.merchants.MerchantRules
import org.apache.commons.csv.CSVRecord
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object SchwabParser {

    private val LINE_FORMAT: Regex = Regex("\\d{2}[/]\\d{2}[/]\\d{4},.+?(?=,),.+?(?=,),.+?(?=,),(\\\$[\\d,]+[.]\\d{2})?,(\\\$[\\d,]+[.]\\d{2})?,(\\\$[\\d,]+[.]\\d{2})?")
    private val DATE_PATTERN: Regex = Regex("\\d{2}[/]\\d{2}[/]\\d{4}")
    private val DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy")


//    "Date","Status","Type","CheckNumber","Description","Withdrawal","Deposit","RunningBalance"
//    "12/22/2023","Posted","ATM","","NORTH SIDE 5159 N CLARK CHICAGO","$122.00","","$10,236.77"
//    "12/18/2023","Posted","ATM","","NORTH SIDE 5159 N CLARK CHICAGO","$482.00","","$10,358.77"
    private val CSV_INDICES: CSVIndices = CSVIndices(date = 0, merchant = 4, amount = 5)

    fun isCorrectFormat(line: String): Boolean {
        return LINE_FORMAT.matches(line)
    }

    fun parse(entries: List<CSVRecord>,
              merchantsByCategory: Map<String, Category>,
              merchantsToIgnore: Set<String>): ParsedFile {
        println("Detected Schwab Checking file")
            val expenditures: List<ParsedExpenditure> = ParserOperations.parseEntries(entries,
                CSV_INDICES,
                DATE_PATTERN,
                DATE_TIME_FORMATTER,
                merchantsByCategory,
                merchantsToIgnore)
            return ParsedFile(expenditures, ParsedFile.AccountType.SCHWAB, LocalDateTime.now())
    }
}

