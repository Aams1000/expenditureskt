package com.expenditureskt.parsers

import java.time.LocalDateTime

data class ParsedFile(val parsedExpenditures: List<ParsedExpenditure>, val accountType: AccountType, val timestamp: LocalDateTime) {

    enum class AccountType(val id: Int) {
        AMEX(0),
        CHASE(2),
        SCHWAB(3),
        FIDELITY_CHECKING(4),
        FIDELITY_CREDIT_CARD(5),
        BANK_OF_AMERICA_CREDIT_CARD(6),
        BANK_OF_AMERICA_CHECKING(7),
        CAPITAL_ONE_CREDIT_CARD(8),
        CAPITAL_ONE_SAVING(9),
        CITI_CREDIT_CARD(10)
    }
}

