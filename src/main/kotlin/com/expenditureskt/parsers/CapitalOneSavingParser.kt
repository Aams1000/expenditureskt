package com.expenditureskt.parsers

import com.expenditureskt.categories.Category
import org.apache.commons.csv.CSVRecord
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object CapitalOneSavingParser {

    private val LINE_FORMAT: Regex = Regex("\\d{4},.+?(?=,),\\d{2}[/]\\d{2}[/]\\d{2},.+?(?=,),(\\d+.\\d{2}|\\d+)?,(\\d+.\\d{2}|\\d+)?")
    private val DATE_PATTERN: Regex = Regex("\\d{2}[/]\\d{2}[/]\\d{2}")
    private val DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yy")

//    Account Number,Transaction Description,Transaction Date,Transaction Type,Transaction Amount,Balance
//    1022,Monthly Interest Paid,12/31/24,Credit,415.43,131175.37
    private val CSV_INDICES: CSVIndices = CSVIndices(date = 2, merchant = 1, amount = 4)

    fun isCorrectFormat(line: String): Boolean {
        return LINE_FORMAT.containsMatchIn(line)
    }

    fun parse(entries: List<CSVRecord>,
              merchantsByCategory: Map<String, Category>,
              merchantsToIgnore: Set<String>): ParsedFile {
        println("Detected CapitalOne Savings file")
        print("Filtering entries to those of 'Transaction Type' Debit")
        val filteredEntries: List<CSVRecord> = entries.filter { it.get("Transaction Type").equals("Debit") }
        val expenditures: List<ParsedExpenditure> = ParserOperations.parseEntries(filteredEntries,
            CSV_INDICES,
            DATE_PATTERN,
            DATE_TIME_FORMATTER,
            merchantsByCategory,
            merchantsToIgnore,
            shouldNegateAmount = false)
        return ParsedFile(expenditures, ParsedFile.AccountType.CAPITAL_ONE_SAVING, LocalDateTime.now())
    }

}
