package com.expenditureskt.parsers

data class ParsingResult(val parsedFiles: List<ParsedFile>,
                        val failedFiles: List<FailedFileWithReason>,
                        val skippedFiles: List<String>) {

    class Builder {
        private val parsedFiles: MutableList<ParsedFile> = mutableListOf()
        private val failedFiles: MutableList<FailedFileWithReason> = mutableListOf()
        private val skippedFiles: MutableList<String> = mutableListOf()

        fun addParsedFile(file: ParsedFile) : Unit {
            parsedFiles.add(file)
        }

        fun addFailedFile(file: FailedFileWithReason) : Unit {
            failedFiles.add(file)
        }

        fun addSkippedFile(fileName: String) : Unit {
            skippedFiles.add(fileName)
        }

        fun build() : ParsingResult {
            return ParsingResult(parsedFiles, failedFiles, skippedFiles)
        }
    }
}

