package com.expenditureskt.parsers

import com.fasterxml.jackson.annotation.JsonProperty

data class FailedFileWithReason(@JsonProperty("fileName") val fileName: String, @JsonProperty("reason") val reason: String)
