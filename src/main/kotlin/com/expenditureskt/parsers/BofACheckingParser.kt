package com.expenditureskt.parsers

import com.expenditureskt.categories.Category
import org.apache.commons.csv.CSVRecord
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object BofACheckingParser {

//    \d{2}[/]\d{2}[/]\d{4},.+?(?=",)","[-]?\d+[,]?([\d]+)?+\.\d{2}","[-]?\d+[,]?([\d]+)?+\.\d{2}"
//    private val LINE_FORMAT: Regex = Regex("Beginning balance as of \\d{2}/\\d{2}/\\d{4},,\"[-]?\\d+[,]?([\\d]+)?+\\.\\d{2}\"")
    private val LINE_FORMAT: Regex = Regex("\\d{2}[/]\\d{2}[/]\\d{4},.+?(?=,),[-]?\\d+[,]?([\\d]+)?+\\.\\d{2},[-]?\\d+[,]?([\\d]+)?+\\.\\d{2}")
    private val DATE_PATTERN: Regex = Regex("\\d{2}[/]\\d{2}[/]\\d{4}")
    private val DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy")

    //    Transaction Date,Post Date,Description,Category,Type,Amount,Memo
    //    12/18/2020,12/20/2020,NORTHWESTERN MY CHART,Health & Wellness,Sale,-48.50,
    private val CSV_INDICES: CSVIndices = CSVIndices(date = 0, merchant = 1, amount = 2)

    fun isCorrectFormat(line: String): Boolean {
        return LINE_FORMAT.containsMatchIn(line)
    }

    fun parse(entries: List<CSVRecord>,
              merchantsByCategory: Map<String, Category>,
              merchantsToIgnore: Set<String>): ParsedFile {
        println("Detected BofA Checking file")
        val expenditures: List<ParsedExpenditure> = ParserOperations.parseEntries(entries,
            CSV_INDICES,
            DATE_PATTERN,
            DATE_TIME_FORMATTER,
            merchantsByCategory,
            merchantsToIgnore,
            shouldNegateAmount = true)
        return ParsedFile(expenditures, ParsedFile.AccountType.BANK_OF_AMERICA_CHECKING, LocalDateTime.now())
    }

}
