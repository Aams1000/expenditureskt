package com.expenditureskt.parsers.db

import com.expenditureskt.categories.db.CategoryLookup
import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam

enum class ParsingQuery(override val query: String,
                        override val queryIndicesByParam: Map<QueryParam, List<Int>>,
                        override val resultIndexByParam: Map<QueryParam, Int>) : QueryInfo {

    QUERY_PARSED_FILES("select file_name, transaction_date from t_parsed_files",

            mapOf(),
            mapOf(QueryParam.FILE_NAME to 1,
                    QueryParam.DATE_LAST_MODIFIED to 2,
                    QueryParam.FILE_ID to 3)
    ),

    BACKUP_PARSED_FILES("drop table if exists t_parsed_files_backup " +
            "create table t_parsed_files_backup (like t_parsed_files including all) " +
            "insert into t_parsed_files_backup select * from t_parsed_files",

            mapOf(),
            mapOf()
    ),

    DELETE_PARSED_FILES_BY_NAME("delete from t_parsed_files where file_name = ?",

            mapOf(QueryParam.FILE_NAME to listOf(1)),
            mapOf()
    ),

    INSERT_PARSED_FILES("insert into t_parsed_files values (default, ?, ?)",

            mapOf(QueryParam.FILE_NAME to listOf(1),
                    QueryParam.DATE_LAST_MODIFIED to listOf(2)),
            mapOf()
    ),

    BACKUP_EXPENDITURES("drop table if exists t_expenditures_backup; " +
            "create table t_expenditures_backup (like t_expenditures including all); " +
            "insert into t_expenditures_backup select * from t_expenditures",

            mapOf(),
            mapOf()
    ),

    INSERT_EXPENDITURES("insert into t_expenditures values (default, ?, ?, ?, ?, ?) on conflict do nothing",

        mapOf(QueryParam.DATE_OF_TRANSACTION to listOf(1),
            QueryParam.MERCHANT to listOf(2),
            QueryParam.CATEGORY_ID to listOf(3),
            QueryParam.AMOUNT to listOf(4),
            QueryParam.INSERTION_DATE to listOf(5)),
        mapOf()
    ),

    INSERT_ACCOUNT_TYPES_FOR_EXPENDITURES("insert into t_account_type values (?, ?)",

        mapOf(QueryParam.EXPENDITURE_ID to listOf(1),
              QueryParam.ACCOUNT_TYPE to listOf(2)),
        mapOf()

    ),

    QUERY_UNCATEGORIZED_EXPENDITURES("select id, transaction_date, merchant, amount from t_expenditures where category = '" + CategoryLookup.getUnknownCategory().id + "'",

            mapOf(),
            mapOf(QueryParam.EXPENDITURE_ID to 1,
                    QueryParam.DATE_OF_TRANSACTION to 2,
                    QueryParam.MERCHANT to 3,
                    QueryParam.AMOUNT to 4)
    ),

    UPDATE_EXPENDITURES_CATEGORIES("update t_expenditures set category = ? where id = ?",

            mapOf(QueryParam.CATEGORY_ID to listOf(1),
                    QueryParam.EXPENDITURE_ID to listOf(2)),
            mapOf()
    )

}
