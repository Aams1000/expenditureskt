package com.expenditureskt.parsers.db

import com.expenditureskt.categories.Category
import com.expenditureskt.common.db.DBExpenditure
import com.expenditureskt.common.db.DatabaseConnections
import com.expenditureskt.common.db.PreparedStatements
import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam
import com.expenditureskt.homepage.db.ExpendituresDAO
import com.expenditureskt.parsers.ParsedExpenditure
import com.expenditureskt.parsers.ParsedFile
import com.expenditureskt.homepage.db.ExpendituresQuery
import com.expenditureskt.merchants.db.MerchantsUpdates
import com.expenditureskt.parsers.ParsingServlet
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement
import java.time.LocalDateTime
import kotlin.Exception

object SpendingUpdates {

    fun insertNewMerchantsAndEntries(parsedFiles: List<ParsedFile>, potentialNewMerchants: Map<LocalDateTime, List<ParsingServlet.MerchantAndId>>) : List<DBExpenditure> {
        val insertedExpenditures: MutableList<DBExpenditure> = mutableListOf()
        try {
            DatabaseConnections.getNonAutoCommitExpendituresConnection().use { connection ->
                MerchantsUpdates.backupMerchantsTable(connection)
                MerchantsUpdates.addNewMerchants(connection, potentialNewMerchants)
                backupExpendituresTable(connection)
                val insertedIds: List<Int> = insertNewRows(connection, parsedFiles)
                insertedExpenditures.addAll(queryExpendituresFromIds(connection, insertedIds))
                updateAccountTypesForInsertedExpenditures(connection, insertedExpenditures, parsedFiles)
                try {
                    connection.commit()
                }
                catch (e: SQLException) {
                    println("Failed to commit changes to database due to exception. Rolling back. $e")
                    try {
                        connection.rollback()
                    }
                    catch (e2: SQLException) {
                        println("Failed to rollback after failed commit. Something is severely wrong. $e2")
                    }
                }
            }
        }
        catch (e: Exception) {
            println("Failed to update parsed files due to exception: $e")
            throw e
        }
        return insertedExpenditures
    }

    private fun insertNewRows(connection: Connection, parsedFiles: List<ParsedFile>) : List<Int> {
        val queryInfo: QueryInfo = ParsingQuery.INSERT_EXPENDITURES
        try {
            connection.prepareStatement(queryInfo.query, Statement.RETURN_GENERATED_KEYS).use { statement ->
                for (parsedFile: ParsedFile in parsedFiles) {
                    val expenditures: List<ParsedExpenditure> = parsedFile.parsedExpenditures
                    for (entry: ParsedExpenditure in expenditures) {
                        PreparedStatements.setDates(statement, queryInfo.getQueryIndices(QueryParam.DATE_OF_TRANSACTION), entry.transactionDate)
                        PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.MERCHANT), entry.merchant)
                        PreparedStatements.setInts(statement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), entry.categoryId)
                        PreparedStatements.setDoubles(statement, queryInfo.getQueryIndices(QueryParam.AMOUNT), entry.amount)
                        PreparedStatements.setTimestamps(statement, queryInfo.getQueryIndices(QueryParam.INSERTION_DATE), parsedFile.timestamp)
                        statement.addBatch()
                    }
                }
                statement.executeBatch()
                val generatedKeys: List<Int> = getGeneratedKeysFromResultSet(statement.generatedKeys)
                return generatedKeys
            }
        }
        catch (e: Exception) {
            println("Failed to insert new expenditures due to exception $e")
            throw e
        }
    }

    private fun updateAccountTypesForInsertedExpenditures(connection: Connection, insertedExpenditures: List<DBExpenditure>, parsedFiles: List<ParsedFile>) : Unit {
        val accountTypeToExpenditureIds: Map<Int, Set<Long>> = createAccountTypeToIdsMap(insertedExpenditures, parsedFiles)
        val queryInfo: QueryInfo = ParsingQuery.INSERT_ACCOUNT_TYPES_FOR_EXPENDITURES
        try {
            connection.prepareStatement(queryInfo.query).use { statement ->
                for ((accountType, expenditureIds) in accountTypeToExpenditureIds) {
                    for (id in expenditureIds) {
                        PreparedStatements.setInts(statement, queryInfo.getQueryIndices(QueryParam.ACCOUNT_TYPE), accountType)
                        PreparedStatements.setLongs(statement, queryInfo.getQueryIndices(QueryParam.EXPENDITURE_ID), id)
                        statement.addBatch()
                    }
                }
                statement.executeBatch()
            }
        }
        catch (e: Exception) {
            println("Failed to insert new expenditures into t_account_type due to exception $e")
            throw e
        }
    }

    private fun getGeneratedKeysFromResultSet(resultSet: ResultSet) : List<Int> {
        val generatedKeys: MutableList<Int> = mutableListOf()
        while (resultSet.next()) {
            generatedKeys.add(resultSet.getInt(1))
        }
        return generatedKeys
    }

    private fun queryExpendituresFromIds(connection: Connection, ids: List<Int>) : List<DBExpenditure> {
        val queryInfo: QueryInfo = ExpendituresQuery.QUERY_EXPENDITURES_BY_ID
        connection.prepareStatement(queryInfo.query).use { statement ->
            PreparedStatements.setArrays(statement, queryInfo.getQueryIndices(QueryParam.EXPENDITURE_ID), DatabaseConnections.convertIntListToSqlArray(connection, ids))
            val resultSet: ResultSet = statement.executeQuery()
            return ExpendituresDAO.extractEntriesFromResultSet(queryInfo, resultSet)
        }
    }

    fun updateCategoriesForEntries(idsToNewCategories: Map<Long, Category>) : Int {
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                backupExpendituresTable(connection)
                return updateCategoriesForExpenditureIds(connection, idsToNewCategories)
            }
        }
        catch (e: Exception) {
            println("Failed to update categories due to exception: $e")
            throw e
        }
    }

    private fun updateCategoriesForExpenditureIds(connection: Connection, idsToNewCategories: Map<Long, Category>) : Int {
        val queryInfo: QueryInfo = ParsingQuery.UPDATE_EXPENDITURES_CATEGORIES
        try {
            connection.prepareStatement(queryInfo.query).use { statement ->
                for ((id: Long, category: Category) in idsToNewCategories) {
                    PreparedStatements.setLongs(statement, queryInfo.getQueryIndices(QueryParam.EXPENDITURE_ID), id)
                    PreparedStatements.setInts(statement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), category.id)
                    statement.addBatch()
                }
                statement.executeBatch()
                return statement.updateCount
            }
        }
        catch (e: Exception) {
            println("Failed to update expenditures rows due to exception: $e")
            throw e
        }
    }

    fun backupExpendituresTable(connection: Connection) : Unit {
        try {
            connection.prepareStatement(ParsingQuery.BACKUP_EXPENDITURES.query).use { statement ->
                statement.execute()
            }
        }
        catch (e: SQLException) {
            println("Failed to backup expenditures table due to SQLException: $e")
            throw e
        }
    }

    fun queryUncategorizedExpenditures() : List<DBExpenditure> {
        val queryInfo: QueryInfo = ParsingQuery.QUERY_UNCATEGORIZED_EXPENDITURES
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                connection.prepareStatement(queryInfo.query).use { statement ->
                    val resultSet: ResultSet = statement.executeQuery()
                    return ExpendituresDAO.extractEntriesFromResultSet(queryInfo, resultSet)
                }
            }
        }
        catch (e: Exception) {
            println("Failed to query uncategorized expenditures due to exception: $e")
            throw e
        }
    }

    private fun createAccountTypeToIdsMap(insertedExpenditures: List<DBExpenditure>,parsedFiles: List<ParsedFile>) : Map<Int, Set<Long>> {
        val accountTypeToIds: MutableMap<Int, MutableSet<Long>> = mutableMapOf()
        for (parsedFile: ParsedFile in parsedFiles) {
            for (expenditure: ParsedExpenditure in parsedFile.parsedExpenditures) {
                // Edge case: the files might have contained duplicate expenditures, meaning two ParsedExpenditure objects can
                // correspond to the same DBExpenditure, hence the set
                insertedExpenditures.firstOrNull { areParsedAndDBExpenditureEqual(it, expenditure) }?.let { accountTypeToIds.computeIfAbsent(parsedFile.accountType.id) { mutableSetOf() }.add(it.id) }
            }
        }
        val result: Map<Int, Set<Long>> = accountTypeToIds.entries.associate { (accountType, mutableSet) -> accountType to mutableSet.toSet() }
        return result
    }

    private fun areParsedAndDBExpenditureEqual(dbExpenditure: DBExpenditure, parsedExpenditure: ParsedExpenditure) : Boolean {
        return dbExpenditure.amount == parsedExpenditure.amount
                && dbExpenditure.categoryId == parsedExpenditure.categoryId
                && dbExpenditure.merchant == parsedExpenditure.merchant
                && dbExpenditure.transactionDate == parsedExpenditure.transactionDate
    }
}
