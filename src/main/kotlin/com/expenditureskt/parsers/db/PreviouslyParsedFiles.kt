package com.expenditureskt.parsers.db

import com.expenditureskt.common.db.DatabaseConnections
import com.expenditureskt.common.db.PreparedStatements
import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam
import com.expenditureskt.common.db.QueryResult
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException

@Deprecated("No longer relevant to ensuring non-duplicate parsing of files")
object PreviouslyParsedFiles {

    fun queryFileNamesAndDateLastModified() : Map<String, Long> {
        val fileNamesToDates: MutableMap<String, Long> = mutableMapOf()
        val queryInfo: QueryInfo = ParsingQuery.QUERY_PARSED_FILES
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                connection.prepareStatement(queryInfo.query).use { statement ->
                    val resultSet: ResultSet = statement.executeQuery()
                    while (resultSet.next()) {
                        val fileName: String = resultSet.getString(queryInfo.getResultIndex(QueryParam.FILE_NAME))
                        val timestamp: Long = resultSet.getLong(queryInfo.getResultIndex(QueryParam.DATE_LAST_MODIFIED))
                        fileNamesToDates[fileName] = timestamp
                    }
                }
            }
        }
        catch (e: Exception) {
            println("Failed to query parsed files due to exception: $e")
        }
        return fileNamesToDates
    }

    fun updateParsedFiles(fileNamesByDateModified: Map<String, Long>) : QueryResult {
        var result: QueryResult = QueryResult.SUCCESS
        try {
            DatabaseConnections.getExpendituresConnection().use { connection ->
                backupParsedFiles(connection)
                deleteRowsThatWillBeReplaced(connection, fileNamesByDateModified)
                insertNewRows(connection, fileNamesByDateModified)
            }
        }
        catch (e: Exception) {
            println("Failed to update parsed files due to exception: $e")
            result = QueryResult.FAILURE
        }
        return result
    }

    private fun insertNewRows(connection: Connection, fileNamesByDateModified: Map<String, Long>) : Unit {
        val queryInfo: QueryInfo = ParsingQuery.INSERT_PARSED_FILES
        connection.prepareStatement(queryInfo.query).use { statement ->
            for ((fileName: String, timestamp: Long) in fileNamesByDateModified) {
                PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.FILE_NAME), fileName)
                PreparedStatements.setLongs(statement, queryInfo.getQueryIndices(QueryParam.DATE_LAST_MODIFIED), timestamp)
                statement.addBatch()
            }
            statement.executeBatch()
        }
    }

    private fun deleteRowsThatWillBeReplaced(connection: Connection, fileNamesByDateModified: Map<String, Long>) : Unit {
        val queryInfo: QueryInfo = ParsingQuery.DELETE_PARSED_FILES_BY_NAME
        try {
            connection.prepareStatement(queryInfo.query).use { statement ->
                for ((fileName: String, timestamp: Long) in fileNamesByDateModified) {
                    PreparedStatements.setStrings(statement, queryInfo.getQueryIndices(QueryParam.FILE_NAME), fileName)
                    PreparedStatements.setLongs(statement, queryInfo.getQueryIndices(QueryParam.DATE_LAST_MODIFIED), timestamp)
                    statement.addBatch()
                }
                statement.executeBatch()
            }
        }
        catch(e: Exception) {
            println("Failed to delete parsed file rows due to exception: $e")
            throw e
        }
    }

    private fun backupParsedFiles(connection: Connection) : Unit {
        try {
            connection.prepareStatement(ParsingQuery.BACKUP_PARSED_FILES.query).use { statement ->
                statement.execute()
            }
        }
        catch (e: SQLException) {
            println("Failed to backup parsed files due to SQLException: $e")
            throw e
        }
    }
}

