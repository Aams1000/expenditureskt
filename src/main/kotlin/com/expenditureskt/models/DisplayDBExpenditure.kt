package com.expenditureskt.models

import com.fasterxml.jackson.annotation.JsonProperty

data class DisplayDBExpenditure(@JsonProperty("id") val id: Long,
                                @JsonProperty("transactionDate") val transactionDate: String,
                                @JsonProperty("merchant") val merchant: String,
                                @JsonProperty("amount") val amount: Double,
                                @JsonProperty("category") val categoryName: String,
                                @JsonProperty("insertionDate") val insertionDate: String)

