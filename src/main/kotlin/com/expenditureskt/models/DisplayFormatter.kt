package com.expenditureskt.models

import com.expenditureskt.models.DisplayFormatter.round
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Month
import java.time.format.DateTimeFormatter

object DisplayFormatter {

    private fun Double.round(places: Int) : Double {
        var multiplier: Double = 1.0
        repeat(places) { multiplier *= 10 }
        return kotlin.math.round(this * multiplier) / multiplier
    }

    fun roundDollarAmount(amount: Double) : Double {
        return roundDouble(amount, 2)
    }

    fun roundDouble(value: Double, places: Int) : Double {
        return value.round(places)
    }

    fun formatMonth(month: Month) : String {
        val stringMonth: String = month.toString()
        return stringMonth[0].toUpperCase() + stringMonth.substring(1).toLowerCase()
    }

    fun formatMonthAndYear(monthAndYear: MonthAndYear) : String {
        val formattedMonth: String = formatMonth(monthAndYear.month)
        return formattedMonth + " '" + monthAndYear.year.toString().substring(2)
    }

    fun formatDate(localDate: LocalDate) : String {
        return localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
    }

    fun formatDate(localDateTime: LocalDateTime) : String {
        return localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
    }
}