package com.expenditureskt.models

import com.expenditureskt.categories.db.CategoryLookup
import com.fasterxml.jackson.annotation.JsonProperty

// Mainly for testing and debugging purposes
data class DisplayParsedExpenditure(@JsonProperty("transactionDate") val transactionDate: String,
                                    @JsonProperty("merchant") val merchant: String,
                                    @JsonProperty("amount") val amount: Double,
                                    @JsonProperty("categoryId") val categoryId: Int,
                                    @JsonProperty("insertionDate") val insertionDate: String) {

    fun toDisplay() : DisplayDBExpenditure {
        return DisplayDBExpenditure(categoryId.toLong(), transactionDate, merchant ,amount, CategoryLookup.getCategoryForId(categoryId)!!.name, insertionDate)
    }
}
