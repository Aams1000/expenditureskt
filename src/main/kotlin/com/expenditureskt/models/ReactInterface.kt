package com.expenditureskt.models

import java.io.BufferedReader
import java.io.File
import java.io.IOException

object ReactInterface {

    enum class PageType(val filePath: String) {
        EXPENDITURES_GET("frontend/index.html")
    }

    fun getBaseTemplate(type: PageType): String {
        val bufferedReader: BufferedReader = File(type.filePath).bufferedReader(charset = Charsets.UTF_8)
        val template: String = bufferedReader.use {
            try {
                it.readText()
            }
            catch (e: IOException) {
                println("Failed to load template for type: ${type.name}")
                throw e
            }
        }
        return template
    }
}