package com.expenditureskt.models

import com.expenditureskt.categories.Category
import com.expenditureskt.categories.db.CategoryLookup
import com.expenditureskt.common.db.QueryInfo
import com.expenditureskt.common.db.QueryParam
import com.fasterxml.jackson.annotation.JsonProperty
import java.sql.ResultSet
import java.time.LocalDate


data class PercentChangesSummary(@JsonProperty("totalSpending") val totalSpending: Double,
                                 @JsonProperty("percentChange") val percentChange: Double,
                                 @JsonProperty("absoluteChange") val absoluteChange: Double,
                                 @JsonProperty("changesByCategory") val changesByCategory: Map<String, Double>,
                                 @JsonProperty("startDate") val startDate: String,
                                 @JsonProperty("endDate") val endDate: String) {

    companion object StaticConstructor {

        fun fromUnformattedValues(totalSpending: Double,
                                  percentChange: Double,
                                  absoluteChange: Double,
                                  changesByCategory: Map<String, Double>,
                                  startDate: LocalDate,
                                  endDate: LocalDate) : PercentChangesSummary {
            return PercentChangesSummary(DisplayFormatter.roundDollarAmount(totalSpending),
                DisplayFormatter.roundDouble(percentChange, 2),
                DisplayFormatter.roundDollarAmount(absoluteChange),
                changesByCategory.map { (category, percentChange) -> category to DisplayFormatter.roundDouble(percentChange, 2) }.toMap(),
                DisplayFormatter.formatDate(startDate),
                DisplayFormatter.formatDate(endDate)
            )
        }
    }
}

data class CategorySummary(@JsonProperty("totalSpending") val totalSpending: Double,
                           @JsonProperty("spendingByCategory") val spendingByCategory: Map<String, Double>) {

    companion object StaticConstructor {

        fun fromResultSet(resultSet: ResultSet, queryInfo: QueryInfo) : CategorySummary {
            var overallSpending: Double = 0.0
            val spendingMap: MutableMap<String, Double> = mutableMapOf()
            while (resultSet.next()) {
                val id: Int = resultSet.getInt(queryInfo.getResultIndex(QueryParam.CATEGORY_ID))
                val spending: Double = resultSet.getDouble(queryInfo.getResultIndex(QueryParam.AMOUNT))
                val category: Category? = CategoryLookup.getCategoryForId(id)
                if (category != null) {
                    overallSpending += spending
                    spendingMap[category.name] = DisplayFormatter.roundDollarAmount(spending)
                }
                else {
                    println("Unknown expenditure category $id retrieved from database.")
                }
            }
            return CategorySummary(DisplayFormatter.roundDollarAmount(overallSpending), spendingMap.toSortedMap())
        }

        fun fromSpendingAndCategoryMap(overallSpending: Double, categoryMap: Map<Category, Double>) : CategorySummary {
            val updatedMap: Map<String, Double> = categoryMap.map { (category, amount) -> category.name to DisplayFormatter.roundDollarAmount(amount) }.toMap()
            println(updatedMap)
            return CategorySummary(DisplayFormatter.roundDollarAmount(overallSpending), updatedMap)
        }
    }

}

