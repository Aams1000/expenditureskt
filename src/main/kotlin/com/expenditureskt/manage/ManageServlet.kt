package com.expenditureskt.manage

import com.expenditureskt.categories.db.CategoryLookup
import com.expenditureskt.common.JsonKeys
import com.expenditureskt.common.requests.RequestResponses
import com.expenditureskt.homepage.db.ExpendituresDAO
import com.expenditureskt.merchants.MerchantRules
import com.expenditureskt.merchants.db.MerchantsByCategory
import com.expenditureskt.merchants.db.MerchantsToIgnore
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class ManageServlet {

    @GetMapping("/manage")
    fun handleGetRequest() : ResponseEntity<String> {
        val response: ResponseEntity<String> = try {
            val responseMap: MutableMap<String, Any> = mutableMapOf()
            responseMap[JsonKeys.MERCHANT_RULES.key] = MerchantRules.getRulesToCategories().map { it.key to it.value.name }.toMap()
            responseMap[JsonKeys.IGNORED_MERCHANT_RULES.key] = MerchantRules.getIgnoreRules()
            responseMap[JsonKeys.IGNORED_MERCHANTS.key] = MerchantsToIgnore.getMerchantsToIgnore().toList()
            responseMap[JsonKeys.MERCHANTS_BY_CATEGORY.key] = MerchantsByCategory.queryMerchantsByCategory().map { it.key to it.value.name }.toMap()
            responseMap[JsonKeys.ALL_EXPENDITURES.key] = ExpendituresDAO.retrieveAllExpenditures().map { it.toDisplay() }
            responseMap[JsonKeys.CATEGORIES.key] = CategoryLookup.getAllCategories().map { it.name }.sorted()
            RequestResponses.createResponseEntity(responseMap)
        }
        catch (e: Exception) {
            RequestResponses.createErrorResponse("Failed to return manageable content due to exception $e", HttpStatus.INTERNAL_SERVER_ERROR)
        }
        return response
    }

}
