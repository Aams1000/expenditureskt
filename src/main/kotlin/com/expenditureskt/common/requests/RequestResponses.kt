package com.expenditureskt.common.requests

import com.expenditureskt.common.JsonKeys
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

object RequestResponses {

    fun createResponseEntity(responseMap: Map<String, Any>, status: HttpStatus = HttpStatus.OK) : ResponseEntity<String> {
        val objectWriter: ObjectWriter = ObjectMapper().registerKotlinModule().writer().withDefaultPrettyPrinter()
        return ResponseEntity(objectWriter.writeValueAsString(responseMap), status)
    }

    fun createErrorResponse(responseMap: MutableMap<String, Any>,
                                    e: Exception) : ResponseEntity<String> {
        responseMap[JsonKeys.ERROR.key] = e.toString()
        val objectWriter: ObjectWriter = ObjectMapper().registerKotlinModule().writer().withDefaultPrettyPrinter()
        return ResponseEntity(objectWriter.writeValueAsString(responseMap), HttpStatus.INTERNAL_SERVER_ERROR)
    }

    fun createErrorResponse(message: String, status: HttpStatus) : ResponseEntity<String> {
        val objectWriter: ObjectWriter = ObjectMapper().registerKotlinModule().writer().withDefaultPrettyPrinter()
        val responseMap: Map<String, String> = mapOf(JsonKeys.ERROR.key to message)
        return ResponseEntity(objectWriter.writeValueAsString(responseMap), status)
    }
}