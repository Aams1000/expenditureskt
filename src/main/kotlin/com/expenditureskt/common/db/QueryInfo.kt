package com.expenditureskt.common.db

// Right now I'm making this more of an abstract class, as every implementation
// has the exact same implementation of the functions. The downside is that
// the members are visible
interface QueryInfo {

    val query: String
    val queryIndicesByParam: Map<QueryParam, List<Int>>
    val resultIndexByParam: Map<QueryParam, Int>

    fun getQueryIndices(param: QueryParam) : List<Int> {
        return queryIndicesByParam.getOrDefault(param, listOf())
    }

    fun getResultIndex(param: QueryParam) : Int {
        return resultIndexByParam.getValue(param)
    }
}
