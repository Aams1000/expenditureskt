package com.expenditureskt.common.db

import com.expenditureskt.categories.db.CategoryLookup
import com.expenditureskt.models.DisplayDBExpenditure
import com.expenditureskt.models.DisplayFormatter
import java.time.LocalDate
import java.time.LocalDateTime

data class DBExpenditure(val id: Long,
                         val transactionDate: LocalDate,
                         val merchant: String,
                         val amount: Double,
                         val categoryId: Int,
                         val insertionDate: LocalDateTime) {

    fun toDisplay() : DisplayDBExpenditure {
        return DisplayDBExpenditure(id,
                                    DisplayFormatter.formatDate(transactionDate),
                                    merchant,
                                    amount,
                                    CategoryLookup.getCategoryForId(categoryId)!!.name,
                                    DisplayFormatter.formatDate(insertionDate))
    }
}