package com.expenditureskt.common.db

import java.sql.PreparedStatement
import java.sql.Timestamp
import java.time.LocalDate
import java.time.LocalDateTime

object PreparedStatements {

    fun setDates(statement: PreparedStatement, indices: List<Int>, date: LocalDate) : Unit {
        for (index in indices) {
            statement.setDate(index, java.sql.Date.valueOf(date))
        }
    }

    fun setTimestamps(statement: PreparedStatement, indices: List<Int>, localDateTime: LocalDateTime) : Unit {
        for (index in indices) {
            statement.setObject(index, localDateTime)
        }
    }

    fun setStrings(statement: PreparedStatement, indices: List<Int>, value: String) : Unit {
        for (index in indices) {
            statement.setString(index, value)
        }
    }

    fun setInts(statement: PreparedStatement, indices: List<Int>, value: Int) : Unit {
        for (index in indices) {
            statement.setInt(index, value)
        }
    }

    fun setDoubles(statement: PreparedStatement, indices: List<Int>, value: Double) : Unit {
        for (index in indices) {
            statement.setDouble(index, value)
        }
    }

    fun setArrays(statement: PreparedStatement, indices: List<Int>, value: java.sql.Array) : Unit {
        for (index in indices) {
            statement.setArray(index, value)
        }
    }

    fun setLongs(statement: PreparedStatement, indices: List<Int>, value: Long) : Unit {
        for (index in indices) {
            statement.setLong(index, value)
        }
    }
}
