package com.expenditureskt.common.db

enum class QueryResult {
    SUCCESS,
    FAILURE
}

