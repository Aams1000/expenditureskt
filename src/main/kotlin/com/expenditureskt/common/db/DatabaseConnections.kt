package com.expenditureskt.common.db

import com.expenditureskt.categories.Category
import java.sql.Connection
import java.sql.DriverManager
import java.sql.JDBCType

object DatabaseConnections {

    fun getExpendituresConnection(shouldUseAutoCommit: Boolean = true) : Connection {
        //TODO - move this String to Config
        try {
            Class.forName("org.postgresql.Driver")
        }
        catch (e: ClassNotFoundException) {
            throw RuntimeException("Unable to load Postgres driver", e)
        }
        val connection: Connection =  DriverManager.getConnection("jdbc:postgresql://localhost/finances")
        connection.setAutoCommit(shouldUseAutoCommit)
        return connection
    }

    fun getNonAutoCommitExpendituresConnection() : Connection {
        return getExpendituresConnection(false)
    }

    fun convertExpendituresToSqlArray(connection: Connection, categories: List<Category>) : java.sql.Array {
        return connection.createArrayOf(JDBCType.INTEGER.getName(), categories.map {it.id}.toTypedArray())
    }

    fun convertIntListToSqlArray(connection: Connection, list: List<Int>) : java.sql.Array {
        return connection.createArrayOf(JDBCType.INTEGER.getName(), list.toTypedArray())
    }

    fun convertStringListToSqlArray(connection: Connection, list: List<String>) : java.sql.Array {
        return connection.createArrayOf(JDBCType.VARCHAR.getName(), list.toTypedArray())
    }
}