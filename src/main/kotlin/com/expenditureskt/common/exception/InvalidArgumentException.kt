package com.expenditureskt.common.exception

class InvalidArgumentException(message: String) : Exception(message)
