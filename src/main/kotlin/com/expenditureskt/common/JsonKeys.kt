package com.expenditureskt.common

enum class JsonKeys(val key: String) {
    AVERAGE_MONTHLY_SUMMARIES("averageSpendingByMonth"),
    INSERTED_EXPENDITURES("insertedExpenditures"),
    UNCATEGORIZED_MERCHANTS("uncategorizedMerchants"),
    FAILED_FILES("failedFiles"),
    CATEGORIES("categories"),
    ERROR("error"),
    AFFECTED_ENTRIES("affectedEntries"),
    MERCHANTS_BY_CATEGORY("merchantsByCategory"),
    MERCHANT_RULES("merchantRules"),
    IGNORED_MERCHANT_RULES("ignoredMerchantRules"),
    IGNORED_MERCHANTS("ignoredMerchants"),
    ALL_EXPENDITURES("allExpenditures"),
    CATEGORY_SUMMARIES("spendingByCategory"),
    MONTH_SUMMARIES("spendingByMonth"),
    WARNINGS("warnings"),
    MONTHLY_CATEGORY_AVERAGES("monthlyCategoryAverages"),
    CATEGORY_SPENDING_OVER_TIME("categorySpendingOverTime"),
    PERCENT_CHANGES("percentChanges")
}
