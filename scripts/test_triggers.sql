\c tmp;
drop database finances;
\i /Users/Andrew/Documents/CodingProjects/expenditureskt/scripts/init_database.sql;
insert into t_merchants_by_category values ('test', 5);
insert into t_expenditures values (default, to_date('01-01-2020', 'DD-MM-YYYY'), 'test', 1, 5);
select * from t_merchants_by_category;
select * from t_expenditures;
update t_merchants_by_category set category = 3 where merchant = 'test';
select * from t_merchants_by_category;
select * from t_expenditures;
insert into t_merchant_pattern_rules values (default, 'st', 1);
select * from t_merchants_by_category;
select * from t_expenditures;
select unknown_category_id();
select * from t_merchant_pattern_rules;
delete from t_merchant_pattern_rules where pattern = 'st';
select * from t_merchants_by_category;
select * from t_expenditures;

