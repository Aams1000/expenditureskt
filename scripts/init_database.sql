CREATE DATABASE finances;
\c finances;

create table t_categories (
    id serial primary key not null,
    name text unique not null check(length(name) > 0),
    is_custom boolean not null
);

insert into t_categories (id, name, is_custom) values 
    (default, 'Groceries', TRUE),
    (default, 'Restaurants', TRUE),
    (default, 'Transportation', TRUE),
    (default, 'Vacation', TRUE),
    (default, 'Gifts', TRUE),
    (default, 'Miscellaneous', TRUE),
    (default, 'Personal Expenditures', TRUE),
    (default, 'Pharmacy & Home Supplies', TRUE),
    (default, 'Newspapers & Publications', TRUE),
    (default, 'Entertainment', TRUE),
    (default, 'Health & Fitness', TRUE),
    (default, 'Video Games', TRUE),
    (default, 'Rent', TRUE),
    (default, 'Home Expenditures', TRUE),
    (default, 'Clothing', TRUE),
    (default, 'Charity', TRUE),
    (default, 'Investments', TRUE),
    (default, 'Cash Expenditures', TRUE),
    (default, 'Education', TRUE),
    (default, 'Insurance', TRUE),
    (default, 'Medical Expenses', TRUE),
    (default, 'Internet & Utilities', TRUE),
    (default, 'Unknown', FALSE),
    (default, 'Beauty', TRUE),
    (default, 'Kids', TRUE),
    (default, 'Dating', TRUE),
    (default, 'Work', TRUE);

create function unknown_category_id()
  returns int language sql as
  'SELECT id from t_categories where name = ''Unknown''';

create table t_merchants_by_category (
    merchant text unique primary key not null,
    category integer references t_categories (id),
    insertion_date timestamp not null
);

create table t_merchants_by_category_backup (
    like t_merchants_by_category including all
);

CREATE TABLE t_expenditures (
    id          SERIAL                  PRIMARY KEY NOT NULL,
    transaction_date DATE                     NOT NULL,
    merchant    TEXT                   NOT NULL references t_merchants_by_category(merchant) on delete cascade,
    category    smallint                NOT NULL references t_categories(id),
    amount      NUMERIC                 NOT NULL check (amount > 0),
    insertion_date timestamp                 not null,
    unique(transaction_date, merchant, amount)
);

create table t_expenditures_backup(
    like t_expenditures including all
);

-- Expenditures by account type
create table t_account_type (
    expenditure_id integer primary key references t_expenditures (id) on delete cascade,
    type smallint check (type >= 0)
);

create table t_merchants_to_ignore (
    merchant text primary key not null check (length(merchant) > 0)
);

create table t_merchant_ignore_rules (
    id serial primary key not null,
    pattern text not null check(length(pattern) > 0)
);

create table t_merchant_pattern_rules (
    id serial primary key not null,
    pattern text not null check(length(pattern) > 0),
    category smallint not null references t_categories(id)
);

create or replace function cascade_merchant_rule_insert_or_update()
    returns trigger as
    $body$
    begin
        update t_merchants_by_category
        set category = NEW.category
        where lower(merchant) like '%'  || NEW.pattern || '%';
        return NEW;
    end;
    $body$
    language plpgsql volatile
;

create trigger trigger_cascade_merchant_rule_insert_or_update
    after insert or update on t_merchant_pattern_rules
    for each row execute procedure cascade_merchant_rule_insert_or_update();

create or replace function cascade_merchant_update()
    returns trigger as
    $body$
    begin
        update t_expenditures
        set category = NEW.category
        where merchant = NEW.merchant;
        return NEW;
    end;
    $body$
    language plpgsql volatile
;

create trigger trigger_cascade_merchant_update
    after update on t_merchants_by_category
    for each row execute procedure cascade_merchant_update();

create or replace function cascade_merchant_rule_delete()
    returns trigger as
    $body$
    begin
        update t_merchants_by_category
        set category = (select unknown_category_id())
        where lower(merchant) like '%'  || OLD.pattern || '%';
        return OLD;
    end;
    $body$
    language plpgsql volatile
;

create trigger trigger_cascade_merchant_rule_delete
    before delete on t_merchant_pattern_rules
    for each row execute procedure cascade_merchant_rule_delete();

create or replace function cascade_category_delete_merchant_pattern_rules()
    returns trigger as
    $body$
    begin
        delete from t_merchant_pattern_rules where category = OLD.id;
--        return NEW;
        return OLD;
    end;
    $body$
    language plpgsql volatile
;

create trigger trigger_cascade_category_1_delete_merchant_pattern_rules
    before delete on t_categories
    for each row execute procedure cascade_category_delete_merchant_pattern_rules();

create or replace function cascade_category_delete_merchants_by_category()
    returns trigger as
    $body$
    begin
        update t_merchants_by_category
        set category = (select unknown_category_id())
        where category = OLD.id;
--        return NEW;
        return OLD;
    end;
    $body$
    language plpgsql volatile
;

create trigger trigger_cascade_category_delete_2_merchants_by_category
    before delete on t_categories
    for each row execute procedure cascade_category_delete_merchants_by_category();

--create or replace function cascade_category_delete_expenditures()
--    returns trigger as
--    $body$
--    begin
--       update t_expenditures
--       set category = (select unknown_category_id())
--       where category = OLD.id;
----        return NEW;
--       return NEW;
--    end;
--    $body$
--    language plpgsql volatile
--;
--
--create trigger trigger_cascade_category_delete_3_expenditures
--    before delete on t_categories
--    for each row execute procedure cascade_category_delete_expenditures();

create or replace function cascade_merchant_ignore_rule_insert_or_update()
    returns trigger as
    $body$
    begin
        insert into t_merchants_to_ignore (select m.merchant from t_merchants_by_category m where lower(m.merchant) like '%' || NEW.pattern || '%') on conflict do nothing;
        return NEW;
    end;
    $body$
    language plpgsql volatile
;

create trigger trigger_cascade_merchant_ignore_rule_insert_or_update
    after insert or update on t_merchant_ignore_rules
    for each row execute procedure cascade_merchant_ignore_rule_insert_or_update();






--create or replace function cascade_merchant_ignore()
--    returns trigger as
--    $body$
--    begin
--        delete from t_merchants_by_category where merchant = OLD.merchant;
--        return NEW;
--    end;
--    $body$
--    language plpgsql volatile
--;
--
--create trigger trigger_cascade_merchant_ignore
--    before insert on t_merchants_to_ignore
--    for each row execute procedure cascade_merchant_ignore();
--
--create or replace function cascade_merchant_deletion()
--    returns trigger as
--    $body$
--    begin
--        delete from t_expenditures where merchant = OLD.merchant;
--        return NEW;
--    end;
--    $body$
--    language plpgsql volatile
--;
--
--create trigger trigger_cascade_merchant_deletion
--    before delete on t_merchants_by_category
--    for each row execute procedure cascade_merchant_deletion();
