#!/bin/bash
set -e
bold_white=`tput setaf 7``tput bold`
bold_red=`tput setaf 1``tput bold`
normal=$(tput sgr0)

check_exit_code() {
	if [[ $1 -ne 0 ]]; then
		echo "${bold_red}===== Failure: please check previous output ===== ${normal}"
		cd $TOP
		exit $2
	fi
}

cd $TOP/expenditureskt
check_exit_code $? 1

echo "${bold_white}===== Running webpack =====  ${normal}"
cd frontend
webpack --bail

# compile
echo "${bold_white}===== Cleaning and compiling Kotlin =====  ${normal}"
cd ..
./gradlew clean
check_exit_code $? 2

./gradlew assemble
check_exit_code $? 3

# IntelliJ
# ./gradlew cleanIdea
# check_exit_code $? 4

# ./gradlew idea
# check_exit_code $? 5

echo "${bold_white}===== Launching Spring Boot application =====  ${normal}"
./gradlew bootRun
check_exit_code $? 6



